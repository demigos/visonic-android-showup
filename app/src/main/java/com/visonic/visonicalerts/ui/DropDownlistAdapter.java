package com.visonic.visonicalerts.ui;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.visonic.visonicalerts.R;

public class DropDownlistAdapter extends BaseAdapter {
    private Context mContext;
    private String[] mItems;

    public DropDownlistAdapter(Context context, String[] items) {
        this.mContext = context;
        this.mItems = items;
    }

    @Override
    public int getCount() {
        return mItems.length;
    }

    @Override
    public Object getItem(int position) {
        return mItems[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater = LayoutInflater.from(mContext);
            convertView = mInflater.inflate(R.layout.dropdown_listitem, parent, false);
        }

        TextView tv = (TextView) convertView.findViewById(R.id.text_item);
        String text = mItems[position];

        tv.setText(text);

        return convertView;
    }
}