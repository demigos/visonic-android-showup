package com.visonic.visonicalerts.ui.fragments.functionalfragments;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.visonic.visonicalerts.R;
import com.visonic.visonicalerts.data.DataLoadingSubject;
import com.visonic.visonicalerts.data.datamanager.ListDataManager;
import com.visonic.visonicalerts.data.datamanager.DevicesDataManager;
import com.visonic.visonicalerts.data.model.Device;
import com.visonic.visonicalerts.data.model.Trouble;
import com.visonic.visonicalerts.ui.BaseRecyclerViewAdapter;
import com.visonic.visonicalerts.ui.DataBindable;
import com.visonic.visonicalerts.ui.UiUtils;

import butterknife.BindView;

public class TroublesFragment extends BaseFunctionalFragment
		implements DataLoadingSubject.DataLoadingCallbacks {
	@BindView(R.id.list)
	RecyclerView mTroublesList;
	@BindView(R.id.toolbar)
	Toolbar mToolbar;

	private TroublesAdapter mTroublesAdapter;
	private ListDataManager<Trouble> mDataManager;
	private DevicesDataManager mDevicesDataManager;

	@NonNull
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = super.onCreateView(inflater, container, savedInstanceState);
		mDataManager = new ListDataManager<Trouble>(mLoginPrefs) {
			@Override
			protected void visonicServiceCall(SimpleListCallback<Trouble> callback) {
				getVisonicService().troubles(mLoginPrefs.getSessionToken()).enqueue(callback);
			}

			@Nullable
			@Override
			protected Class<Trouble> getDataClass() {
				return Trouble.class;
			}
		};
		mDataManager.addCallbacks(this);
		mDevicesDataManager = new DevicesDataManager(mLoginPrefs);
		mDevicesDataManager.addCallbacks(this);
		mTroublesAdapter = new TroublesAdapter(mDevicesDataManager);
		mTroublesList.setAdapter(mTroublesAdapter);
		Drawable icon = mUiUtils.getIconDrawable(R.drawable.ic_troubles_bold);
		mToolbar.setNavigationIcon(icon);
		return view;
	}

	@Override
	public void onResume() {
		super.onResume();
		mDevicesDataManager.startPolling();
	}

	@Override
	public void onPause() {
		super.onPause();
		mDevicesDataManager.stopPolling();
	}

	@Override
	protected int getLayoutId() {
		return R.layout.fragment_troubles;
	}

	@Nullable
	@Override
	protected ListDataManager<?> getDataManager() {
		return mDataManager;
	}

	@Override
	public void dataFinishedLoading() {
		super.dataFinishedLoading();
		if (mDataManager.getData() != null) {
			mTroublesAdapter.setData(mDataManager.getData());
		}
	}

	private static class TroublesAdapter extends BaseRecyclerViewAdapter<Trouble, TroublesAdapter.TroublesViewHolder> {
		private final DevicesDataManager mDevicesDataManager;

		private TroublesAdapter(DevicesDataManager devicesDataManager) {
			mDevicesDataManager = devicesDataManager;
		}

		@Override
		protected TroublesViewHolder getNewViewHolder(ViewGroup parent) {
			return new TroublesViewHolder(
					getViewFromLayout(R.layout.left_and_right_text_list_item, parent),
					parent.getContext());
		}

		public class TroublesViewHolder extends RecyclerView.ViewHolder
				implements DataBindable<Trouble> {
			private TextView leftText;
			private TextView rightText;
			private TextView leftSubtext;
			private Context context;

			public TroublesViewHolder(View itemView, Context context) {
				super(itemView);
				leftText = (TextView) itemView.findViewById(R.id.left_text);
				rightText = (TextView) itemView.findViewById(R.id.right_text);
				leftSubtext = (TextView) itemView.findViewById(R.id.left_subtext);
				this.context = context;
			}

			public void bind(Trouble trouble) {
				StringBuilder stringBuilder = new StringBuilder();
				if (trouble.location != null) {
					stringBuilder.append(trouble.location);
				} else {
					stringBuilder.append(context.getString(trouble.device_type.getTextResource()));
				}
				if (trouble.zone != null) {
					stringBuilder.append(" ").append(trouble.zone);
				}
				leftText.setText(stringBuilder.toString());
				if (trouble.zone == null && trouble.location == null) {
					leftSubtext.setVisibility(View.GONE);
				} else {
					boolean handled = false;
					if (mDevicesDataManager.getData() != null) {
						for (Device device : mDevicesDataManager.getData()) {
							if (device.type == trouble.zone_type
									&& device.zone == trouble.zone
									&& device.subtype != null) {
								leftSubtext.setText(UiUtils.getDeviceName(device.subtype, device.device_type));
								handled = true;
							}
						}
					}
					if (!handled) {
						if (trouble.zone_type != null) {
							leftSubtext.setText(trouble.zone_type.getTextResource());
						} else {
							leftSubtext.setText(trouble.device_type.getTextResource());
						}
					}
					leftSubtext.setVisibility(View.VISIBLE);
				}
				rightText.setText(trouble.trouble_type.getTextResource());
			}
		}
	}
}
