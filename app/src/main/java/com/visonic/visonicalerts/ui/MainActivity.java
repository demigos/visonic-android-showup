package com.visonic.visonicalerts.ui;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageButton;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.crashlytics.android.Crashlytics;
import com.visonic.visonicalerts.BuildConfig;
import com.visonic.visonicalerts.R;
import com.visonic.visonicalerts.data.RegisterGCMService;
import com.visonic.visonicalerts.data.databasemodel.PanelData;
import com.visonic.visonicalerts.ui.fragments.LoginFragment;
import com.visonic.visonicalerts.ui.fragments.LoginWrapperFragment;
import com.visonic.visonicalerts.ui.fragments.MainFragment;
import com.visonic.visonicalerts.ui.fragments.UserAgrrementFragment;
import com.visonic.visonicalerts.ui.fragments.settings.ApplicationSettingsFragment;

import java.text.MessageFormat;
import java.util.List;
import java.util.Locale;
import java.util.Random;

import butterknife.ButterKnife;
import io.fabric.sdk.android.Fabric;
import io.realm.Realm;

public class MainActivity extends AppCompatActivity
		implements LoginListener {
	private static final String TAG = "MainActivity";
	private static final String LAST_ACTIVITY_TIME = "last_activity_time";
	public static final int SEND_SMS_REQUEST_ID = 0;
	public static final String USER_CODE_STATE = "user_code_state";
	private static final long NO_ACTIVITY_DELAY = BuildConfig.NO_ACTIVITY_DELAY;
	public static final String ACTION_VIEW_PUSH_NOTIFICATION = "com.visonic.visonicalerts.intent.action.VIEW_PUSH_NOTIFICATION";
	public static final String ARG_PUSH_NOTIFICATION_MESSAGE = "com.visonic.visonicalerts.ui.MainActivity.ARG_PUSH_NOTIFICATION_MESSAGE";
	public static final String ARG_PUSH_NOTIFICATION_TITLE = "com.visonic.visonicalerts.ui.MainActivity.ARG_PUSH_NOTIFICATION_TITLE";

	private Realm realm;
	private int mErrorTextRes;
	private long mLastActivityTime;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ButterKnife.bind(this);
		Fabric.with(this, new Crashlytics());
		if (savedInstanceState == null) {
			replaceFragment(LoginWrapperFragment.createLoginInstance(), false);
		}
		mLastActivityTime = getPreferences(MODE_PRIVATE).getLong(LAST_ACTIVITY_TIME,
				System.currentTimeMillis());
		PreferenceManager.setDefaultValues(this, R.xml.preferences, false);

		Intent intent = new Intent(this, RegisterGCMService.class);
		startService(intent);

		SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
		boolean showTooltips = sharedPref.getBoolean(
				ApplicationSettingsFragment.SHOW_HELP_TOOLTIPS, false);
		if (showTooltips) {
			showTooltip();
		}
		Locale activeLocale = ApplicationSettingsFragment.Localization.getActiveLocale(this);
		Configuration conf = getBaseContext().getResources().getConfiguration();
		conf.locale = activeLocale;
		getBaseContext().getResources().updateConfiguration(conf, getResources().getDisplayMetrics());

		Configuration systemConf = Resources.getSystem().getConfiguration();
		systemConf.locale = activeLocale;
		Resources.getSystem().updateConfiguration(systemConf, Resources.getSystem().getDisplayMetrics());

		Locale.setDefault(activeLocale);

		realm = Realm.getDefaultInstance();
		handleLaunchIntent(getIntent());
	}

	@Override
	protected void onStart() {
		super.onStart();
		long delay = System.currentTimeMillis() - mLastActivityTime;
		if (delay > NO_ACTIVITY_DELAY) {
			getSupportFragmentManager().popBackStack(USER_CODE_STATE, 0);
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		mLastActivityTime = System.currentTimeMillis();
		getPreferences(MODE_PRIVATE).edit()
				.putLong(LAST_ACTIVITY_TIME, mLastActivityTime).apply();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		realm.close();
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		handleLaunchIntent(intent);
	}

	private void handleLaunchIntent(Intent intent) {
		String action = intent.getAction();
		if (ACTION_VIEW_PUSH_NOTIFICATION.equals(action)) {
			String message = intent.getStringExtra(ARG_PUSH_NOTIFICATION_MESSAGE);
			String panelName = intent.getStringExtra(ARG_PUSH_NOTIFICATION_TITLE);
			MessageDialogFragment.createInstance(message, MessageFormat.format(getString(R.string.format_panel), panelName))
				.show(getSupportFragmentManager(), null);
			LoginFragment loginFragment = getLoginFragment();
			if (loginFragment != null) {
				loginFragment.setPanelName(panelName);
				PanelData panelData = realm.where(PanelData.class)
						.equalTo(LoginFragment.PANEL_NAME, panelName).findFirst();
				if (panelData != null) {
					loginFragment.setHostName(panelData.getHostAddress());
				}
			}
		}
	}

	public Realm getRealmInstance() {
		return realm;
	}

	public void updateLastActivityTime() {
		mLastActivityTime = System.currentTimeMillis();
	}

	@Override
	public void onPanelExists() {
		replaceFragment(LoginWrapperFragment.createPinCodeInstance(), true, USER_CODE_STATE);
	}

	@Override
	public void onPinCorrect() {
		replaceFragment(new MainFragment(), true);
	}

	@Override
	public void goBackToLoginWithError(int errorTextRes) {
		getSupportFragmentManager().popBackStack(USER_CODE_STATE, FragmentManager.POP_BACK_STACK_INCLUSIVE);
		mErrorTextRes = errorTextRes;
	}

	@Override
	public int getLastErrorTextRes() {
		int errorTextRes = mErrorTextRes;
		mErrorTextRes = 0;
		return errorTextRes;
	}

	@Override
	public void showUserAgreement() {
		replaceFragment(new UserAgrrementFragment(), true);
	}

	private int replaceFragment(Fragment loginFragment, boolean addToStack) {
		return replaceFragment(loginFragment, addToStack, null);
	}

	private int replaceFragment(Fragment loginFragment, boolean addToStack, String stateName) {
		if (addToStack) {
			return getSupportFragmentManager().beginTransaction()
					.replace(R.id.fragmentContainer, loginFragment)
					.addToBackStack(stateName)
					.commit();
		} else {
			return getSupportFragmentManager().beginTransaction()
					.replace(R.id.fragmentContainer, loginFragment)
					.commit();
		}
	}

	public void showTooltip() {
		new HelpDialogFragment().show(getSupportFragmentManager(), null);
	}

	@Override
	public void onBackPressed() {
		for (Fragment fragment : getSupportFragmentManager().getFragments()) {
			if (fragment instanceof BackButtonListener) {
				if (fragment.isVisible() && ((BackButtonListener) fragment).onBackButtonPressed()) {
					return;
				}
			}
		}
		super.onBackPressed();
	}

	public interface BackButtonListener {
		boolean onBackButtonPressed();
	}

	public interface OnPermissionReceivedListener {
		boolean onPermissionReceived(int permissionId);
	}

	@Override
	public void onRequestPermissionsResult(int requestCode,
										   @NonNull String permissions[],
										   @NonNull int[] grantResults) {
		switch (requestCode) {
			case SEND_SMS_REQUEST_ID: {
				// If request is cancelled, the result arrays are empty.
				//noinspection StatementWithEmptyBody
				if (grantResults.length > 0
						&& grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					for (Fragment fragment : getSupportFragmentManager().getFragments()) {
						if (fragment instanceof OnPermissionReceivedListener) {
							if (fragment.isVisible() && ((OnPermissionReceivedListener) fragment)
									.onPermissionReceived(SEND_SMS_REQUEST_ID)) {
								return;
							}
						}
					}
				} else {
					// TODO: 3/2/16 implement no permission message
				}
			}
		}
	}

	@Nullable
	private LoginFragment getLoginFragment() {
		final List<Fragment> fragments = getSupportFragmentManager().getFragments();
		if (fragments == null) {
			return null;
		}
		for (Fragment fragment : fragments) {
			if (fragment instanceof LoginWrapperFragment
					&& fragment.isVisible()
					&& ((LoginWrapperFragment) fragment).isLoginScreen()) {
				return (LoginFragment) ((LoginWrapperFragment) fragment).getWrappedFragment();
			}
		}
		return null;
	}

	public static class HelpDialogFragment extends DialogFragment {
		private static final int[] helpStrings = new int[]{
				R.string.panel_swipe_right,
				R.string.address_swipe_right,
				R.string.panel_swipe_left,
				R.string.address_swipe_left,
				R.string.panel_double_tap,
				R.string.address_double_tap,
				R.string.code_swipe_right,
				R.string.alarm_tap,
				R.string.alarm_swipe_left,
				R.string.alarm_text_tap,
				R.string.detector_swipe_left,
				R.string.event_tap,
				R.string.aggr_tap,
				R.string.help_tap,
				R.string.lang_tap,
				R.string.settings_tap,
				R.string.camera_swipe_right,
				R.string.camera_tap,
				R.string.video_tap,
				R.string.frames_tap,
				R.string.frames_swipe_left,
				R.string.reload_button,
		};

		private final Random mRandom = new Random(System.currentTimeMillis());

		@NonNull
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			UiUtils mUiUtils = new UiUtils(getActivity());
			LayoutInflater inflater = LayoutInflater.from(getActivity());
			@SuppressLint("InflateParams")
			View view = inflater.inflate(R.layout.dialog_tooltips, null);
			final ViewFlipper tipsFlipper = (ViewFlipper) view.findViewById(R.id.text_switcher);
			for (int helpString : helpStrings) {
				@SuppressLint("InflateParams")
				TextView tipTextView = (TextView) inflater.inflate(R.layout.fragment_tip_item, null);
				tipTextView.setText(helpString);
				tipsFlipper.addView(tipTextView);
			}
			tipsFlipper.setDisplayedChild(mRandom.nextInt(helpStrings.length));

			AppCompatImageButton mLeftButton =
					(AppCompatImageButton) view.findViewById(R.id.left_button);
			mLeftButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					tipsFlipper.showNext();
				}
			});
			AppCompatImageButton mRightButton =
					(AppCompatImageButton) view.findViewById(R.id.right_button);
			mRightButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					tipsFlipper.showPrevious();
				}
			});

			int arrowColor = ContextCompat.getColor(getActivity(), R.color.colorAccent);
			Drawable leftArrow = mUiUtils.getIconDrawable(
					R.drawable.ic_keyboard_arrow_left_white_48dp, arrowColor);
			mLeftButton.setImageDrawable(leftArrow);
			Drawable rightArrow = mUiUtils.getIconDrawable(
					R.drawable.ic_keyboard_arrow_right_white_48dp, arrowColor);
			mRightButton.setImageDrawable(rightArrow);

			CompoundButton showTooltipsCheckbox =
					(CompoundButton) view.findViewById(R.id.show_tooltip_checkbox);
			final SharedPreferences sharedPref = PreferenceManager
					.getDefaultSharedPreferences(getActivity());
			boolean showTooltips = sharedPref.getBoolean(
					ApplicationSettingsFragment.SHOW_HELP_TOOLTIPS, false);
			showTooltipsCheckbox.setChecked(showTooltips);
			showTooltipsCheckbox.setOnCheckedChangeListener(
					new CompoundButton.OnCheckedChangeListener() {
						@Override
						public void onCheckedChanged(CompoundButton buttonView,
													 boolean isChecked) {
							sharedPref.edit().putBoolean(ApplicationSettingsFragment.SHOW_HELP_TOOLTIPS,
									isChecked).apply();
						}
					});

			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setTitle(R.string.tip_of_the_day);
			builder.setView(view);
			builder.setPositiveButton(R.string.ok, null);
			return builder.create();
		}
	}

	public static class MessageDialogFragment extends DialogFragment {
		private static final String MESSAGE = "message";
		private static final String TITLE = "title";

		@NonNull
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setTitle(getArguments().getString(TITLE));
			builder.setMessage(getArguments().getString(MESSAGE));
			builder.setPositiveButton(R.string.ok, null);
			return builder.create();
		}

		public static MessageDialogFragment createInstance(String message, String title) {
			Bundle args = new Bundle();
			args.putString(MESSAGE, message);
			args.putString(TITLE, title);
			MessageDialogFragment fragment = new MessageDialogFragment();
			fragment.setArguments(args);
			return fragment;
		}
	}
}
