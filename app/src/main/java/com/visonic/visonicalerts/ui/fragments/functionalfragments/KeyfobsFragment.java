package com.visonic.visonicalerts.ui.fragments.functionalfragments;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.visonic.visonicalerts.R;
import com.visonic.visonicalerts.data.DataLoadingSubject;
import com.visonic.visonicalerts.data.datamanager.ListDataManager;
import com.visonic.visonicalerts.data.datamanager.DevicesDataManager;
import com.visonic.visonicalerts.data.model.DeviceType;
import com.visonic.visonicalerts.data.model.ZoneSubtype;
import com.visonic.visonicalerts.ui.DevicesAdapter;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;

import butterknife.BindView;

public class KeyfobsFragment extends BaseFunctionalFragment implements DataLoadingSubject.DataLoadingCallbacks {
	public static final EnumSet<DeviceType> KEYFOB_TYPES = EnumSet.of(DeviceType.KEYFOB,
			DeviceType.WIRELESS_COMMANDER, DeviceType.TWO_WAY_WIRELESS_KEYPAD,
			DeviceType.PENDANT, DeviceType.GUARD_KEY);
	public static final EnumSet<ZoneSubtype> KEYFOB_SUBTYPES =
			EnumSet.of(ZoneSubtype.BASIC_KEYFOB, ZoneSubtype.KEYFOB_ARM_LED);
	private static final String TAG = "KeyfobsFragment";
	public static final int MAX_ZONES = 32;
	public static final int DELAY_TIME = 3 * 60 * 1000;
	@BindView(R.id.list)
	RecyclerView mDevicesList;
	@BindView(R.id.toolbar)
	Toolbar mToolbar;
	private DevicesDataManager mDataManager;
	private DevicesAdapter mKeyfobsAdapter;
	private Handler mHandler = new Handler();

	@NonNull
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = super.onCreateView(inflater, container, savedInstanceState);
		mDataManager = new DevicesDataManager(mLoginPrefs);
		mDataManager.addCallbacks(this);
		mDataManager.setFilters(KEYFOB_TYPES);
		mDataManager.setSubtypesFilters(KEYFOB_SUBTYPES);
		mKeyfobsAdapter = new DevicesAdapter(mDataManager, getActivity(), mHandler, true);
		mDevicesList.setAdapter(mKeyfobsAdapter);
		view.findViewById(R.id.floating_action_button).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				new EnrollKeyfobDialogFragment().show(getChildFragmentManager(), null);
			}
		});
		mToolbar.setNavigationIcon(mUiUtils.getIconDrawable(R.drawable.ic_keyfobs));
		return view;
	}

	@Override
	protected int getLayoutId() {
		return R.layout.fragment_keyfobs;
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mHandler.removeCallbacksAndMessages(null);
	}

	@Nullable
	@Override
	protected ListDataManager<?> getDataManager() {
		return mDataManager;
	}

	@Override
	public void dataFinishedLoading() {
		super.dataFinishedLoading();
		mKeyfobsAdapter.notifyNewData();
	}

	public void enrollKeyfob(String keyfobId, int zoneId) {
		mDataManager.enrollKeyfob(keyfobId, zoneId);
	}

	public Set<Integer> getZonesWithKeyfob() {
		return mKeyfobsAdapter.getZonesWithKeyfob();
	}

	public static class EnrollKeyfobDialogFragment extends DialogFragment {
		@NonNull
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setTitle(R.string.add_new_keyfob);
			@SuppressLint("InflateParams")
			View view = LayoutInflater.from(getActivity())
					.inflate(R.layout.enroll_keyfob_dialog, null);
			final Spinner zoneIdSpinner = (Spinner) view.findViewById(R.id.zone_id);
			Set zonesWithKeyfob = ((KeyfobsFragment) getParentFragment()).getZonesWithKeyfob();
			List<String> zones = new ArrayList<>(MAX_ZONES - zonesWithKeyfob.size());
			for (int i = 1; i <= MAX_ZONES; i++) {
				if (!zonesWithKeyfob.contains(i)) {
					zones.add(String.valueOf(i));
				}
			}
			ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(getActivity(),
					android.R.layout.simple_spinner_item, zones);
			spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			zoneIdSpinner.setAdapter(spinnerArrayAdapter);
			final EditText keyfobIdField = (EditText) view.findViewById(R.id.keyfob_id);
			keyfobIdField.addTextChangedListener(new TextWatcher() {
				int prevL = 0;

				@Override
				public void beforeTextChanged(CharSequence s, int start, int count, int after) {
					prevL = keyfobIdField.getText().toString().length();
				}

				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {

				}

				@Override
				public void afterTextChanged(Editable s) {
					int length = s.length();
					if ((prevL < length) && (length == 3)) {
						s.append("-");
					}
				}
			});
			builder.setView(view);
			builder.setPositiveButton(R.string.enroll, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					Integer selectedItem = Integer.valueOf((String) zoneIdSpinner.getSelectedItem());
					String keyfobId = keyfobIdField.getText().toString().replaceAll("-", "");
					((KeyfobsFragment) getParentFragment())
							.enrollKeyfob(keyfobId, selectedItem);
				}
			});
			return builder.create();
		}
	}
}