package com.visonic.visonicalerts.ui;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.ColorInt;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.swipe.SwipeLayout;
import com.visonic.visonicalerts.R;
import com.visonic.visonicalerts.data.datamanager.DevicesDataManager;
import com.visonic.visonicalerts.data.model.AlarmType;
import com.visonic.visonicalerts.data.model.AlertType;
import com.visonic.visonicalerts.data.model.Device;
import com.visonic.visonicalerts.data.model.DeviceType;
import com.visonic.visonicalerts.data.model.TroubleType;
import com.visonic.visonicalerts.ui.fragments.functionalfragments.KeyfobsFragment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DevicesAdapter extends BaseRecyclerViewAdapter<Device, DevicesAdapter.DeviceViewHolder> {
	private static final Comparator<Device> DEVICE_BY_TYPE_COMPARATOR = new Comparator<Device>() {
		@Override
		public int compare(Device lhs, Device rhs) {
			int LEFT_BIGGER = 1;
			int RIGHT_BIGGER = -1;
			if (lhs == rhs) return 0;
			if (lhs.device_type == null) return RIGHT_BIGGER;
			if (rhs.device_type == null) return LEFT_BIGGER;
			return lhs.device_type.compareTo(rhs.device_type);
		}
	};
	private static final String TAG = "DevicesAdapter";
	private Set<Integer> zonesWithKeyfob = new HashSet<>(KeyfobsFragment.MAX_ZONES);
	private Set<Integer> mDeletedKeyfobZones = new HashSet<>(KeyfobsFragment.MAX_ZONES);
	private Set<Integer> mAddedKeyfobZones = new HashSet<>(KeyfobsFragment.MAX_ZONES);
	private final DevicesDataManager mDataManager;
	private final Context mContext;
	private final Handler mHandler;
	private List<Device> mCache = new ArrayList<>();

	private static final int TIMER_DELAY = 2500;
	List<TimerListener> mTimerListeners = new ArrayList<>();
	List<TimerListener> mListenersToRemove = new ArrayList<>();
	Runnable mTimerRunnable = new Runnable() {
		@Override
		public void run() {
			for (TimerListener timerListener : mTimerListeners) {
				timerListener.onTimerEvent();
			}
			mTimerListeners.removeAll(mListenersToRemove);
			mListenersToRemove.clear();
			mHandler.postDelayed(this, TIMER_DELAY);
		}
	};
	private boolean mIsKeyfobAdapter;

	@ColorInt
	private final int mAlarmsColor;
	@ColorInt
	private final int mAlertColor;
	@ColorInt
	private final int mTroubleColor;
	@ColorInt
	private final int mInactiveColor;

	public DevicesAdapter(DevicesDataManager mDataManager, Context context, Handler handler, boolean keyfobs) {
		this.mDataManager = mDataManager;
		mContext = context;
		mHandler = handler;
		mAlarmsColor = ContextCompat.getColor(mContext, R.color.alarmColor);
		mAlertColor = ContextCompat.getColor(mContext, R.color.alertColor);
		mTroubleColor = ContextCompat.getColor(mContext, R.color.troubleColor);
		mInactiveColor = ContextCompat.getColor(mContext, R.color.inactiveColor);
		mHandler.postDelayed(mTimerRunnable, TIMER_DELAY);
		mIsKeyfobAdapter = keyfobs;
	}

	@Override
	protected DeviceViewHolder getNewViewHolder(ViewGroup parent) {
		return new DeviceViewHolder(getViewFromLayout(R.layout.device_list_item, parent));
	}

	public void notifyNewData() {
		Set<Integer> zonesNotDeleted = mDataManager.getZonesNotDeleted();
		if (!zonesNotDeleted.isEmpty()) {
			for (Integer zone : zonesNotDeleted) {
				mDeletedKeyfobZones.remove(zone);
				deleteOperationResult(false, zone);
			}
		}
		Set<Integer> zonesDeleted = mDataManager.getZonesDeleted();
		if (!zonesDeleted.isEmpty()) {
			for (Integer zone : zonesDeleted) {
				mHandler.postDelayed(new CheckIfZoneDeletedRunnable(zone), KeyfobsFragment.DELAY_TIME);
			}
		}
		Set<Integer> zonesNotAdded = mDataManager.getZonesNotAdded();
		if (!zonesNotAdded.isEmpty()) {
			for (Integer zone : zonesNotAdded) {
				addOperationResult(false, zone);
			}
		}
		Set<Integer> zonesAdded = mDataManager.getZonesAdded();
		if (!zonesAdded.isEmpty()) {
			for (Integer zone : zonesAdded) {
				mAddedKeyfobZones.add(zone);
				mHandler.postDelayed(new CheckIfZoneAddedRunnable(zone), KeyfobsFragment.DELAY_TIME);
			}
		}

		if (mDataManager.getData() != null) {
			zonesWithKeyfob.clear();
			Set<Integer> deleted = new HashSet<>(mDeletedKeyfobZones);
			for (Device device : mDataManager.getData()) {
				zonesWithKeyfob.add(device.zone);
				if (mAddedKeyfobZones.contains(device.zone)) {
					addOperationResult(true, device.zone);
				}
				mAddedKeyfobZones.remove(device.zone);
				deleted.remove(device.zone);
			}
			for (Integer zone : deleted) {
				deleteOperationResult(true, zone);
				mDeletedKeyfobZones.remove(zone);
			}

			ArrayList<Device> fullData = new ArrayList<>();
			fullData.addAll(mDataManager.getData());
			for (Integer addedKeyfobZone : mAddedKeyfobZones) {
				fullData.add(new Device(addedKeyfobZone, DeviceType.KEYFOB));
			}
			if (mCache == null || !mCache.equals(fullData)) {
				mCache = new ArrayList<>(fullData);
				Collections.sort(fullData, DEVICE_BY_TYPE_COMPARATOR);
				super.setData(fullData);
			}
		}
	}

	private void addOperationResult(boolean success, int zone) {
		int messagePattern = success ? R.string.creation_of_new_keyfob_succeeded_pattern
				: R.string.creation_of_new_keyfob_failed_pattern;
		Toast.makeText(mContext, mContext.getString(messagePattern, zone), Toast.LENGTH_SHORT).show();
	}

	private void deleteOperationResult(boolean success, int zone) {
		int messagePattern = success ? R.string.deletion_succeeded_for_zone_pattern
				: R.string.deletion_failed_for_zone_pattern;
		Toast.makeText(mContext, mContext.getString(messagePattern, zone), Toast.LENGTH_SHORT).show();
	}

	public Set<Integer> getZonesWithKeyfob() {
		return zonesWithKeyfob;
	}

	class DeviceViewHolder extends RecyclerView.ViewHolder
			implements DataBindable<Device> {
		private ImageView icon;
		private TextView name;
		private TextView location;
		private TextView zone;
		private TextView statusText;
		private Button mDeleteButton;
		private SwipeLayout mSwipeLayout;
		private final List<Status> statuses = new ArrayList<>();
		private int currentStatus;

		public DeviceViewHolder(View itemView) {
			super(itemView);
			mSwipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe_layout);
			icon = (ImageView) itemView.findViewById(R.id.device_type_icon);
			name = (TextView) itemView.findViewById(R.id.device_name);
			location = (TextView) itemView.findViewById(R.id.device_location);
			zone = (TextView) itemView.findViewById(R.id.device_zone);
			statusText = (TextView) itemView.findViewById(R.id.status_text);
			mDeleteButton = (Button) itemView.findViewById(R.id.delete_button);
		}

		public void bind(final Device device) {
			this.name.setText(UiUtils.getDeviceName(device.subtype, device.device_type));
			location.setText(device.location);
			zone.setText(mContext.getString(R.string.zone_pattern, device.zone));

			LayoutInflater layoutInflater = LayoutInflater.from(mContext);
			statuses.clear();
			if (device.alarms != null && device.alarms.length > 0) {
				for (AlarmType alarm : device.alarms) {
					statuses.add(new Status(alarm.getTextResource(), mAlarmsColor));
				}
			}
			if (device.alerts != null && device.alerts.length > 0) {
				for (AlertType alert : device.alerts) {
					statuses.add(new Status(alert.getTextResource(), mAlertColor));
				}
			}
			if (device.troubles != null && device.troubles.length > 0) {
				for (TroubleType troubleType : device.troubles) {
					boolean isActive = troubleType == TroubleType.INACTIVE;
					@ColorInt
					int color = isActive ? mInactiveColor : mTroubleColor;
					statuses.add(new Status(troubleType.getTextResource(), color));
				}
			}
			if (statuses.size() == 0) {
				statusText.setText("");
			} else if (statuses.size() > 1) {
				currentStatus = 0;
				setStatus(getNextStatus());
				mTimerListeners.add(new TimerListener() {
					@Override
					public void onTimerEvent() {
						if (statuses.size() < 1) {
							mListenersToRemove.add(this);
						} else {
							setStatus(getNextStatus());
						}
					}
				});
			} else {
				setStatus(statuses.get(0));
			}

			icon.setImageResource(UiUtils.getDeviceIcon(device.subtype, device.device_type));

			boolean isKeyfob = device.device_type == DeviceType.KEYFOB;
			boolean enabled = !isKeyfob ||
					!(mDeletedKeyfobZones.contains(device.zone) || mAddedKeyfobZones.contains(device.zone));

			mSwipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);
			boolean bypassAvailability = device.bypass_availability;
			boolean isDeleteAvailable = (isKeyfob && mIsKeyfobAdapter) || bypassAvailability;
			mSwipeLayout.setSwipeEnabled(isDeleteAvailable);
			mDeleteButton.setText(isKeyfob ? R.string.remove : device.bypass ? R.string.unbypass : R.string.bypass);
			mDeleteButton.setEnabled(enabled);
			if (enabled) {
				if (isKeyfob) {
					mDeleteButton.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							Snackbar.make(mDeleteButton, R.string.remove_keyfob, Snackbar.LENGTH_LONG)
									.setAction(R.string.confirm, new View.OnClickListener() {
										@Override
										public void onClick(View v) {
											mDataManager.deleteKeyfob(device.zone);
											mDeletedKeyfobZones.add(device.zone);
											notifyDataSetChanged();
										}
									}).show();
						}
					});
				} else if (device.bypass_availability) {
					mDeleteButton.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							mDataManager.setBypassZone(device.zone, !device.bypass);
						}
					});
				}
			} else {
				mDeleteButton.setOnClickListener(null);
			}

			icon.setAlpha(enabled ? 1f : 0.3f);
			this.name.setEnabled(enabled);
			location.setEnabled(enabled);
			zone.setEnabled(enabled);
		}

		private void setStatus(Status status) {
			SwipeLayout.Status openStatus = mSwipeLayout.getOpenStatus();
			statusText.setText(status.textRes);
			statusText.setTextColor(status.color);
			if (openStatus == SwipeLayout.Status.Open) {
				mSwipeLayout.open(false);
			}
		}

		private Status getNextStatus() {
			currentStatus = ++currentStatus % statuses.size();
			return statuses.get(currentStatus);
		}
	}

	private static class Status {
		@StringRes
		private final int textRes;
		private final int color;

		private Status(@StringRes int textRes, int color) {
			this.textRes = textRes;
			this.color = color;
		}
	}

	private class CheckIfZoneDeletedRunnable implements Runnable {
		private final Integer mZone;

		public CheckIfZoneDeletedRunnable(Integer zone) {
			mZone = zone;
		}

		@Override
		public void run() {
			for (Device device : mDataManager.getData()) {
				if (mZone.equals(device.zone)) {
					mDeletedKeyfobZones.remove(mZone);
					deleteOperationResult(false, mZone);
					notifyDataSetChanged();
				}
			}
		}
	}

	private class CheckIfZoneAddedRunnable implements Runnable {
		private final Integer mZone;

		public CheckIfZoneAddedRunnable(Integer zone) {
			mZone = zone;
		}

		@Override
		public void run() {
			for (Integer addedKeyfob : mAddedKeyfobZones) {
				if (mZone.equals(addedKeyfob)) {
					for (Device device : mDataManager.getData()) {
						if (mZone.equals(device.zone)) {
							mAddedKeyfobZones.remove(addedKeyfob);
							return;
						}
					}
					mAddedKeyfobZones.remove(mZone);
					addOperationResult(false, mZone);
					notifyDataSetChanged();
				}
			}
		}
	}

	private interface TimerListener {
		void onTimerEvent();
	}
}
