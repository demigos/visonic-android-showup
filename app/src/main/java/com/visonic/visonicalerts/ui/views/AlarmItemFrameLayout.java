package com.visonic.visonicalerts.ui.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

public class AlarmItemFrameLayout extends FrameLayout {

	public AlarmItemFrameLayout(Context context) {
		super(context, null);
	}

	public AlarmItemFrameLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		//noinspection SuspiciousNameCombination
		int widthSize = MeasureSpec.getSize(widthMeasureSpec);
		int heightSize = MeasureSpec.getSize(heightMeasureSpec);
		if (heightSize < widthSize) {
			//noinspection SuspiciousNameCombination
			super.onMeasure(heightMeasureSpec, heightMeasureSpec);
		} else {
			//noinspection SuspiciousNameCombination
			super.onMeasure(widthMeasureSpec, widthMeasureSpec);
		}
	}

	@Override
	public boolean hasOverlappingRendering() {
		return false;
	}
}
