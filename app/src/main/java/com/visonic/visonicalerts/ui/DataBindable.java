package com.visonic.visonicalerts.ui;

public interface DataBindable<T> {
	void bind(T data);
}
