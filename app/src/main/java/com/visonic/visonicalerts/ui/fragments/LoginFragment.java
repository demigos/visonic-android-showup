package com.visonic.visonicalerts.ui.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v7.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.visonic.visonicalerts.BuildConfig;
import com.visonic.visonicalerts.R;
import com.visonic.visonicalerts.data.datamanager.BaseStatusDataManager;
import com.visonic.visonicalerts.data.DataLoadingSubject;
import com.visonic.visonicalerts.data.datamanager.LoginDataManager;
import com.visonic.visonicalerts.data.databasemodel.PanelData;
import com.visonic.visonicalerts.ui.FilterableRealmBaseAdapter;
import com.visonic.visonicalerts.ui.LoginListener;
import com.visonic.visonicalerts.ui.MainActivity;
import com.visonic.visonicalerts.ui.fragments.settings.ApplicationSettingsFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmResults;

public class LoginFragment extends BaseFragment implements DataLoadingSubject.DataLoadingCallbacks {
	public static final String TAG = "LoginFragment";
	private static final String TERMS_OF_USE_ACCEPTED = "terms_of_use_accepted";
	public static final int MAX_HOST_ADDRESS_LENGTH = 256;
	public static final int MAX_PANEL_ID_LENGTH = 25;
	public static final String PANEL_NAME = "panelName";
	public static final String HOST_ADDRESS = "hostAddress";

	private LoginListener mListener;

	@BindView(R.id.title)
	TextView titleTextView;
	@BindView(R.id.hostAddressTextView)
	TextView hostAddressTextView;
	@BindView(R.id.hostAddressAutocompleteTextView)
	AutoCompleteTextView hostAddressAutocompleteTextView;
	@BindView(R.id.panelIdTextView)
	TextView panelIdTextView;
	@BindView(R.id.panelIdAutoCompleteTextView)
	AutoCompleteTextView panelIdAutoCompleteTextView;
	@BindView(R.id.termsOfUseCheckBox)
	CheckBox termsOfUseCheckBox;
	@BindView(R.id.termsOfUseTextView)
	TextView termsOfUseTextView;

	private LoginDataManager mDataManager;
	private ApplicationSettingsFragment.RememberLoginInfo rememberLoginInfo;
	private Realm realm;

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		if (context instanceof LoginListener) {
			mListener = (LoginListener) context;
		} else {
			throw new RuntimeException(context.toString()
					+ " must implement LoginListener");
		}
	}

	@NonNull
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = super.onCreateView(inflater, container, savedInstanceState);
		SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
		String rememberLoginInfoKey = sharedPref.getString(ApplicationSettingsFragment.REMEMBER_LOGIN_INFO,
				ApplicationSettingsFragment.RememberLoginInfo.LAST.getKey());
		rememberLoginInfo =
				ApplicationSettingsFragment.RememberLoginInfo.getByKey(rememberLoginInfoKey);
		realm = ((MainActivity) getActivity()).getRealmInstance();
		if (rememberLoginInfo == ApplicationSettingsFragment.RememberLoginInfo.LAST) {
			hostAddressTextView.setText(mLoginPrefs.getHostAddress());
			hostAddressAutocompleteTextView.setVisibility(View.GONE);
			panelIdTextView.setText(mLoginPrefs.getPanelId());
			panelIdAutoCompleteTextView.setVisibility(View.GONE);
			RealmResults<PanelData> panelData = realm.where(PanelData.class).findAll();
			realm.beginTransaction();
			panelData.clear();
			realm.commitTransaction();
		} else if (rememberLoginInfo == ApplicationSettingsFragment.RememberLoginInfo.ALL) {
			RealmResults<PanelData> panelData = realm.where(PanelData.class).findAll();
			hostAddressTextView.setVisibility(View.GONE);
			final PanelDataByHostAdapter hostAdapter = new PanelDataByHostAdapter(getActivity(),
					android.R.layout.simple_dropdown_item_1line, panelData);
			hostAddressAutocompleteTextView.setText(mLoginPrefs.getHostAddress());
			hostAddressAutocompleteTextView.setAdapter(hostAdapter);
			hostAddressAutocompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
					hostAddressAutocompleteTextView.setText(hostAdapter.getItem(position).getHostAddress());
					panelIdAutoCompleteTextView.setText(hostAdapter.getItem(position).getPanelName());
				}
			});
			panelIdTextView.setVisibility(View.GONE);
			final PanelDataByPanelAdapter panelAdapter = new PanelDataByPanelAdapter(getActivity(),
					android.R.layout.simple_dropdown_item_1line, panelData);
			panelIdAutoCompleteTextView.setText(mLoginPrefs.getPanelId());
			panelIdAutoCompleteTextView.setAdapter(panelAdapter);
			panelIdAutoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
					hostAddressAutocompleteTextView.setText(panelAdapter.getItem(position).getHostAddress());
					panelIdAutoCompleteTextView.setText(panelAdapter.getItem(position).getPanelName());
				}
			});
		} else {
			hostAddressAutocompleteTextView.setVisibility(View.GONE);
			panelIdAutoCompleteTextView.setVisibility(View.GONE);
			mLoginPrefs.setHostAddress(null);
			mLoginPrefs.setPanelId(null);
			RealmResults<PanelData> panelData = realm.where(PanelData.class).findAll();
			realm.beginTransaction();
			panelData.clear();
			realm.commitTransaction();
		}

		setErrorCleaner(hostAddressTextView);
		setErrorCleaner(hostAddressAutocompleteTextView);
		setErrorCleaner(panelIdTextView);
		setErrorCleaner(panelIdAutoCompleteTextView);

		termsOfUseCheckBox.setChecked(sharedPref.getBoolean(TERMS_OF_USE_ACCEPTED, false));
		termsOfUseCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				termsOfUseTextView.setError(null);
			}
		});

		return view;
	}

	public void setHostName(String hostName) {
		hostAddressAutocompleteTextView.setText(hostName);
		hostAddressTextView.setText(hostName);
	}

	public void setPanelName(String panelName) {
		panelIdAutoCompleteTextView.setText(panelName);
		panelIdTextView.setText(panelName);
	}

	public static void setErrorCleaner(final TextView textView) {
		textView.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				textView.setError(null);
			}

			@Override
			public void afterTextChanged(Editable s) {
			}
		});
	}

	@Override
	public void onResume() {
		int lastErrorTextRes = mListener.getLastErrorTextRes();
		if (lastErrorTextRes != 0) {
			setPanelError(lastErrorTextRes);
		}
		super.onResume();
	}

	@Override
	protected int getLayoutId() {
		return R.layout.fragment_login;
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		if (mDataManager != null) {
			mDataManager.removeCallbacks(this);
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mListener = null;
	}

	@OnClick(R.id.nextButton)
	public void checkPanelExistence() {
		if (BuildConfig.TEST_RUN) {
			mListener.onPanelExists();
		}

		String hostAddress = validateAndGetHostAddress();

		if (!termsOfUseCheckBox.isChecked()) {
			setCheckBoxError(R.string.you_must_accept_the_terms);
		}
		final String panelId = validateAndGetPanelId();
		if (isLoginDataValid(hostAddress, panelId)) {
			mLoginPrefs.setHostAddress(hostAddress);
			mLoginPrefs.setPanelId(panelId);

			if (mDataManager != null) {
				mDataManager.removeCallbacks(this);
			}
			mDataManager = new LoginDataManager(mLoginPrefs);
			mDataManager.addCallbacks(this);

			if (rememberLoginInfo == ApplicationSettingsFragment.RememberLoginInfo.ALL) {
				PanelData panelData;
				panelData = realm.where(PanelData.class).equalTo(HOST_ADDRESS, hostAddress)
						.equalTo(PANEL_NAME, panelId).findFirst();
				if (panelData == null) {
					// TODO make thread safe
					realm.beginTransaction();
					PanelData localPanelData = realm.createObject(PanelData.class);
					localPanelData.setHostAddress(hostAddress);
					localPanelData.setPanelName(panelId);
					realm.commitTransaction();
				}
			}

			mDataManager.checkPanelAvailability(panelId);
		}
	}

	@Nullable
	private String validateAndGetPanelId() {
		String panelId;
		if (panelIdTextView.getVisibility() == View.VISIBLE) {
			panelId = panelIdTextView.getText().toString();
		} else {
			panelId = panelIdAutoCompleteTextView.getText().toString();
		}
		if (TextUtils.isEmpty(panelId)) {
			setPanelError(R.string.panel_id_is_missing);
			return null;
		} else if (panelId.length() > MAX_PANEL_ID_LENGTH) {
			setPanelError(R.string.incorrect_panel_format);
		}
		return panelId;
	}

	@Nullable
	private String validateAndGetHostAddress() {
		final String hostAddress;
		if (hostAddressTextView.getVisibility() == View.VISIBLE) {
			hostAddress = hostAddressTextView.getText().toString().toLowerCase();
		} else {
			hostAddress = hostAddressAutocompleteTextView.getText().toString().toLowerCase();
		}
		if (TextUtils.isEmpty(hostAddress)) {
			setHostError(R.string.host_address_is_missing);
			return null;
		}
		if (!Patterns.DOMAIN_NAME.matcher(hostAddress).matches()) {
			setHostError(R.string.incorrect_address_format);
			return null;
		}
		if (hostAddress.length() > MAX_HOST_ADDRESS_LENGTH) {
			setHostError(R.string.incorrect_address_format);
			return null;
		}
		return hostAddress;
	}

	@OnClick(R.id.termsOfUseTextView)
	public void showUserAgreement() {
		mListener.showUserAgreement();
	}

	private boolean isLoginDataValid(@Nullable String hostAddress, @Nullable String panelId) {
		return hostAddress != null
				&& !TextUtils.isEmpty(panelId)
				&& Patterns.DOMAIN_NAME.matcher(hostAddress).matches()
				&& termsOfUseCheckBox.isChecked()
				&& !TextUtils.isEmpty(panelId);
	}

	@Override
	public void dataStartedLoading() {
		new ProgressDialogFragment().show(getChildFragmentManager(), ProgressDialogFragment.TAG);
	}

	@Override
	public void dataFinishedLoading() {
		Fragment progressDialog = getChildFragmentManager().findFragmentByTag(ProgressDialogFragment.TAG);
		if (progressDialog != null) {
			((ProgressDialogFragment) progressDialog).dismiss();
		}
		BaseStatusDataManager.Result result = mDataManager.getResult();
		switch (result) {
			case OK:
				SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
				sharedPref.edit().putBoolean(TERMS_OF_USE_ACCEPTED, true).apply();
				mListener.onPanelExists();
				break;
			case UNEXPECTED_PROBLEM:
				break;
			case NETWORK_PROBLEM:
				titleTextView.setText(R.string.no_internet_access);
				break;
			case HTTP_PROBLEM:
				switch (mDataManager.getCode()) {
					case 445:
						setPanelError(R.string.wrong_panel_id);
						break;
					case 448:
						setPanelError(R.string.panel_is_not_authorized);
						break;
					case 443:
						setPanelError(R.string.session_limit_message);
						break;
					case 503:
						setHostError(R.string.host_is_unreachable);
						break;
				}
				break;
			case WRONG_PANEL_ID:
				setPanelError(R.string.wrong_panel_id);
				break;
			case HOST_NOT_ACCESSIBLE:
				setHostError(R.string.wrong_host_address);
				break;
		}
	}

	private void setHostError(@StringRes int message) {
		hostAddressAutocompleteTextView.setError(getString(message));
		hostAddressTextView.setError(getString(message));
		titleTextView.setText(message);
	}

	private void clearHostError() {
		hostAddressAutocompleteTextView.setError(null);
		hostAddressTextView.setError(null);
	}

	private void setPanelError(@StringRes int message) {
		panelIdAutoCompleteTextView.setError(getString(message));
		panelIdTextView.setError(getString(message));
		titleTextView.setText(message);
	}

	private void setCheckBoxError(@StringRes int message) {
		termsOfUseTextView.setError(getString(message));
		titleTextView.setText(message);
	}

	private void clearPanelError() {
		panelIdAutoCompleteTextView.setError(null);
		panelIdTextView.setError(null);
	}

	private class PanelDataByHostAdapter extends FilterableRealmBaseAdapter<PanelData> {

		public PanelDataByHostAdapter(Context context, @LayoutRes int layout,
									  RealmResults<PanelData> realmObjectList) {
			super(context, layout, realmObjectList);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = super.getView(position, convertView, parent);
			PanelData item = getItem(position);
			((TextView) view).setText(String.format("%s; %s", item.getHostAddress(), item.getPanelName()));
			return view;
		}

		@Override
		protected List<PanelData> performRealmFiltering(@NonNull CharSequence constraint,
														RealmResults<PanelData> results) {
			List<PanelData> filtered = new ArrayList<>();
			for (PanelData result : results) {
				if (result.getHostAddress().startsWith(constraint.toString())) {
					filtered.add(result);
				}
			}
			return filtered;
		}
	}

	private class PanelDataByPanelAdapter extends FilterableRealmBaseAdapter<PanelData> {

		public PanelDataByPanelAdapter(Context context, @LayoutRes int layout,
									   RealmResults<PanelData> realmObjectList) {
			super(context, layout, realmObjectList);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = super.getView(position, convertView, parent);
			PanelData item = getItem(position);
			((TextView) view).setText(String.format("%s; %s", item.getHostAddress(), item.getPanelName()));
			return view;
		}

		@Override
		protected List<PanelData> performRealmFiltering(@NonNull CharSequence constraint,
														RealmResults<PanelData> results) {
			List<PanelData> filtered = new ArrayList<>();
			for (PanelData result : results) {
				if (result.getPanelName().startsWith(constraint.toString())) {
					filtered.add(result);
				}
			}
			return filtered;
		}
	}
}
