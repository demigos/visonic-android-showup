package com.visonic.visonicalerts.ui.fragments.functionalfragments;


import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.visonic.visonicalerts.R;
import com.visonic.visonicalerts.data.DataLoadingSubject;
import com.visonic.visonicalerts.data.datamanager.ListDataManager;
import com.visonic.visonicalerts.data.datamanager.DevicesDataManager;
import com.visonic.visonicalerts.data.model.DeviceType;
import com.visonic.visonicalerts.ui.DevicesAdapter;
import com.visonic.visonicalerts.ui.FilterAdapter;
import com.visonic.visonicalerts.ui.FilterAdapter.Filter;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import butterknife.BindView;

public class DevicesFragment extends BaseFunctionalFragment implements DataLoadingSubject.DataLoadingCallbacks {
	private static final String TAG = "DevicesFragment";
	@BindView(R.id.drawer)
	DrawerLayout mDrawerLayout;
	@BindView(R.id.toolbar)
	Toolbar mToolbar;
	@BindView(R.id.list)
	RecyclerView mDevicesList;
	@BindView(R.id.filters)
	RecyclerView mFiltersMenu;

	private FilterAdapter<DeviceType> mFilterAdapter;
	private DevicesAdapter mDevicesAdapter;

	private DevicesDataManager mDataManager;
	private Handler mHandler = new Handler();

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
	}

	@Override
	protected int getLayoutId() {
		return R.layout.fragment_devices;
	}

	@NonNull
	@Override
	public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = super.onCreateView(inflater, container, savedInstanceState);
		List<FilterAdapter.Filter<? extends Set<DeviceType>>> filters = new ArrayList<>();
		Filter<EnumSet<DeviceType>> allFilter = new Filter<>(R.drawable.ic_all,
				getString(R.string.all), EnumSet.allOf(DeviceType.class));
		filters.add(allFilter);
		filters.add(new Filter<>(R.drawable.ic_detectors, getString(R.string.detectors),
				EnumSet.of(DeviceType.ZONE)));
		filters.add(new Filter<>(R.drawable.ic_keyfobs, getString(R.string.keyfobs),
				KeyfobsFragment.KEYFOB_TYPES));
		filters.add(new Filter<>(R.drawable.ic_sirens, getString(R.string.sirens),
				EnumSet.of(DeviceType.WL_SIREN)));
		filters.add(new Filter<>(R.drawable.ic_repeaters, getString(R.string.repeaters),
				EnumSet.of(DeviceType.REPEATER)));
		mFilterAdapter = new FilterAdapter<>(getActivity(), filters,
				new HashSet<DeviceType>(), allFilter, allFilter);

		mFiltersMenu.setAdapter(mFilterAdapter);
		mToolbar.inflateMenu(R.menu.filter_menu);
		mToolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
			@Override
			public boolean onMenuItemClick(MenuItem item) {
				if (item.getItemId() == R.id.menu_filter) {
					mDrawerLayout.openDrawer(GravityCompat.END);
					return true;
				}
				return false;
			}
		});
		mToolbar.setNavigationIcon(mUiUtils.getIconDrawable(R.drawable.ic_devices));

		mDataManager = new DevicesDataManager(mLoginPrefs);
		mDataManager.addCallbacks(this);
		mFilterAdapter.addFilterChangedListener(new FilterAdapter.FiltersChangedListener<DeviceType>() {
			@Override
			public void onFiltersChanged(Set<DeviceType> changedFilter, boolean hasVideo) {
				mDataManager.setFilters(changedFilter);
				mDataManager.performFiltering();
			}
		});

		mDevicesAdapter = new DevicesAdapter(mDataManager, getActivity(), mHandler, false);
		mDevicesList.setAdapter(mDevicesAdapter);
		return view;
	}

	@Nullable
	@Override
	protected ListDataManager<?> getDataManager() {
		return mDataManager;
	}

	@Override
	public void dataFinishedLoading() {
		super.dataFinishedLoading();
		mDevicesAdapter.notifyNewData();
	}
}