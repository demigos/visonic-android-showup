package com.visonic.visonicalerts.ui.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.visonic.visonicalerts.R;

import butterknife.BindView;

public class UserAgrrementFragment extends BaseFragment {
	@BindView(R.id.webView)
	WebView mWebView;

	@NonNull
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = super.onCreateView(inflater, container, savedInstanceState);
		mWebView.loadUrl("file:///android_asset/end_user_license_agreement.html");
		return view;
	}

	@Override
	protected int getLayoutId() {
		return R.layout.fragment_user_agrrement;
	}
}
