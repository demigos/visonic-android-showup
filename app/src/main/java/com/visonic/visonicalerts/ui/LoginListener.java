package com.visonic.visonicalerts.ui;

import android.support.annotation.StringRes;

public interface LoginListener {
	void onPanelExists();
	void onPinCorrect();
	void goBackToLoginWithError(@StringRes int errorTextRes);
	int getLastErrorTextRes();
	void showUserAgreement();
}
