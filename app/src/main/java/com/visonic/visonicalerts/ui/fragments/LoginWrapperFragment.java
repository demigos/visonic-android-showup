package com.visonic.visonicalerts.ui.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.visonic.visonicalerts.R;
import com.visonic.visonicalerts.ui.MainActivity;
import com.visonic.visonicalerts.ui.fragments.settings.ApplicationSettingsFragment;

import butterknife.BindView;

public class LoginWrapperFragment extends BaseFragment {
	private static final String MODE = "mode";
	private static final int LOGIN_SCREEN = 1;
	private static final int PIN_SCREEN = 2;
	private Fragment mFragment;
	@BindView(R.id.toolbar)
	Toolbar mToolbar;
	@BindView(R.id.drawer)
	DrawerLayout mDrawerLayout;
	@BindView(R.id.drawer_menu)
	NavigationView mDrawerNavigationView;

	@NonNull
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = super.onCreateView(inflater, container, savedInstanceState);
		int mode = getArguments().getInt(MODE);
		switch (mode) {
			case LOGIN_SCREEN:
				mFragment = new LoginFragment();
				break;
			case PIN_SCREEN:
				mFragment = new LoginUserCodeFragment();
				break;
		}
		mDrawerNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
			@Override
			public boolean onNavigationItemSelected(MenuItem item) {
				switch (item.getItemId()) {
					case R.id.general_item:
						replaceFragment(mFragment);
						break;
					case R.id.settings_item:
						replaceFragment(new ApplicationSettingsFragment());
						break;
					case R.id.show_tooltip_item:
						((MainActivity) getActivity()).showTooltip();
						break;
				}
				mDrawerLayout.closeDrawer(GravityCompat.START);
				return false;
			}
		});
		replaceFragment(mFragment);
		ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(getActivity(), mDrawerLayout,
				mToolbar, R.string.open_drawer, R.string.close_drawer);
		mDrawerLayout.addDrawerListener(toggle);
		mToolbar.setNavigationIcon(R.drawable.ic_menu_white_24dp);
		mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@SuppressLint("RtlHardcoded")
			@Override
			public void onClick(View v) {
				mDrawerLayout.openDrawer(Gravity.LEFT);
			}
		});
		return view;
	}

	public boolean isLoginScreen() {
		return getArguments().getInt(MODE) == LOGIN_SCREEN;
	}

	public Fragment getWrappedFragment() {
		return mFragment;
	}

	@Override
	protected int getLayoutId() {
		return R.layout.fragment_login_wraper;
	}

	private void replaceFragment(Fragment fragment) {
		getChildFragmentManager().beginTransaction()
				.replace(R.id.fragmentContainer, fragment)
				.commit();
	}

	public static LoginWrapperFragment createLoginInstance() {
		Bundle args = new Bundle();
		args.putInt(MODE, LOGIN_SCREEN);
		LoginWrapperFragment fragment = new LoginWrapperFragment();
		fragment.setArguments(args);
		return fragment;
	}

	public static LoginWrapperFragment createPinCodeInstance() {
		Bundle args = new Bundle();
		args.putInt(MODE, PIN_SCREEN);
		LoginWrapperFragment fragment = new LoginWrapperFragment();
		fragment.setArguments(args);
		return fragment;
	}
}
