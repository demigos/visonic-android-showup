package com.visonic.visonicalerts.ui;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.ListPopupWindow;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListAdapter;
import android.widget.Toast;

import com.visonic.visonicalerts.R;
import com.visonic.visonicalerts.data.datamanager.BaseStatusDataManager;
import com.visonic.visonicalerts.data.DataLoadingSubject;
import com.visonic.visonicalerts.data.datamanager.PollStateDataManager;
import com.visonic.visonicalerts.data.model.Status;
import com.visonic.visonicalerts.ui.fragments.MainFragment;
import com.visonic.visonicalerts.ui.fragments.settings.ApplicationSettingsFragment;

import java.util.HashMap;

public class TopPanelDelegate implements DataLoadingSubject.DataLoadingCallbacks, Armable {
	private static final String TAG = "TopPanelDelegate";

	public static final int ARM_AWAY = 0;
	public static final int ARM_INSTANT = 1;
	public static final int ARM_HOME = 0;
	public static final int ARM_HOME_INSTANT = 1;

	private final int blinkingTempColor;

	private AppCompatButton awayButton;
	private AppCompatButton homeButton;
	private AppCompatButton disarmButton;
	private View topPanel;
	private PollStateDataManager mDataManager;
	private MainFragment mMainFragment;
	private Fragment mFragment;
	private UiUtils mUiUtils;
	private Context mContext;

	private HashMap<ListAdapter, Point> listLayoutMeasurementsCache = new HashMap<>();

	public TopPanelDelegate(Fragment fragment) {
		mFragment = fragment;
		mMainFragment = (MainFragment) fragment.getParentFragment();
		mDataManager = mMainFragment.getStateDataManager();
		mDataManager.addCallbacks(this);
		mContext = fragment.getActivity();
		mUiUtils = new UiUtils(fragment.getActivity());

		blinkingTempColor = ContextCompat.getColor(mMainFragment.getContext(), R.color.selectedBlack);
	}

	public void initButton(View topPanel) {
		this.topPanel = topPanel;
		awayButton = (AppCompatButton) topPanel.findViewById(R.id.away_button);
		Drawable awayDrawable = mUiUtils.getIconDrawable(R.drawable.arm_away);
		awayButton.setCompoundDrawablesWithIntrinsicBounds(null, awayDrawable, null, null);
		homeButton = (AppCompatButton) topPanel.findViewById(R.id.home_button);
		Drawable homeDrawable = mUiUtils.getIconDrawable(R.drawable.arm_home);
		homeButton.setCompoundDrawablesWithIntrinsicBounds(null, homeDrawable, null, null);
		disarmButton = (AppCompatButton) topPanel.findViewById(R.id.disarm_button);
		Drawable disarmDrawable = mUiUtils.getIconDrawable(R.drawable.disarm);
		disarmButton.setCompoundDrawablesWithIntrinsicBounds(null, disarmDrawable, null, null);

		final ListPopupWindow armAwayPopup = createArmAwayPopup();
		awayButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (mMainFragment.isPanelReady()) {
					SharedPreferences sharedPref =
							PreferenceManager.getDefaultSharedPreferences(mContext);
					boolean armBySimpleToudh = sharedPref.getBoolean(
							ApplicationSettingsFragment.ARM_BY_SIMPLE_TOUCH, false);
					if (armBySimpleToudh) {
						armAway();
					} else {
						int xOffset = (awayButton.getMeasuredWidth() - armAwayPopup.getWidth()) / 2;
						armAwayPopup.setHorizontalOffset(xOffset);
						armAwayPopup.show();
						dimBackgroundActivity(armAwayPopup);
					}
				} else {
					showPanelErrorMessage();
				}
			}
		});
		final ListPopupWindow armHomePopup = createArmHomePopup();
		homeButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (mMainFragment.isPanelReady()) {
					SharedPreferences sharedPref =
							PreferenceManager.getDefaultSharedPreferences(mContext);
					boolean armBySimpleToudh = sharedPref.getBoolean(
							ApplicationSettingsFragment.ARM_BY_SIMPLE_TOUCH, false);
					if (armBySimpleToudh) {
						armHome();
					} else {
						int xOffset = (homeButton.getMeasuredWidth() - armHomePopup.getWidth()) / 2;
						armHomePopup.setHorizontalOffset(xOffset);
						armHomePopup.show();
						dimBackgroundActivity(armHomePopup);
					}
				} else {
					showPanelErrorMessage();
				}
			}
		});
		disarmButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				setActiveState(Status.State.ENTRY_DELAY);

				if (mMainFragment.getStateDataManager().getConnectedStatus() == Status.ConnectedStatus.CONNECTED) {
					mDataManager.disarm();
				} else {
					showPanelErrorMessage();
				}
			}
		});
		resetButtonsColor();
	}

	private void setActiveState(@NonNull Status.State state) {
		mMainFragment.setActiveState(state);
		dataFinishedLoading();
	}

	@NonNull
	private ListPopupWindow createArmAwayPopup() {
		Resources resources = mContext.getResources();
		String[] armAwayItems = resources.getStringArray(R.array.arm_away_items);
		final ListPopupWindow listPopupWindow = createListPopupWindow(awayButton, armAwayItems);
		listPopupWindow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				switch (position) {
					case ARM_AWAY:
						((Armable) mFragment).armAway();
						break;
					case ARM_INSTANT:
						((Armable) mFragment).armAwayInstant();
						break;
				}
				listPopupWindow.dismiss();
			}
		});
		return listPopupWindow;
	}

	@NonNull
	private ListPopupWindow createArmHomePopup() {
		Resources resources = mContext.getResources();
		String[] armAwayItems = resources.getStringArray(R.array.arm_home_items);
		final ListPopupWindow listPopupWindow = createListPopupWindow(homeButton, armAwayItems);
		listPopupWindow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				switch (position) {
					case ARM_HOME:
						((Armable) mFragment).armHome();
						break;
					case ARM_HOME_INSTANT:
						((Armable) mFragment).armHomeInstant();
						break;
				}
				listPopupWindow.dismiss();
			}
		});
		return listPopupWindow;
	}

	@NonNull
	private ListPopupWindow createListPopupWindow(@NonNull View anchor, String[] items) {
		DropDownlistAdapter adapter = new DropDownlistAdapter(mContext, items);
		Point measurements = getMeasurements(adapter);

		ListPopupWindow listPopupWindow = new ListPopupWindow(mContext);
		listPopupWindow.setBackgroundDrawable(ContextCompat.getDrawable(mContext, R.drawable.custom_dropdown));
		listPopupWindow.setDropDownGravity(Gravity.CENTER);
		listPopupWindow.setHeight(measurements.y * adapter.getCount());
		listPopupWindow.setWidth(measurements.x);
		listPopupWindow.setModal(true);
		listPopupWindow.setAdapter(adapter);
		listPopupWindow.setAnchorView(anchor);

		return listPopupWindow;
	}

	private void dimBackgroundActivity(ListPopupWindow listPopupWindow) {
		listPopupWindow.getListView().setDividerHeight(4);
		listPopupWindow.getListView().setDivider(ContextCompat.getDrawable(mContext, R.drawable.drawable_divider));
		final View container;
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			container = (View) listPopupWindow.getListView().getParent().getParent();
		} else {
			container = (View) listPopupWindow.getListView().getParent();
		}
		WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
		WindowManager.LayoutParams p = (WindowManager.LayoutParams) container.getLayoutParams();
		p.flags = WindowManager.LayoutParams.FLAG_DIM_BEHIND;
		p.dimAmount = 0.65f;
		wm.updateViewLayout(container, p);
	}

	private void showPanelErrorMessage() {
		if(mMainFragment.getStateDataManager().getConnectedStatus() == Status.ConnectedStatus.DISCONNECTED) {
			Toast.makeText(mFragment.getActivity(), R.string.panel_not_connected_message, Toast.LENGTH_SHORT).show();
		} else {
			Toast.makeText(mFragment.getActivity(), R.string.panel_not_ready, Toast.LENGTH_SHORT).show();
		}
	}


	public void clearUp() {
		mDataManager.removeCallbacks(this);
		mMainFragment = null;
	}

	public void dataStartedLoading() {
	}

	public void dataFinishedLoading() {
		if (mDataManager.getResult() == BaseStatusDataManager.Result.OK) {
			resetButtonsColor();
			if (mDataManager.getState() != null) {
				switch (mDataManager.getState()) {
					case AWAY:
						UiUtils.setButtonColor(awayButton, ContextCompat.getColor(mMainFragment.getContext(), R.color.arm_away_red));
						break;
					case HOME:
						UiUtils.setButtonColor(homeButton, ContextCompat.getColor(mMainFragment.getContext(), R.color.arm_home_yellow));
						break;
					case DISARM:
						UiUtils.setButtonColor(disarmButton, ContextCompat.getColor(mMainFragment.getContext(), R.color.arm_disarmed));
						break;
				}
			}
			if (mDataManager.getConnectedStatus() == Status.ConnectedStatus.CONNECTED) {
				if (mMainFragment.getPreviousState() != null
						&& mMainFragment.getPreviousState() == mDataManager.getState()) {
					setButtonBlinking(mMainFragment.getActiveState());
				} else {
					setButtonBlinking(mDataManager.getState());
				}
			} else {
				mMainFragment.setActiveState(null);
			}
		}
	}

	private void setButtonBlinking(Status.State activeState) {
		switch (activeState) {
			case ENTRY_DELAY:
				UiUtils.setButtonColor(disarmButton, blinkingTempColor);
				break;
			case EXIT_DELAY_AWAY:
				UiUtils.setButtonColor(awayButton, blinkingTempColor);
				break;
			case EXIT_DELAY_HOME:
				UiUtils.setButtonColor(homeButton, blinkingTempColor);
				break;
		}
	}

	private void resetButtonsColor() {
		UiUtils.setButtonColor(awayButton, mUiUtils.getTextColorPrimary());
		UiUtils.setButtonColor(homeButton, mUiUtils.getTextColorPrimary());
		UiUtils.setButtonColor(disarmButton, mUiUtils.getTextColorPrimary());
	}


	public void setTopPanelVisibility(boolean isTopPanelVisible) {
		if (isTopPanelVisible) {
			topPanel.setVisibility(View.VISIBLE);
		} else {
			topPanel.setVisibility(View.GONE);
		}
	}

	@Override
	public void armAway() {
		mDataManager.armAway();
		setActiveState(Status.State.EXIT_DELAY_AWAY);
	}

	@Override
	public void armAwayInstant() {
		mDataManager.armAwayInstant();
		setActiveState(Status.State.EXIT_DELAY_AWAY);
	}

	@Override
	public void armHome() {
		mDataManager.armHome();
		setActiveState(Status.State.EXIT_DELAY_HOME);
	}

	@Override
	public void armHomeInstant() {
		mDataManager.armHomeInstant();
		setActiveState(Status.State.EXIT_DELAY_HOME);
	}

	private Point getMeasurements(ListAdapter adapter) {
		if (listLayoutMeasurementsCache.get(adapter) == null) {
			ViewGroup mMeasureParent = null;
			int maxWidth = 0;
			int maxHeight = 0;
			View itemView = null;
			int itemType = 0;

			final int widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
			final int heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
			final int count = adapter.getCount();
			for (int i = 0; i < count; i++) {
				final int positionType = adapter.getItemViewType(i);
				if (positionType != itemType) {
					itemType = positionType;
					itemView = null;
				}

				if (mMeasureParent == null) {
					mMeasureParent = new FrameLayout(mContext);
				}

				itemView = adapter.getView(i, itemView, mMeasureParent);
				itemView.measure(widthMeasureSpec, heightMeasureSpec);

				final int itemWidth = itemView.getMeasuredWidth();
				final int itemHeight = itemView.getMeasuredHeight();

				if (itemWidth > maxWidth) {
					maxWidth = itemWidth;
				}
				if (itemHeight > maxHeight) {
					maxHeight = itemHeight;
				}
			}

			listLayoutMeasurementsCache.put(adapter, new Point(maxWidth, maxHeight));
		}
		return listLayoutMeasurementsCache.get(adapter);
	}
}
