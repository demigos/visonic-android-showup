package com.visonic.visonicalerts.ui.fragments;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.visonic.visonicalerts.R;
import com.visonic.visonicalerts.data.datamanager.BaseStatusDataManager.Result;
import com.visonic.visonicalerts.data.DataLoadingSubject;
import com.visonic.visonicalerts.data.datamanager.ListDataManager;
import com.visonic.visonicalerts.data.datamanager.PollStateDataManager;
import com.visonic.visonicalerts.data.model.Alarm;
import com.visonic.visonicalerts.data.model.ContentResponse;
import com.visonic.visonicalerts.data.model.DisableSirensMode;
import com.visonic.visonicalerts.data.model.DisableSirensModeWrapper;
import com.visonic.visonicalerts.data.model.Status;
import com.visonic.visonicalerts.data.prefs.LoginPrefs;
import com.visonic.visonicalerts.ui.AlarmsUiHelper;
import com.visonic.visonicalerts.ui.HasTopPanel;
import com.visonic.visonicalerts.ui.MainActivity;
import com.visonic.visonicalerts.ui.UiUtils;
import com.visonic.visonicalerts.ui.fragments.BottomMenuItemsFragment.BottomMenuItem;
import com.visonic.visonicalerts.ui.fragments.functionalfragments.AlarmsParentFragment;
import com.visonic.visonicalerts.ui.fragments.functionalfragments.AlertsFragment;
import com.visonic.visonicalerts.ui.fragments.functionalfragments.BaseFunctionalFragment;
import com.visonic.visonicalerts.ui.fragments.functionalfragments.CameraParentFragment;
import com.visonic.visonicalerts.ui.fragments.functionalfragments.DevicesFragment;
import com.visonic.visonicalerts.ui.fragments.functionalfragments.EventsParentFragment;
import com.visonic.visonicalerts.ui.fragments.functionalfragments.KeyfobsFragment;
import com.visonic.visonicalerts.ui.fragments.functionalfragments.StatsFragment;
import com.visonic.visonicalerts.ui.fragments.functionalfragments.TroublesFragment;
import com.visonic.visonicalerts.ui.fragments.settings.ApplicationSettingsFragment;
import com.visonic.visonicalerts.ui.fragments.settings.NotificationSettingsFragment;
import com.visonic.visonicalerts.ui.fragments.settings.UserAliasesSettingsFragment;
import com.visonic.visonicalerts.ui.views.BadgeView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import butterknife.BindView;

public class MainFragment extends BaseFragment implements View.OnClickListener,
		MainActivity.BackButtonListener,
		DataLoadingSubject.DataLoadingCallbacks,
		MainActivity.OnPermissionReceivedListener,
		PopupMenu.OnMenuItemClickListener,
		AlarmsUiHelper.AlarmsProvider {
	private static final String TAG = "MainFragment";
	private static final int BUTTON_MIN_WIDHT = 72;
	private final static BottomMenuItem moreItem =
			new BottomMenuItem("more_item", R.id.more_button, R.string.more, R.drawable.ic_more);
	private static final String ALARM_DATA = "alarm_data";

	@IdRes
	private int previousFragmentButtonId = R.id.statsButton;

	@BindView(R.id.toolbar)
	Toolbar mToolbar;
	@BindView(R.id.connection_status_icon)
	ImageView mConnectionStatusImageView;
	@BindView(R.id.connection_status_text_switcher)
	ViewFlipper mConnectionStatusTextSwitcher;
	@BindView(R.id.text1)
	TextView mTextView1;
	@BindView(R.id.text2)
	TextView mTextView2;
	@BindView(R.id.bottom_panel)
	LinearLayout bottomPanel;
	@BindView(R.id.top_panel_visibility_toggle_button)
	ImageView topPanelHandleImageView;
	@BindView(R.id.drawer)
	DrawerLayout mDrawerLayout;
	@BindView(R.id.drawer_menu)
	NavigationView mDrawerNavigationView;
	@BindView(R.id.logoutButton)
	AppCompatTextView mLogoutButton;

	private List<BottomMenuItem> itemsInPopup;
	private PollStateDataManager mStateDataManager;
	private boolean mIsTopPanelVisible = true;
	private Map<Class<? extends HasTopPanel>, Boolean> mTopPanelVisibilities = new HashMap<>();
	private AlarmsDataManager mAlarmDataManager;
	private long mTimerStartedTime;
	private Status.State mActiveState = null;
	private Status.State mPreviousState = null;
	private Status.State mCachedState = null;

	private List<BottomMenuItem> mMenuOrder;
	private final Set<Alarm> mCachedData = new HashSet<>();
	private BadgeView badgeView;
	private Status.State mLastTimerState;
	private boolean mDelayFromOtherDevice;
	private boolean wasSmsSent;
	private String mPreviousStatusText;
	private String mPreviousStatusText1;
	private String mPreviousStatusText2;

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		mStateDataManager = new PollStateDataManager(mLoginPrefs);
		mStateDataManager.addCallbacks(this);
		mAlarmDataManager = new AlarmsDataManager(mLoginPrefs);
		mAlarmDataManager.addCallbacks(this);
	}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		if (savedInstanceState != null) {
			ArrayList<Alarm> data = savedInstanceState.getParcelableArrayList(ALARM_DATA);
			mAlarmDataManager.setSaveData(data);
		}
		super.onCreate(savedInstanceState);
	}

	@Override
	protected int getLayoutId() {
		return R.layout.fragment_main;
	}

	@NonNull
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		final View view = super.onCreateView(inflater, container, savedInstanceState);

		final ViewTreeObserver viewTreeObserver = view.getViewTreeObserver();
		if (viewTreeObserver.isAlive()) {
			viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
				@Override
				public void onGlobalLayout() {
					if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
						view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
					}
					initBottomPanel();
				}
			});
		}

		topPanelHandleImageView.setOnClickListener(this);

		replaceFragment(new StatsFragment());
		mDrawerNavigationView.setCheckedItem(R.id.general_item);
		mDrawerNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
			@Override
			public boolean onNavigationItemSelected(MenuItem item) {
				mDrawerNavigationView.setCheckedItem(item.getItemId());
				switch (item.getItemId()) {
					case R.id.general_item:
						performActionFromId(previousFragmentButtonId);
						break;
					case R.id.settings_item:
						replaceFragment(new ApplicationSettingsFragment());
						break;
					case R.id.notifications_item:
						replaceFragment(new NotificationSettingsFragment());
						break;
					case R.id.users_item:
						replaceFragment(new UserAliasesSettingsFragment());
						break;
					case R.id.menu_order_item:
						resetButtonsColor();
						replaceFragment(new BottomMenuItemsFragment());
						break;
					case R.id.keyfobs_item:
						replaceFragment(new KeyfobsFragment());
						break;
					case R.id.show_tooltip_item:
						((MainActivity) getActivity()).showTooltip();
						break;
				}
				((MainActivity) getActivity()).updateLastActivityTime();
				mDrawerLayout.closeDrawer(GravityCompat.START);
				return false;
			}
		});
		Drawable drawable = mUiUtils.getIconDrawable(R.drawable.logout);
		mLogoutButton.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
		UiUtils.setListItemColor(mLogoutButton,
				ContextCompat.getColorStateList(getActivity(), R.color.drawer_text_color));
		mLogoutButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				new LogoutDialogFragment().show(getChildFragmentManager(), null);
			}
		});
		ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(getActivity(), mDrawerLayout,
				mToolbar, R.string.open_drawer, R.string.close_drawer);
		mDrawerLayout.addDrawerListener(toggle);
		mToolbar.setNavigationIcon(R.drawable.ic_menu_white_24dp);
		mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mDrawerLayout.openDrawer(GravityCompat.START);
			}
		});
		return view;
	}

	@Override
	public void onResume() {
		super.onResume();
		mStateDataManager.checkConnectionStatus();
		mStateDataManager.startPoling();
		mAlarmDataManager.startPolling();
	}

	@Override
	public void onPause() {
		super.onPause();
		mStateDataManager.stopPolling();
		mAlarmDataManager.stopPolling();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		outState.putParcelableArrayList(ALARM_DATA, mAlarmDataManager.getSaveData());
		super.onSaveInstanceState(outState);
	}

	@Override
	public boolean onBackButtonPressed() {
		Fragment fragment = getFragmentWithPanel();
		if (!(fragment instanceof MainActivity.BackButtonListener
				&& ((MainActivity.BackButtonListener) fragment).onBackButtonPressed())) {
			new LogoutDialogFragment().show(getChildFragmentManager(), null);
		}
		return true;
	}

	public boolean isPanelReady() {
		return mStateDataManager.getReadyStatus() == Status.ReadyStatus.READY;
	}

	@Override
	public void notifyAlarmsListener() {
		Fragment fragment = getFragmentWithPanel();
		if (mCachedData.size() == 0 && mAlarmDataManager.getData() != null
				&& mAlarmDataManager.getData().size() > 0) {
			mCachedData.clear();
			mCachedData.addAll(mAlarmDataManager.getData());
		}
		if (fragment instanceof AlarmsUiHelper.AlarmsListener) {
			((AlarmsUiHelper.AlarmsListener) fragment).onAlarmsUpdated(mAlarmDataManager.getData());
			mCachedData.clear();
			if (mAlarmDataManager.getData() != null && mAlarmDataManager.getData().size() > 0) {
				mCachedData.addAll(mAlarmDataManager.getData());
			}
		} else {
			int difference = 0;
			if (mAlarmDataManager.getData() != null) {
				for (Alarm alarm : mAlarmDataManager.getData()) {
					if (!mCachedData.contains(alarm)) {
						difference++;
					}
				}
			}
			if (isVisible() && difference != 0) {
				@SuppressWarnings("ConstantConditions")
				View alarmsButton = getView().findViewById(R.id.alarmsButton);
				if (alarmsButton != null) {
					if (badgeView == null) {
						badgeView = new BadgeView(getActivity(), alarmsButton);
					}
					badgeView.setText(String.valueOf(difference));
					badgeView.show();
				}
			}
		}
	}

	@Nullable
	private Fragment getFragmentWithPanel() {
		for (Fragment fragment : getChildFragmentManager().getFragments()) {
			if (fragment != null && fragment.isVisible()) {
				return fragment;
			}
		}
		return null;
	}

	public void initBottomPanel() {
		if (bottomPanel == null ) {
			return;
		}
		bottomPanel.removeAllViews();
		Resources r = getResources();
		float buttonMinWidth = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
				BUTTON_MIN_WIDHT, r.getDisplayMetrics());
		int panelWidth = bottomPanel.getWidth();
		int numberOfButtons = (int) (panelWidth / buttonMinWidth);
		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
		mMenuOrder = BottomMenuItemsFragment.getBottomMenuOrder(preferences);
		if (numberOfButtons < mMenuOrder.size()) {
			for (int i = 0; i < numberOfButtons - 1; i++) {
				bottomPanel.addView(getBottomMenuItemView(mMenuOrder.get(i)));
			}
			itemsInPopup = mMenuOrder.subList(numberOfButtons - 1, mMenuOrder.size());
			bottomPanel.addView(getBottomMenuItemView(moreItem));
		} else {
			for (int i = 0; i < mMenuOrder.size(); i++) {
				bottomPanel.addView(getBottomMenuItemView(mMenuOrder.get(i)));
			}
		}

		if (getView() != null && getView().findViewById(previousFragmentButtonId) != null) {
			AppCompatButton button = (AppCompatButton) getView().findViewById(previousFragmentButtonId);
			UiUtils.setButtonColor(button, mUiUtils.getHighlightColor());
		}
	}

	private AppCompatButton getBottomMenuItemView(BottomMenuItem item) {
		AppCompatButton button = (AppCompatButton) LayoutInflater.from(getActivity())
				.inflate(R.layout.bottom_panel_item, bottomPanel, false);
		button.setText(item.getTitle());
		Drawable drawable = ContextCompat.getDrawable(getActivity(), item.getIcon());
		assert drawable != null;
		drawable = DrawableCompat.wrap(drawable);
		drawable.mutate();
		DrawableCompat.setTint(drawable, mUiUtils.getTextColorPrimary());
		button.setCompoundDrawablesWithIntrinsicBounds(null, drawable, null, null);
		button.setId(item.getId());
		button.setOnClickListener(MainFragment.this);
		return button;
	}
	// Clickers

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.more_button) {
			PopupMenu popup = new PopupMenu(getActivity(), v);
			Menu menu = popup.getMenu();
			for (int i = 0; i < itemsInPopup.size(); i++) {
				BottomMenuItem menuItem = itemsInPopup.get(i);
				menu.add(1, menuItem.getId(), i + 1, menuItem.getTitle());
			}
			popup.setOnMenuItemClickListener(this);
			popup.show();
		} else if (v.getId() == R.id.top_panel_visibility_toggle_button) {
			mIsTopPanelVisible = !mIsTopPanelVisible;
			syncToolbarVisibility();
		} else {
			performActionFromId(v.getId());
		}
	}

	private void syncToolbarVisibility() {
		HasTopPanel visibleFunctionalFragment = (HasTopPanel) getFragmentWithPanel();
		if (visibleFunctionalFragment != null) {
			if (mIsTopPanelVisible) {
				topPanelHandleImageView.setImageResource(R.drawable.ic_keyboard_arrow_up_white_48dp);
			} else {
				topPanelHandleImageView.setImageResource(R.drawable.ic_keyboard_arrow_down_white_48dp);
			}

			visibleFunctionalFragment.setTopPanelVisibility(mIsTopPanelVisible);
			visibleFunctionalFragment.dataFinishedLoading();
		}
	}


	@Override
	public boolean onMenuItemClick(MenuItem item) {
		return performActionFromId(item.getItemId());
	}

	public boolean performActionFromId(@IdRes int id) {
		return performActionFromId(id, null);
	}

	public boolean performActionFromId(@IdRes int id, Bundle bundle) {
		((MainActivity) getActivity()).updateLastActivityTime();
		previousFragmentButtonId = id;
		resetButtonsColor();
		if (getView() != null && getView().findViewById(id) != null) {
			AppCompatButton button = (AppCompatButton) getView().findViewById(id);
			UiUtils.setButtonColor(button, mUiUtils.getHighlightColor());
		}
		switch (id) {
			case R.id.alarmsButton:
				if (badgeView != null) {
					badgeView.setVisibility(View.GONE);
				}
				replaceFragment(new AlarmsParentFragment());
				return true;
			case R.id.devicesButton:
				replaceFragment(new DevicesFragment());
				return true;
			case R.id.camerasButton:
				replaceFragment(new CameraParentFragment());
				return true;
			case R.id.eventsButton:
				replaceFragment(EventsParentFragment.newInstance(bundle));
				return true;
			case R.id.alertsButton:
				replaceFragment(new AlertsFragment());
				return true;
			case R.id.troublesButton:
				replaceFragment(new TroublesFragment());
				return true;
			case R.id.keyfobsButton:
				replaceFragment(new KeyfobsFragment());
				return true;
			case R.id.statsButton:
				if (isTimerTicking()) {
					showTimerFragment(mLastTimerState, true);
				} else {
					replaceFragment(new StatsFragment());
				}
				return true;

		}
		throw new RuntimeException("Unknown id:" + id);
	}

	private void resetButtonsColor() {
		for (BottomMenuItem mBottomMenuItem : mMenuOrder) {
			if (getView() != null && getView().findViewById(mBottomMenuItem.getId()) != null) {
				AppCompatButton button = (AppCompatButton) getView().findViewById(mBottomMenuItem.getId());
				UiUtils.setButtonColor(button, mUiUtils.getTextColorPrimary());
			}
		}
	}

	public PollStateDataManager getStateDataManager() {
		return mStateDataManager;
	}

	public void requestSyncToolbarVisibility() {
		syncToolbarVisibility();
	}

	@Override
	public void dataStartedLoading() {
		// TODO: 2/15/16 implement
	}

	@Override
	public void dataFinishedLoading() {
		if (isVisible()) {
			mConnectionStatusTextSwitcher.setOnClickListener(null);
			notifyAlarmsListener();
			mUiUtils.handleError(mAlarmDataManager.getResult());
			if (mStateDataManager.getResult() == Result.NETWORK_PROBLEM
					|| mAlarmDataManager.getResult() == Result.NETWORK_PROBLEM) {
				mConnectionStatusImageView.setImageResource(R.drawable.disconnected_icon);
				setOneStatusText(getString(R.string.disconnected));
			}
			//noinspection StatementWithEmptyBody
			if (mStateDataManager.getResult() == Result.OK) {
				if (mCachedState != mStateDataManager.getState()) {
					if (mCachedState != null && mStateDataManager != null) {
						switch (mStateDataManager.getState()) {
							case DISARM:
								Toast.makeText(getActivity(), R.string.panel_is_disarmed, Toast.LENGTH_SHORT).show();
								break;
							case AWAY:
								Toast.makeText(getActivity(), R.string.panel_is_armed_away, Toast.LENGTH_SHORT).show();
								break;
							case HOME:
								Toast.makeText(getActivity(), R.string.panel_is_armed_home, Toast.LENGTH_SHORT).show();
								break;
							case EXIT_DELAY_AWAY:
							case EXIT_DELAY_HOME:
							case ENTRY_DELAY:
								Toast.makeText(getActivity(), R.string.panel_state_was_changed, Toast.LENGTH_SHORT).show();
								break;
							case UNKNOWN:
								break;
						}
					}
					mCachedState = mStateDataManager.getState();
				}
				Status.State currentState = mStateDataManager.getState();
				if (mStateDataManager.getConnectedStatus() == Status.ConnectedStatus.CONNECTED) {
					if (currentState == Status.State.UNKNOWN) {
						if (mStateDataManager.getReadyStatus() == Status.ReadyStatus.READY) {
							mConnectionStatusImageView.setImageResource(R.drawable.connecting_icon);
							setTwoStatusTexts(getString(R.string.connected), getString(R.string.not_ready));
						} else {
							// TODO: 2/18/16 clear up this state
							mConnectionStatusImageView.setImageResource(R.drawable.connecting_icon);
							setOneStatusText(getString(R.string.disconnected));
						}
					} else {
						if (mStateDataManager.getReadyStatus() == Status.ReadyStatus.NOT_READY) {
							mConnectionStatusImageView.setImageResource(R.drawable.connecting_icon);
							setTwoStatusTexts(getString(R.string.connected), getString(R.string.not_ready));
						} else if (mStateDataManager.getReadyStatus() == Status.ReadyStatus.READY) {
							mConnectionStatusImageView.setImageResource(R.drawable.connected_icon);
							setOneStatusText(getString(R.string.connected));
						} else {
							throw new IllegalStateException();
						}
					}
					mToolbar.setTitle(currentState.getStringId());
					if (currentState == Status.State.EXIT_DELAY_AWAY
							|| currentState == Status.State.EXIT_DELAY_HOME) {
						if (!isTimerTicking() && (getFragmentWithPanel() instanceof StatsFragment
								|| getFragmentWithPanel() instanceof TimerFragment)) {
							switch (currentState) {
								case EXIT_DELAY_AWAY:
									mLastTimerState = currentState;
									replaceFragment(TimerFragment.createInstance(getString(R.string.arm_away)));
									mDelayFromOtherDevice = true;
									break;
								case EXIT_DELAY_HOME:
									mLastTimerState = currentState;
									replaceFragment(TimerFragment.createInstance(getString(R.string.arm_home)));
									mDelayFromOtherDevice = true;
									break;
							}
						}
					}
				} else {
					mConnectionStatusImageView.setImageResource(R.drawable.disconnected_icon);
					if (mStateDataManager.getWakeUpPhone() == null) {
						setOneStatusText(getString(R.string.connecting));
					} else {
						// need to send SMS only one timegit
						if (!wasSmsSent) {
							setOneStatusText(getString(R.string.tap_to_connect));
							mConnectionStatusTextSwitcher.setOnClickListener(new View.OnClickListener() {
								@Override
								public void onClick(View v) {
									// TODO Validate SMS permission at runtime
									int permissionCheck = ContextCompat.checkSelfPermission(getActivity(),
											Manifest.permission.SEND_SMS);
									if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
										onPermissionReceived(MainActivity.SEND_SMS_REQUEST_ID);
									} else {
										ActivityCompat.requestPermissions(getActivity(),
												new String[]{Manifest.permission.SEND_SMS},
												MainActivity.SEND_SMS_REQUEST_ID);
									}
								}
							});
						}
					}
				}
				if ((mPreviousState == null && (currentState != Status.State.EXIT_DELAY_AWAY
						&& currentState != Status.State.EXIT_DELAY_HOME)) || mActiveState == Status.State.ENTRY_DELAY) {
					hideTimer();
				}
				if (mPreviousState != null && mPreviousState != currentState) {
					if (!isTimerTicking() || mLastTimerState != currentState) {
						showTimerFragment(mActiveState, false);
					}
					mActiveState = null;
					mPreviousState = null;
				}
			} else {
				// TODO: 2/22/16 handle failure
			}
			mStateDataManager.setTimerTicking(isTimerTicking());
		}
	}

	private void hideTimer() {
		if (getFragmentWithPanel() instanceof TimerFragment) {
			performActionFromId(R.id.statsButton);
		}
		mDelayFromOtherDevice = false;
		mTimerStartedTime = 0;
	}

	private void showTimerFragment(Status.State status, boolean force) {
		if (status == Status.State.EXIT_DELAY_AWAY
				|| status == Status.State.EXIT_DELAY_HOME) {
			if (!isTimerTicking() || mLastTimerState != status) {
				mTimerStartedTime = System.currentTimeMillis();
			}
			mLastTimerState = status;
		}
		if (getFragmentWithPanel() instanceof StatsFragment
				|| getFragmentWithPanel() instanceof TimerFragment
				|| force) {
			switch (status) {
				case EXIT_DELAY_AWAY:
					replaceFragment(TimerFragment.createInstance(getString(R.string.arm_away)));
					break;
				case EXIT_DELAY_HOME:
					replaceFragment(TimerFragment.createInstance(getString(R.string.arm_home)));
					break;
				case ENTRY_DELAY:
					break;
				default:
					throw new IllegalArgumentException("Only EXIT_DELAY_AWAY and EXIT_DELAY_HOME " +
							"states permitted, got:" + status);
			}
		}
	}

	public Status.State getActiveState() {
		return mActiveState;
	}

	public Status.State getPreviousState() {
		return mPreviousState;
	}

	private boolean isTimerTicking() {
		return getTimerTime() > 0;
	}

	public void setActiveState(Status.State activeState) {
		mActiveState = activeState;
		if (activeState == null) {
			mPreviousState = null;
		} else {
			mPreviousState = mStateDataManager.getState();
		}
	}

	public void replaceFragment(Fragment fragment) {
		if (fragment instanceof PreferenceFragmentCompat) {
			topPanelHandleImageView.setVisibility(View.GONE);
			mIsTopPanelVisible = false;
			bottomPanel.setVisibility(View.GONE);
		} else if (fragment instanceof BottomMenuItemsFragment) {
			topPanelHandleImageView.setVisibility(View.GONE);
			mIsTopPanelVisible = false;
			bottomPanel.setVisibility(View.GONE);
		} else {
			HasTopPanel localFragment = (HasTopPanel) fragment;
			if (!(fragment instanceof TimerFragment) && !isTimerTicking()) {
				mTimerStartedTime = 0;
			}
			mIsTopPanelVisible = localFragment.getDefaultTopPanelVisibility();

			topPanelHandleImageView.setVisibility(View.VISIBLE);
			bottomPanel.setVisibility(View.VISIBLE);
		}
		getChildFragmentManager().beginTransaction()
				.replace(R.id.fragmentContainer, fragment)
				.commit();
	}

	@Override
	public boolean onPermissionReceived(int permissionId) {
		if (permissionId == MainActivity.SEND_SMS_REQUEST_ID && isVisible()) {
			setOneStatusText(getString(R.string.connecting));
			try {
				SmsManager smsManager = SmsManager.getDefault();
				smsManager.sendTextMessage(mStateDataManager.getWakeUpPhone(), null,
						mStateDataManager.getWakeUpSms(), null, null);
				setOneStatusText(getString(R.string.connecting));
				wasSmsSent = true;
			} catch (Exception e) {
				Toast.makeText(getActivity(), R.string.failed_sending_sms, Toast.LENGTH_SHORT).show();
			}
			return true;
		}
		return false;
	}

	public void setOneStatusText(String statusText) {
		if (!statusText.equals(mPreviousStatusText)) {
			mPreviousStatusText1 = null;
			mPreviousStatusText2 = null;
			mPreviousStatusText = statusText;
			mTextView1.setText(statusText);
			mConnectionStatusTextSwitcher.setDisplayedChild(0);
			mConnectionStatusTextSwitcher.stopFlipping();
		}
	}

	public void setTwoStatusTexts(String statusText1, String statusText2) {
		if (!statusText1.equals(mPreviousStatusText1) || !statusText2.equals(mPreviousStatusText2)) {
			mPreviousStatusText1 = statusText1;
			mPreviousStatusText2 = statusText2;
			mPreviousStatusText = null;
			mTextView1.setText(statusText1);
			mTextView2.setText(statusText2);
			mConnectionStatusTextSwitcher.setDisplayedChild(0);
			mConnectionStatusTextSwitcher.startFlipping();
		}
	}

	public void muteSirens() {
		mAlarmDataManager.muteSirens();
	}

	public int getTimerTime() {
		long startTime = mTimerStartedTime;
		long delta = System.currentTimeMillis() - startTime;
		return mStateDataManager.getExitDelay() - Math.round(delta / 1000);
	}

	public boolean isDelayFromOtherDevice() {
		return mDelayFromOtherDevice;
	}

	public void startVideoOnDemandForCamera(int cameraId) {
		Bundle args = new Bundle();
		args.putInt(CameraParentFragment.CAMERA_ID, cameraId);
		CameraParentFragment fragment = new CameraParentFragment();
		fragment.setArguments(args);
		replaceFragment(fragment);
	}

	public static class TimerFragment extends BaseFunctionalFragment {
		private static final String TITLE = "title";

		Handler handler = new Handler();
		TextView timerText;

		@NonNull
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			View view = super.onCreateView(inflater, container, savedInstanceState);
			timerText = (TextView) view.findViewById(R.id.timer_text);
			timerText.setText(formatTime(getTimerTime()));
			final TextView title = (TextView) view.findViewById(R.id.title);
			title.setText(getArguments().getString(TITLE));
			handler.postDelayed(new Runnable() {
				@Override
				public void run() {
					int timerTime = getTimerTime();
					if (timerTime > 0) {
						handler.postDelayed(this, 1000);
					} else {
						timerText.setText("");
						if (!((MainFragment) getParentFragment()).isDelayFromOtherDevice()) {
							title.setText("");
						}
					}
					timerText.setText(formatTime(timerTime));
				}
			}, 1000);
			return view;
		}

		public int getTimerTime() {
			return ((MainFragment) getParentFragment()).getTimerTime();
		}

		@Override
		protected int getLayoutId() {
			return R.layout.fragment_timer;
		}

		@Override
		public boolean getDefaultTopPanelVisibility() {
			return true;
		}

		private static String formatTime(int time) {
			if (time > 0) {
				int minutes = time / 60;
				int seconds = time % 60;
				return String.valueOf(String.format(Locale.UK, "%d:%02d", minutes, seconds));
			} else {
				return "";
			}
		}

		public static TimerFragment createInstance(String title) {
			Bundle args = new Bundle();
			args.putString(TITLE, title);
			TimerFragment timerFragment = new TimerFragment();
			timerFragment.setArguments(args);
			return timerFragment;
		}
	}

	private static class AlarmsDataManager extends ListDataManager<Alarm> {
		public AlarmsDataManager(LoginPrefs loginPrefs) {
			super(loginPrefs);
		}

		@Override
		protected void visonicServiceCall(SimpleListCallback<Alarm> callback) {
			getVisonicService().alarms(mLoginPrefs.getSessionToken()).enqueue(callback);
		}

		@Override
		protected Class<Alarm> getDataClass() {
			return Alarm.class;
		}

		public ArrayList<Alarm> getSaveData() {
			if (getData() != null) {
				return new ArrayList<>(getData());
			} else {
				return new ArrayList<>();
			}
		}

		public void setSaveData(ArrayList<Alarm> data) {
			mFilteredData = data;
		}

		private void muteSirens() {
			getVisonicService().disableSiren(mLoginPrefs.getSessionToken(),
					new DisableSirensModeWrapper(DisableSirensMode.ALL))
					.enqueue(new BaseCallback<ContentResponse>());
		}
	}
}
