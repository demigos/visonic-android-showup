package com.visonic.visonicalerts.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.visonic.visonicalerts.data.prefs.LoginPrefs;
import com.visonic.visonicalerts.ui.UiUtils;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public abstract class BaseFragment extends Fragment {
	protected LoginPrefs mLoginPrefs;
	protected UiUtils mUiUtils;
	private Unbinder mUnbinder;

	@Override
	@CallSuper
	public void onAttach(Context context) {
		super.onAttach(context);
		mLoginPrefs = LoginPrefs.getInstance(getActivity());
		mUiUtils = new UiUtils(context);
	}

	@Override
	@CallSuper
	@NonNull
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View view = inflater.inflate(getLayoutId(), container, false);
		mUnbinder = ButterKnife.bind(this, view);
		return view;
	}

	@Override
	@CallSuper
	public void onDestroyView() {
		super.onDestroyView();
		if (mUnbinder != null) {
			mUnbinder.unbind();
		}
	}

	@LayoutRes
	protected abstract int getLayoutId();
}
