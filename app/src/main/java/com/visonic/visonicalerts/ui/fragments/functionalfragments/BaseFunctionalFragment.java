package com.visonic.visonicalerts.ui.fragments.functionalfragments;

import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.visonic.visonicalerts.R;
import com.visonic.visonicalerts.data.DataLoadingSubject;
import com.visonic.visonicalerts.data.datamanager.ListDataManager;
import com.visonic.visonicalerts.ui.Armable;
import com.visonic.visonicalerts.ui.HasTopPanel;
import com.visonic.visonicalerts.ui.TopPanelDelegate;
import com.visonic.visonicalerts.ui.fragments.BaseFragment;
import com.visonic.visonicalerts.ui.fragments.MainFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public abstract class BaseFunctionalFragment extends BaseFragment
		implements DataLoadingSubject.DataLoadingCallbacks, Armable, HasTopPanel {

	@Nullable
	private TopPanelDelegate mTopPanelDelegate;
	@Nullable
	@BindView(R.id.circular_progress_bar)
	ProgressBar mProgressBar;

	@Override
	@CallSuper
	@NonNull
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		ViewGroup commonLayout = (ViewGroup) inflater.inflate(R.layout.top_panel, container, false);
		View view = inflater.inflate(getLayoutId(), commonLayout);
		ButterKnife.bind(this, commonLayout);
		mTopPanelDelegate = new TopPanelDelegate(this);
		mTopPanelDelegate.initButton(commonLayout.findViewById(R.id.top_panel));
		return view;
	}

	@Override
	public void onResume() {
		super.onResume();
		((MainFragment) getParentFragment()).requestSyncToolbarVisibility();
		if (getDataManager() != null) {
			getDataManager().startPolling();
		}
		syncProgressState();
	}

	@Override
	public void onPause() {
		super.onPause();
		if (getDataManager() != null) {
			getDataManager().stopPolling();
		}
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		assert mTopPanelDelegate != null;
		mTopPanelDelegate.clearUp();
		mTopPanelDelegate = null;
	}

	@Nullable
	protected ListDataManager<?> getDataManager(){
		return null;
	}

	@Override
	public void dataStartedLoading() {
		// TODO: 2/15/16 implement
		if (mTopPanelDelegate != null) {
			mTopPanelDelegate.dataStartedLoading();
		}
	}

	@Override
	@CallSuper
	public void dataFinishedLoading() {
		if (getDataManager() != null) {
			mUiUtils.handleError(getDataManager().getResult());
		}
		if (mTopPanelDelegate != null) {
			mTopPanelDelegate.dataFinishedLoading();
		}
		syncProgressState();
	}

	@Override
	public void setTopPanelVisibility(boolean isTopPanelVisible) {
		if (mTopPanelDelegate != null) {
			mTopPanelDelegate.setTopPanelVisibility(isTopPanelVisible);
		}
	}

	@Override
	public boolean getDefaultTopPanelVisibility() {
		return false;
	}

	@Override
	public void armAway() {
		if (mTopPanelDelegate != null) {
			mTopPanelDelegate.armAway();
		}
	}

	@Override
	public void armAwayInstant() {
		if (mTopPanelDelegate != null) {
			mTopPanelDelegate.armAwayInstant();
		}
	}

	@Override
	public void armHome() {
		if (mTopPanelDelegate != null) {
			mTopPanelDelegate.armHome();
		}
	}

	@Override
	public void armHomeInstant() {
		if (mTopPanelDelegate != null) {
			mTopPanelDelegate.armHomeInstant();
		}
	}

	private void syncProgressState() {
		if (getDataManager() != null && mProgressBar != null && isVisible()) {
			mProgressBar.setVisibility(getDataManager().getData() == null ? View.VISIBLE : View.GONE);
		}
	}
}
