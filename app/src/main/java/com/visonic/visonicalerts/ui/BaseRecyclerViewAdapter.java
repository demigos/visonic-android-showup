package com.visonic.visonicalerts.ui;

import android.support.annotation.CallSuper;
import android.support.annotation.LayoutRes;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

public abstract class BaseRecyclerViewAdapter
		<T, VH extends RecyclerView.ViewHolder & DataBindable<T>>
		extends RecyclerView.Adapter<VH> {
	private List<T> mData;

	@Override
	final public VH onCreateViewHolder(ViewGroup parent, int viewType) {
		return getNewViewHolder(parent);
	}

	@Override
	final public void onBindViewHolder(VH holder, int position) {
		holder.bind(mData.get(position));
	}

	@Override
	final public int getItemCount() {
		return mData == null ? 0 : mData.size();
	}

	@CallSuper
	public void setData(List<T> data) {
		this.mData = data;
		notifyDataSetChanged();
	}

	public List<T> getEditableData() {
		return mData;
	}

	protected abstract VH getNewViewHolder(ViewGroup parent);

	final protected View getViewFromLayout(@LayoutRes int layoutId, ViewGroup parent) {
		return LayoutInflater.from(parent.getContext())
				.inflate(layoutId, parent, false);
	}

}
