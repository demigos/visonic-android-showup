package com.visonic.visonicalerts.ui.fragments.functionalfragments;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.visonic.visonicalerts.R;
import com.visonic.visonicalerts.data.DataLoadingSubject;
import com.visonic.visonicalerts.data.datamanager.ListDataManager;
import com.visonic.visonicalerts.data.model.Alert;
import com.visonic.visonicalerts.ui.BaseRecyclerViewAdapter;
import com.visonic.visonicalerts.ui.DataBindable;

import butterknife.BindView;

public class AlertsFragment extends BaseFunctionalFragment
		implements DataLoadingSubject.DataLoadingCallbacks {
	private ListDataManager<Alert> mDataManager;
	@BindView(R.id.list)
	RecyclerView mAlertsList;
	@BindView(R.id.empty_list_view)
	TextView emptyListView;
	@BindView(R.id.toolbar)
	Toolbar mToolbar;
	private AlertsAdapter mAlertsAdapter = new AlertsAdapter();

	@NonNull
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View view = super.onCreateView(inflater, container, savedInstanceState);
		mDataManager = new ListDataManager<Alert>(mLoginPrefs) {
			@Override
			protected void visonicServiceCall(SimpleListCallback<Alert> callback) {
				getVisonicService().alerts(mLoginPrefs.getSessionToken()).enqueue(callback);
			}

			@Override
			protected Class<Alert> getDataClass() {
				return Alert.class;
			}
		};
		mDataManager.addCallbacks(this);
		mAlertsList.setAdapter(mAlertsAdapter);
		Drawable icon = mUiUtils.getIconDrawable(R.drawable.ic_alerts_bold);
		mToolbar.setNavigationIcon(icon);
		return view;
	}

	@Override
	protected int getLayoutId() {
		return R.layout.fragment_alerts;
	}

	@Override
	protected ListDataManager<?> getDataManager() {
		return mDataManager;
	}

	@Override
	public void dataFinishedLoading() {
		super.dataFinishedLoading();
		if (mDataManager.getData() != null && isAdded()) {
			boolean listEmpty = mDataManager.getData().size() == 0;
			emptyListView.setVisibility(listEmpty ? View.VISIBLE : View.GONE);
			mAlertsAdapter.setData(mDataManager.getData());
		}
	}

	private static class AlertsAdapter extends BaseRecyclerViewAdapter<Alert, AlertsAdapter.AlertsViewHolder> {

		@Override
		protected AlertsViewHolder getNewViewHolder(ViewGroup parent) {
			return new AlertsViewHolder(getViewFromLayout(R.layout.left_and_right_text_list_item, parent));
		}

		public static class AlertsViewHolder extends RecyclerView.ViewHolder
				implements DataBindable<Alert> {
			private TextView leftText;
			private TextView rightText;

			public AlertsViewHolder(View itemView) {
				super(itemView);
				leftText = (TextView) itemView.findViewById(R.id.left_text);
				rightText = (TextView) itemView.findViewById(R.id.right_text);
			}

			public void bind(Alert alert) {
				leftText.setText(String.valueOf(alert.device_type));
				rightText.setText(String.valueOf(alert.alert_type));
			}
		}
	}
}
