package com.visonic.visonicalerts.ui.fragments.functionalfragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.visonic.visonicalerts.R;
import com.visonic.visonicalerts.data.DataLoadingSubject;
import com.visonic.visonicalerts.data.datamanager.ListDataManager;
import com.visonic.visonicalerts.data.model.Alarm;
import com.visonic.visonicalerts.data.model.Alert;
import com.visonic.visonicalerts.data.model.Event;
import com.visonic.visonicalerts.data.model.Trouble;
import com.visonic.visonicalerts.ui.AlarmsUiHelper;
import com.visonic.visonicalerts.ui.fragments.MainFragment;
import com.visonic.visonicalerts.ui.fragments.settings.ApplicationSettingsFragment;
import com.visonic.visonicalerts.ui.fragments.settings.UserAliasesSettingsFragment;

import java.text.DateFormat;
import java.text.MessageFormat;
import java.util.BitSet;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;

public class StatsFragment extends BaseFunctionalFragment implements AlarmsUiHelper.AlarmsListener, View.OnClickListener {
    private static final String TAG = "StatsFragment";

    @BindView(R.id.fragment_stats_troubles_count)
    TextView troublesCountView;
    @BindView(R.id.fragment_stats_alarms_count)
    TextView alarmsCountView;
    @BindView(R.id.fragment_stats_alerts_count)
    TextView alertsCountView;
    @BindView(R.id.fragment_stats_last_event_action)
    TextView lastEventAction;
    @BindView(R.id.fragment_stats_last_event_user)
    TextView lastEventUser;
    @BindView(R.id.fragment_stats_last_event_time)
    TextView lastEventTime;
    @BindView(R.id.fragment_stats_all_is_well_container)
    TextView allIsWellContainer;
    @BindView(R.id.fragment_stats_panel_name)
    TextView panelName;

    @BindView(R.id.fragment_stats_troubles_count_card)
    GridLayout troublesCountCard;
    @BindView(R.id.fragment_stats_alerts_count_card)
    GridLayout alertsCountCard;
    @BindView(R.id.fragment_stats_alarms_count_card)
    GridLayout alarmsCountCard;

    @BindView(R.id.fragment_stats_stats_container)
    LinearLayout statsContainer;
    @BindView(R.id.fragment_stats_last_event)
    LinearLayout lastEvent;

    @BindView(R.id.fragment_stats_alarms_image)
    ImageView alarmsImage;
    @BindView(R.id.fragment_stats_alerts_image)
    ImageView alertsImage;
    @BindView(R.id.fragment_stats_troubles_image)
    ImageView troublesImage;

    @BindView(R.id.fragment_stats_last_event_loading)
    View lastEventLoading;
    @BindView(R.id.fragment_stats_stats_loading)
    View statsLoading;

    @BindView(R.id.fragment_stats_view_all)
    Button viewAll;

    private ListDataManager<Trouble> troubleListDataManager;
    private ListDataManager<Alert> alertsListDataManager;
    private ListDataManager<Event> eventsListDataManager;
    private int alarmsCount = -1;
    private int troublesCount = -1;
    private int alertsCount = -1;
    private boolean eventFirstLoad = true;
    private final BitSet eventsStatus = new BitSet(3);

    @NonNull
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = super.onCreateView(inflater, container, savedInstanceState);
        panelName.setText(MessageFormat.format(getString(R.string.format_logged_into_panel), mLoginPrefs.getPanelId()));
        troubleListDataManager = new ListDataManager<Trouble>(mLoginPrefs) {
            @Override
            protected void visonicServiceCall(SimpleListCallback<Trouble> callback) {
                getVisonicService().troubles(mLoginPrefs.getSessionToken()).enqueue(callback);
            }

            @Nullable
            @Override
            protected Class<Trouble> getDataClass() {
                return Trouble.class;
            }
        };
        troubleListDataManager.addCallbacks(new TroublesLoadedCallback());

        alertsListDataManager = new ListDataManager<Alert>(mLoginPrefs) {
            @Override
            protected void visonicServiceCall(SimpleListCallback<Alert> callback) {
                getVisonicService().alerts(mLoginPrefs.getSessionToken()).enqueue(callback);
            }

            @Nullable
            @Override
            protected Class<Alert> getDataClass() {
                return Alert.class;
            }
        };
        alertsListDataManager.addCallbacks(new AlertsLoadedCallback());

        eventsListDataManager = new ListDataManager<Event>(mLoginPrefs) {
            @Override
            protected void visonicServiceCall(SimpleListCallback<Event> callback) {
                getVisonicService().events(mLoginPrefs.getSessionToken()).enqueue(callback);
            }

            @Nullable
            @Override
            protected Class<Event> getDataClass() {
                return Event.class;
            }
        };
        eventsListDataManager.addCallbacks(new EventsLoadedCallback());

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((AlarmsUiHelper.AlarmsProvider) getParentFragment()).notifyAlarmsListener();

        alertsListDataManager.startPolling();
        troubleListDataManager.startPolling();
        eventsListDataManager.startPolling();

        troublesCountCard.setOnClickListener(this);
        alertsCountCard.setOnClickListener(this);
        alarmsCountCard.setOnClickListener(this);
        viewAll.setOnClickListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        troubleListDataManager.stopPolling();
        alertsListDataManager.stopPolling();
        eventsListDataManager.stopPolling();

        troublesCountCard.setOnClickListener(null);
        alertsCountCard.setOnClickListener(null);
        alarmsCountCard.setOnClickListener(null);
        viewAll.setOnClickListener(null);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_stats;
    }

    @Override
    public boolean getDefaultTopPanelVisibility() {
        return true;
    }

    @Override
    public void onAlarmsUpdated(List<Alarm> alarms) {
        alarmsCount = alarms != null ? alarms.size() : 0;

        alarmsImage.setImageResource(alarmsCount == 0 ? R.drawable.grey_solid : R.drawable.red_solid);
        alarmsCountView.setText(String.valueOf(alarmsCount));
        eventsStatus.set(0);
        updateUi();
    }

    private void updateUi() {
        if (eventsStatus.get(0) && eventsStatus.get(1) && eventsStatus.get(2)) {
            statsLoading.setVisibility(View.GONE);

            if (alarmsCount == 0 && alertsCount == 0 && troublesCount == 0) {
                allIsWellContainer.setVisibility(View.VISIBLE);
                statsContainer.setVisibility(View.INVISIBLE);
            } else {
                statsContainer.setVisibility(View.VISIBLE);
                allIsWellContainer.setVisibility(View.GONE);
            }

        }
    }

    @Override
    public void onClick(View v) {
        final int viewId = v.getId();
        final MainFragment parentFragment = (MainFragment) getParentFragment();

        switch (viewId) {
            case R.id.fragment_stats_troubles_count_card:
                parentFragment.performActionFromId(R.id.troublesButton);
                break;
            case R.id.fragment_stats_alarms_count_card:
                parentFragment.performActionFromId(R.id.alarmsButton);
                break;
            case R.id.fragment_stats_alerts_count_card:
                parentFragment.performActionFromId(R.id.alertsButton);
                break;
            case R.id.fragment_stats_view_all:
                Bundle args = new Bundle(1);
                args.putBoolean(EventsFragment.ARGS_ARM_DISARM_ENABLED, true);
                parentFragment.performActionFromId(R.id.eventsButton, args);
                break;
            default:
                throw new RuntimeException("Can't handle click for this view id: " + viewId);
        }
    }

    private class TroublesLoadedCallback implements DataLoadingSubject.DataLoadingCallbacks {

        @Override
        public void dataStartedLoading() {

        }

        @Override
        public void dataFinishedLoading() {
            if (troubleListDataManager.getData() != null && isAdded()) {
                troublesCount = troubleListDataManager.getData().size();
                troublesImage.setImageResource(troublesCount == 0 ? R.drawable.grey_solid : R.drawable.orange_solid);
                troublesCountView.setText(String.valueOf(troublesCount));
                eventsStatus.set(1);
                updateUi();
            }
        }
    }

    private class AlertsLoadedCallback implements DataLoadingSubject.DataLoadingCallbacks {

        @Override
        public void dataStartedLoading() {

        }

        @Override
        public void dataFinishedLoading() {
            if (alertsListDataManager.getData() != null && isAdded()) {
                alertsCount = alertsListDataManager.getData().size();
                alertsImage.setImageResource(alertsCount == 0 ? R.drawable.grey_solid : R.drawable.yellow_solid);
                alertsCountView.setText(String.valueOf(alertsCount));
                eventsStatus.set(2);
                updateUi();
            }

        }
    }

    private class EventsLoadedCallback implements DataLoadingSubject.DataLoadingCallbacks {
        private final Pattern digits = Pattern.compile("\\d+");
        private final DateFormat timeFormat;

        public EventsLoadedCallback() {
            timeFormat = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM,
                ApplicationSettingsFragment.Localization.getActiveLocale(getActivity()));
        }

        @Override
        public void dataStartedLoading() {}

        @Override
        public void dataFinishedLoading() {
            final List<Event> data = eventsListDataManager.getData();
            if (data != null && isAdded()) {
                if(eventFirstLoad) {
                    eventFirstLoad = false;
                    lastEvent.setVisibility(View.VISIBLE);
                    lastEventLoading.setVisibility(View.GONE);
                }

                for (int i = data.size() - 1; i > 0; i--) {
                    final Event event = data.get(i);
                    if (event.label == Event.EventLabel.ARM || event.label == Event.EventLabel.DISARM) {
                        String userId = getUserId(event.appointment);
                        lastEventAction.setText(event.description);
                        if (userId != null) {
                            lastEventUser.setText(UserAliasesSettingsFragment.getUserAlias(getActivity(), mLoginPrefs, userId));
                        } else {
                            lastEventUser.setText(event.appointment);
                        }
                        lastEventTime.setText(String.valueOf(timeFormat.format(event.getDate())));

                        return;
                    }
                }

                lastEventAction.setText(R.string.no_arm_disarm_events_available);
            }
        }

        private String getUserId(String appointment) {
            Matcher m = digits.matcher(appointment);
            if (m.find()) {
                return m.group();
            } else {
                Log.w(TAG, "Appointment must be in form User %d. " + "Actual=" + appointment);
                return null;
            }
        }
    }
}
