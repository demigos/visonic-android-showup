package com.visonic.visonicalerts.ui.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;

import com.visonic.visonicalerts.R;
import com.visonic.visonicalerts.data.prefs.LoginPrefs;
import com.visonic.visonicalerts.ui.MainActivity;

public class LogoutDialogFragment extends DialogFragment {
	public static final int LOGOUT_ITEM = 0;
	public static final int SIGN_OUT_ITEM = 1;
	public static final int QUIT_ITEM = 2;
	protected LoginPrefs mLoginPrefs;

	@Override
	@CallSuper
	public void onAttach(Context context) {
		super.onAttach(context);
		mLoginPrefs = LoginPrefs.getInstance(getActivity());
	}

	@NonNull
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setItems(R.array.logout_items, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				switch (which) {
					case LOGOUT_ITEM:
						mLoginPrefs.login(null);
						getActivity().getSupportFragmentManager()
								.popBackStack(MainActivity.USER_CODE_STATE, 0);
						break;
					case SIGN_OUT_ITEM:
						mLoginPrefs.login(null);
						getActivity().getSupportFragmentManager()
								.popBackStack(MainActivity.USER_CODE_STATE,
										FragmentManager.POP_BACK_STACK_INCLUSIVE);
						break;
					case QUIT_ITEM:
						mLoginPrefs.login(null);
						getActivity().getSupportFragmentManager()
								.popBackStack(MainActivity.USER_CODE_STATE,
										FragmentManager.POP_BACK_STACK_INCLUSIVE);
						getActivity().moveTaskToBack(true);
						break;
				}
			}
		});
		builder.setNegativeButton(getString(R.string.cancel), null);
		return builder.create();
	}
}
