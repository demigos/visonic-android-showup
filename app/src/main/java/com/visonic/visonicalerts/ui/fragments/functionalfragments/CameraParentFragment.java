package com.visonic.visonicalerts.ui.fragments.functionalfragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.visonic.visonicalerts.R;
import com.visonic.visonicalerts.data.model.Camera;
import com.visonic.visonicalerts.ui.MainActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class CameraParentFragment extends BaseFunctionalFragment implements MainActivity.BackButtonListener {
	public static final String CAMERA_ID = "camera_id";

	@NonNull
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = super.onCreateView(inflater, container, savedInstanceState);
		CamerasFragment camerasFragment = new CamerasFragment();
		if (getArguments() != null) {
			camerasFragment.setArguments(getArguments());
		}
		getChildFragmentManager().beginTransaction()
				.replace(R.id.fragmentContainer, camerasFragment)
				.commit();
		return view;
	}

	public void showVideoPlayer(Camera camera, boolean requestVod, boolean preEnrolled) {
		getChildFragmentManager().beginTransaction()
				.replace(R.id.fragmentContainer, VideoPlayerFragment
						.createCameraInstance(camera, requestVod, preEnrolled))
				.addToBackStack(null)
				.commit();
	}

	@Override
	protected int getLayoutId() {
		return R.layout.fragment_with_subfragments;
	}

	@Override
	public boolean onBackButtonPressed() {
		if (getChildFragmentManager().getBackStackEntryCount() >= 1) {
			getChildFragmentManager().popBackStack();
			return true;
		}
		return false;
	}
}
