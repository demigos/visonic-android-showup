package com.visonic.visonicalerts.ui;

public interface Armable {
	void armAway();
	void armAwayInstant();
	void armHome();
	void armHomeInstant();
}
