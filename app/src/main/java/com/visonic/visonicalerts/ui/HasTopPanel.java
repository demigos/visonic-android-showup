package com.visonic.visonicalerts.ui;

public interface HasTopPanel {
	void setTopPanelVisibility(boolean isTopPanelVisible);
	boolean getDefaultTopPanelVisibility();
	void dataFinishedLoading();
}
