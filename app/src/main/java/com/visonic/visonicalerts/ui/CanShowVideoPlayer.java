package com.visonic.visonicalerts.ui;

import com.visonic.visonicalerts.data.model.AlarmPreview;

public interface CanShowVideoPlayer extends MainActivity.BackButtonListener {
	void showVideoPlayer(AlarmPreview camera, int eventId);
}
