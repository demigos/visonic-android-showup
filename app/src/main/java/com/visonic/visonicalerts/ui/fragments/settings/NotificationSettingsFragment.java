package com.visonic.visonicalerts.ui.fragments.settings;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.v14.preference.SwitchPreference;
import android.support.v7.preference.CheckBoxPreference;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceManager;

import com.visonic.visonicalerts.R;
import com.visonic.visonicalerts.data.DataLoadingSubject;
import com.visonic.visonicalerts.data.datamanager.NotificationDataManager;
import com.visonic.visonicalerts.data.prefs.LoginPrefs;

import java.util.HashMap;
import java.util.Map;

public class NotificationSettingsFragment extends BaseSettingsFragment
		implements DataLoadingSubject.DataLoadingCallbacks {
	public static final String ALERTS = "alerts";
	public static final String ALARMS = "alarms";
	public static final String ALARMS_SOUND = "alarm_notification_sound_enabled";
	public static final String RESTORE = "restore";
	public static final String SECURITY_OPEN_CLOSE = "security_open_close";

	public static final String CAMERA_BEING_VIEW = "camera_being_view";
	public static final String CAMERA_TROUBLE = "camera_trouble";
	public static final String HOME_DEVICE_ON_OFF = "home_device_on_off";
	public static final String HOME_DEVICE_TROUBLE = "home_device_trouble";

	public static final String ONLINE = "online";
	public static final String OFFLINE = "offline";
	public static final String NOTICE = "notice";

	private final Preference.OnPreferenceChangeListener mListener = new Preference.OnPreferenceChangeListener() {
		@Override
		public boolean onPreferenceChange(Preference preference, Object o) {
			SwitchPreference switchPreference = (SwitchPreference) preference;
			switchPreference.setChecked(!switchPreference.isChecked());
			sendNewNotificationPreferences();
			return false;
		}
	};

	private Map<SwitchPreference, Integer> mPreferenceMaskMap;

	protected LoginPrefs mLoginPrefs;
	NotificationDataManager mDataManager;

	@Override
	@CallSuper
	public void onAttach(Context context) {
		super.onAttach(context);
		mLoginPrefs = LoginPrefs.getInstance(getActivity());

		mDataManager = new NotificationDataManager(mLoginPrefs, context);
		mDataManager.addCallbacks(this);
	}

	@Override
	public void onCreatePreferences(Bundle bundle, String s) {
		super.initWithFileName();
		addPreferencesFromResource(R.xml.notification_preferences);
		SwitchPreference alertsPreference = (SwitchPreference) findPreference(ALERTS);
		SwitchPreference alarmsPreference = (SwitchPreference) findPreference(ALARMS);
		SwitchPreference restorePreference = (SwitchPreference) findPreference(RESTORE);
		SwitchPreference securityOpenClosePreference = (SwitchPreference) findPreference(SECURITY_OPEN_CLOSE);
		SwitchPreference cameraBeingViewPreference = (SwitchPreference) findPreference(CAMERA_BEING_VIEW);
		SwitchPreference cameraTroublePreference = (SwitchPreference) findPreference(CAMERA_TROUBLE);
		SwitchPreference homeDeviceOnOffPreference = (SwitchPreference) findPreference(HOME_DEVICE_ON_OFF);
		SwitchPreference homeDeviceTroublePreference = (SwitchPreference) findPreference(HOME_DEVICE_TROUBLE);
		SwitchPreference onlinePreference = (SwitchPreference) findPreference(ONLINE);
		SwitchPreference offlinePreference = (SwitchPreference) findPreference(OFFLINE);
		SwitchPreference noticePreference = (SwitchPreference) findPreference(NOTICE);
		CheckBoxPreference alarmSoundPreference = (CheckBoxPreference) findPreference(ALARMS_SOUND);
		alarmSoundPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
			@Override
			public boolean onPreferenceChange(Preference preference, Object o) {
				CheckBoxPreference checkBoxPreference = (CheckBoxPreference) preference;

				PreferenceManager.getDefaultSharedPreferences(getContext())
					.edit()
					.putBoolean(ALARMS_SOUND, (Boolean) o)
					.apply();
				return true;
			}
		});


		mPreferenceMaskMap = new HashMap<>(10);
		mPreferenceMaskMap.put(alertsPreference, NotificationDataManager.ALERT_MASK);
		mPreferenceMaskMap.put(alarmsPreference, NotificationDataManager.ALARMS_MASK);
		mPreferenceMaskMap.put(restorePreference, NotificationDataManager.RESTORE_MASK);
		mPreferenceMaskMap.put(securityOpenClosePreference, NotificationDataManager.SECURITY_OPEN_CLOSE_MASK);
		mPreferenceMaskMap.put(cameraBeingViewPreference, NotificationDataManager.CAMERA_BEING_VIEW_MASK);
		mPreferenceMaskMap.put(cameraTroublePreference, NotificationDataManager.CAMERA_TROUBLE_MASK);
		mPreferenceMaskMap.put(homeDeviceOnOffPreference, NotificationDataManager.HOME_DEVICE_ON_OFF_MASK);
		mPreferenceMaskMap.put(homeDeviceTroublePreference, NotificationDataManager.HOME_DEVICE_TROUBLE_MASK);
		mPreferenceMaskMap.put(onlinePreference, NotificationDataManager.ONLINE_MASK);
		mPreferenceMaskMap.put(offlinePreference, NotificationDataManager.OFFLINE_MASK);
		mPreferenceMaskMap.put(noticePreference, NotificationDataManager.NOTICE_MASK);

		mDataManager.requestAvailableModes();
	}



	@Override
	public void dataStartedLoading() {

	}

	@Override
	public void dataFinishedLoading() {
		for (SwitchPreference preference : mPreferenceMaskMap.keySet()) {
			processPreference(preference, mPreferenceMaskMap.get(preference));
		}
	}

	private void processPreference(SwitchPreference switchPreference, int mask) {
		switchPreference.setEnabled(mDataManager.checkForAvailability(mask));
		switchPreference.setOnPreferenceChangeListener(mListener);
	}

	private void sendNewNotificationPreferences() {
		int newMode = 0;
		for (SwitchPreference preference : mPreferenceMaskMap.keySet()) {
			if (preference.isEnabled() && preference.isChecked()) {
				newMode += mPreferenceMaskMap.get(preference);
			}
		}
		mDataManager.changeSettings(newMode);
	}
}
