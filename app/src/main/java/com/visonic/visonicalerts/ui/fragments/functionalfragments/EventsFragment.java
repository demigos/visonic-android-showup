package com.visonic.visonicalerts.ui.fragments.functionalfragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.visonic.visonicalerts.R;
import com.visonic.visonicalerts.data.DataLoadingSubject;
import com.visonic.visonicalerts.data.datamanager.ListDataManager;
import com.visonic.visonicalerts.data.model.Event;
import com.visonic.visonicalerts.data.model.Event.EventLabel;
import com.visonic.visonicalerts.data.prefs.LoginPrefs;
import com.visonic.visonicalerts.ui.DataBindable;
import com.visonic.visonicalerts.ui.FilterAdapter;
import com.visonic.visonicalerts.ui.fragments.BaseFragment;
import com.visonic.visonicalerts.ui.fragments.settings.ApplicationSettingsFragment;
import com.visonic.visonicalerts.ui.fragments.settings.UserAliasesSettingsFragment;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;

public class EventsFragment extends BaseFragment implements DataLoadingSubject.DataLoadingCallbacks {
	public static final String ARGS_ARM_DISARM_ENABLED = "arm_disarm_enabled";
	private static final String TAG = "EventsFragment";
	@BindView(R.id.drawer)
	DrawerLayout mDrawerLayout;
	@BindView(R.id.toolbar)
	Toolbar mToolbar;
	@BindView(R.id.list)
	RecyclerView mEventsList;
	@BindView(R.id.filters)
	RecyclerView mFiltersMenu;
	@BindView(R.id.circular_progress_bar)
	ProgressBar mProgressBar;

	private FilterAdapter<EventLabel> mFilterAdapter;
	private EventsAdapter mEventsAdapter;

	@Nullable
	private Set<EventLabel> filters;
	private boolean mHasVideo;
	private ListDataManager<Event> mDataManager;

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		mEventsAdapter = new EventsAdapter(this);
	}

	@Override
	protected int getLayoutId() {
		return R.layout.fragment_events;
	}

	@NonNull
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = super.onCreateView(inflater, container, savedInstanceState);
		List<FilterAdapter.Filter<? extends Set<EventLabel>>> filters = new ArrayList<>();
		FilterAdapter.Filter<Set<EventLabel>> allFilter =
				new FilterAdapter.Filter<Set<EventLabel>>(R.drawable.ic_all,
						getString(R.string.all), EnumSet.allOf(EventLabel.class));
		final FilterAdapter.Filter<EnumSet<EventLabel>> armDisarmFilter =
			new FilterAdapter.Filter<>(R.drawable.ic_arm_disarm, getString(R.string.arm_disarm),
				EnumSet.of(EventLabel.ARM, EventLabel.DISARM));
		filters.add(allFilter);
		filters.add(armDisarmFilter);
		filters.add(new FilterAdapter.Filter<>(R.drawable.ic_burglary_alarms, getString(R.string.burglary_alarms),
				EnumSet.of(EventLabel.BURGLER)));
		filters.add(new FilterAdapter.Filter<Set<EventLabel>>(R.drawable.ic_video_events, getString(R.string.video_events),
				EnumSet.allOf(EventLabel.class), true));
		filters.add(new FilterAdapter.Filter<>(R.drawable.ic_panel_alarms, getString(R.string.panel_alarms),
				EnumSet.of(EventLabel.PANEL_ALARM)));
		filters.add(new FilterAdapter.Filter<>(R.drawable.ic_online_offline, getString(R.string.online_offline),
				EnumSet.of(EventLabel.ONLINE, EventLabel.OFFLINE)));
		filters.add(new FilterAdapter.Filter<>(R.drawable.ic_low_battery, getString(R.string.low_battery),
				EnumSet.of(EventLabel.BATTERY)));
		boolean armDisarmEnabled = false;
		if (getArguments() != null) {
			armDisarmEnabled = getArguments().getBoolean(ARGS_ARM_DISARM_ENABLED, false);
		}
		this.filters =  armDisarmEnabled ? armDisarmFilter.getContent() : allFilter.getContent();
		mFilterAdapter = new FilterAdapter<>(getActivity(), filters,
				new HashSet<EventLabel>(), armDisarmEnabled ? armDisarmFilter : allFilter, allFilter);

		mFiltersMenu.setAdapter(mFilterAdapter);
		mToolbar.inflateMenu(R.menu.filter_menu);
		mToolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
			@Override
			public boolean onMenuItemClick(MenuItem item) {
				if (item.getItemId() == R.id.menu_filter) {
					mDrawerLayout.openDrawer(GravityCompat.END);
					return true;
				}
				return false;
			}
		});

		mToolbar.setNavigationIcon(mUiUtils.getIconDrawable(R.drawable.ic_events));
		mEventsList.setAdapter(mEventsAdapter);
		mDataManager = new ListDataManager<Event>(mLoginPrefs) {
			@Override
			protected boolean filter(Event item) {
				if (EventsFragment.this.filters == null) {
					return true;
				}
				if (mHasVideo) {
					return item.video;
				}
				return EventsFragment.this.filters.contains(item.label);
			}

			@Override
			protected void visonicServiceCall(SimpleListCallback<Event> callback) {
				getVisonicService().events(mLoginPrefs.getSessionToken()).enqueue(callback);
			}

			@Nullable
			@Override
			protected Class<Event> getDataClass() {
				return Event.class;
			}
		};
		mDataManager.addCallbacks(this);
		mFilterAdapter.addFilterChangedListener(new FilterAdapter.FiltersChangedListener<EventLabel>() {
			@Override
			public void onFiltersChanged(Set<EventLabel> changedFilter, boolean hasVideo) {
				EventsFragment.this.filters = changedFilter;
				mHasVideo = hasVideo;
				mDataManager.performFiltering();
			}
		});
		return view;
	}

	@Override
	public void onResume() {
		super.onResume();
		mDataManager.startPolling();
		syncProgressState();
	}

	@Override
	public void onPause() {
		super.onPause();
		mDataManager.stopPolling();
	}

	@Override
	public void dataStartedLoading() {

	}

	@Override
	public void dataFinishedLoading() {
		if (mDataManager.getData() != null) {
			mEventsAdapter.setData(mDataManager.getData());
		}
		syncProgressState();
		mUiUtils.handleError(mDataManager.getResult());
	}

	private void syncProgressState() {
		if (mDataManager != null && isVisible()) {
			mProgressBar.setVisibility(mDataManager.getData() == null ? View.VISIBLE : View.GONE);
		}
	}

	private static class EventsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
		public static final int DATE_TYPE = 0;
		public static final int EVENT_TYPE = 1;
		private static final Calendar CALENDAR_1 = Calendar.getInstance();
		private static final Calendar CALENDAR_2 = Calendar.getInstance();

		private final LoginPrefs mLoginPrefs;
		private final Fragment mFragment;
		private final Context mContext;

		private List<Object> mData;

		public EventsAdapter(EventsFragment fragment) {
			mFragment = fragment;
			mContext = fragment.getActivity();
			mLoginPrefs = fragment.mLoginPrefs;
		}

		@Override
		final public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
			switch (viewType) {
				case DATE_TYPE:
					return new DateViewHolder(LayoutInflater.from(parent.getContext())
							.inflate(R.layout.group_title_list_item, parent, false));
				case EVENT_TYPE:
					return new EventsViewHolder(LayoutInflater.from(parent.getContext())
							.inflate(R.layout.left_and_right_text_list_item, parent, false));
				default:
					throw new RuntimeException("Unexpected viewType");
			}
		}

		@Override
		final public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
			switch (getItemViewType(position)) {
				case DATE_TYPE:
					((DateViewHolder) holder).bind((Date) mData.get(position));
					break;
				case EVENT_TYPE:
					boolean hideBottomLine = position >= mData.size() - 1
							|| getItemViewType(position + 1) == DATE_TYPE;
					((EventsViewHolder) holder).bind((Event) mData.get(position), hideBottomLine);
					break;
			}
		}

		@Override
		final public int getItemCount() {
			return mData == null ? 0 : mData.size();
		}

		@Override
		public int getItemViewType(int position) {
			Object item = mData.get(position);
			if (item instanceof Event) {
				return EVENT_TYPE;
			} else if (item instanceof Date) {
				return DATE_TYPE;
			} else {
				throw new RuntimeException("Unexpected type of object in list previousItem="
						+ item);
			}
		}

		final public void setData(List<Event> data) {
			int size = data.size();
			if (data.size() <= 0) {
				mData = Collections.emptyList();
			} else {
				mData = new ArrayList<>();
				mData.add(data.get(size - 1).getDate());
				mData.add(data.get(size - 1));
				for (int i = 1; i < size; i++) {
					Event item = data.get(size - 1 - i);
					Event previousItem = data.get(size - i);
					if (!isInSameDay(item.getDate(), previousItem.getDate())) {
						mData.add(item.getDate());
					}
					mData.add(item);
				}
			}
			notifyDataSetChanged();
		}

		private static boolean isInSameDay(Date date1, Date date2) {
			CALENDAR_1.setTime(date1);
			CALENDAR_2.setTime(date2);
			return CALENDAR_1.get(Calendar.YEAR) == CALENDAR_2.get(Calendar.YEAR) &&
					CALENDAR_1.get(Calendar.DAY_OF_YEAR) == CALENDAR_2.get(Calendar.DAY_OF_YEAR);
		}

		public class EventsViewHolder extends RecyclerView.ViewHolder {
			private final DateFormat timeFormat;
			private final TextView leftText;
			private final TextView rightText;
			private final View bootomLine;
			private final View videoIndicator;

			private final Pattern digits = Pattern.compile("\\d+");

			public EventsViewHolder(View itemView) {
				super(itemView);
				leftText = (TextView) itemView.findViewById(R.id.left_text);
				rightText = (TextView) itemView.findViewById(R.id.right_text);
				timeFormat = DateFormat.getTimeInstance(DateFormat.MEDIUM,
						ApplicationSettingsFragment.Localization.getActiveLocale(mFragment.getActivity()));
				bootomLine = itemView.findViewById(R.id.bottom_line);
				videoIndicator = itemView.findViewById(R.id.video_indicator);
			}

			public void bind(final Event event, boolean hideBottomLine) {
				String description;
				if (event.label == EventLabel.ARM || event.label == EventLabel.DISARM) {
					String userId = getUserId(event.appointment);
					if (userId != null) {
						description = event.description + " ("
								+ UserAliasesSettingsFragment.getUserAlias(mContext, mLoginPrefs, userId) + ")";
					} else {
						description = event.description;
					}
				} else {
					description = event.description;
				}
				leftText.setText(description);
				rightText.setText(String.valueOf(timeFormat.format(event.getDate())));
				if (hideBottomLine) {
					bootomLine.setVisibility(View.GONE);
				} else {
					bootomLine.setVisibility(View.VISIBLE);
				}
				videoIndicator.setVisibility(event.video ? View.VISIBLE : View.INVISIBLE);
				View.OnClickListener onClickListener = new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						((EventsParentFragment) mFragment.getParentFragment()).showVideoForEvent(event.event);
					}
				};
				itemView.setOnClickListener(event.video ? onClickListener : null);
			}

			private String getUserId(String appointment) {
				Matcher m = digits.matcher(appointment);
				if (m.find()) {
					return m.group();
				} else {
					Log.w(TAG, "Appointment must be in form User %d. " + "Actual=" + appointment);
					return null;
				}
			}
		}

		public class DateViewHolder extends RecyclerView.ViewHolder
				implements DataBindable<Date> {
			public static final long TRANSITION_RESOLUTION = 3 * DateUtils.DAY_IN_MILLIS;
			private final DateFormat dateFormat;

			private TextView text;

			public DateViewHolder(View itemView) {
				super(itemView);
				text = (TextView) itemView;
				dateFormat = android.text.format.DateFormat.getMediumDateFormat(mContext);
			}

			public void bind(Date event) {
				if (Math.abs(System.currentTimeMillis() - event.getTime()) < TRANSITION_RESOLUTION) {
					String dateTimeString = DateUtils.getRelativeDateTimeString(mContext,
							event.getTime(), DateUtils.DAY_IN_MILLIS, TRANSITION_RESOLUTION, 0)
							.toString();
					text.setText(dateTimeString.split(",")[0]);
				} else {
					text.setText(dateFormat.format(event));
				}
			}
		}
	}
}
