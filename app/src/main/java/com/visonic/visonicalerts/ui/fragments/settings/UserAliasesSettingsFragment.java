package com.visonic.visonicalerts.ui.fragments.settings;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.preference.EditTextPreference;
import android.support.v7.preference.PreferenceManager;

import com.visonic.visonicalerts.R;
import com.visonic.visonicalerts.data.prefs.LoginPrefs;

public class UserAliasesSettingsFragment extends BaseSettingsFragment implements SharedPreferences.OnSharedPreferenceChangeListener {
	private static final String KEY_PREFIX = "user_";
	private static final int NUMBER_OF_USERS = 48;

	@Override
	public void onCreatePreferences(Bundle bundle, String s) {
		super.initWithFileName();
		addPreferencesFromResource(R.xml.users_preferences);
		for (int i = 1; i <= NUMBER_OF_USERS; i++) {
			EditTextPreference preference = (EditTextPreference) findPreference(KEY_PREFIX + i);
			preference.setTitle(getString(R.string.user_pattern, i));
			preference.setSummary(preference.getText());
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		getPreferenceScreen().getSharedPreferences()
				.registerOnSharedPreferenceChangeListener(this);
	}

	@Override
	public void onPause() {
		super.onPause();
		getPreferenceScreen().getSharedPreferences()
				.unregisterOnSharedPreferenceChangeListener(this);
	}

	public static String getUserAlias(Context context, LoginPrefs loginPrefs, String id) {
		PreferenceManager preferenceManager = new PreferenceManager(context);
		preferenceManager.setSharedPreferencesName(getPreferenceName(context, loginPrefs));
		SharedPreferences preferences = preferenceManager.getSharedPreferences();
		return preferences.getString(KEY_PREFIX + id,
				context.getString(R.string.user_pattern, Integer.valueOf(id)));
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
		EditTextPreference preference = (EditTextPreference) findPreference(key);
		preference.setSummary(sharedPreferences.getString(key, null));
	}
}
