package com.visonic.visonicalerts.ui.fragments.functionalfragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.FileProvider;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.visonic.visonicalerts.R;
import com.visonic.visonicalerts.data.datamanager.BaseDataManager;
import com.visonic.visonicalerts.data.datamanager.BaseStatusDataManager;
import com.visonic.visonicalerts.data.DataLoadingSubject;
import com.visonic.visonicalerts.data.datamanager.VideoLoadingDataManager;
import com.visonic.visonicalerts.data.model.AbstractVideo;
import com.visonic.visonicalerts.data.model.AlarmPreview;
import com.visonic.visonicalerts.data.model.Camera;
import com.visonic.visonicalerts.data.model.VideoProcessStatus;
import com.visonic.visonicalerts.ui.MainActivity;
import com.visonic.visonicalerts.ui.UiUtils;
import com.visonic.visonicalerts.ui.fragments.BaseFragment;
import com.visonic.visonicalerts.ui.fragments.MainFragment;
import com.visonic.visonicalerts.ui.views.StateListenerVideoView;

import java.io.File;
import java.security.cert.X509Certificate;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import butterknife.BindView;
import butterknife.OnClick;

public class VideoPlayerFragment extends BaseFragment implements DataLoadingSubject.DataLoadingCallbacks {
	private static final String TAG = "VideoPlayerFragment";
	private static final String CAMERA = "camera";
	private static final String EVENT_ID = "event_id";
	private static final String REQUEST_VOD = "request_vod";
	private static final int UNACCEPTABLE_ID = -1;
	private static final String PRE_ENROLLED = "pre_enrolled";

	@BindView(R.id.parent_layout)
	View mParentView;
	@BindView(R.id.images)
	ViewPager mImagesPager;
	@BindView(R.id.video_view_wrapper)
	FrameLayout mVideoViewWrapper;
	@BindView(R.id.video_button)
	CompoundButton mVideoButton;
	@BindView(R.id.frames_button)
	CompoundButton mFramesButton;
	@BindView(R.id.play_pause_button)
	ImageButton mPlayPauseButton;
	@BindView(R.id.mute_button)
	ImageButton mMuteButton;
	@BindView(R.id.frame_number_label)
	TextView mFrameNumberLabel;
	@BindView(R.id.location)
	TextView locationLabel;
	@BindView(R.id.zone_number)
	TextView zoneLabel;
	@BindView(R.id.date)
	TextView dateLabel;
	@BindView(R.id.time)
	TextView timeLabel;
	@BindView(R.id.no_preview)
	TextView mErrorLabel;
	@BindView(R.id.circular_progress_bar)
	ProgressBar mCircularProgressBar;
	@BindView(R.id.share_button)
	View mShareButton;
	@BindView(R.id.play_button)
	View mPlayButton;

	@Nullable
	StateListenerVideoView mVideoView;

	private DateFormat dateFormat;

	private VideoLoadingDataManager mDataManager;
	private int mCameraId;
	private int mEventId;
	private MediaPlayer mMediaPlayer;
	private AbstractVideo mAbstractVideo;
	private UiState mState;
	private Set<String> mUrlsToInvalidate = new HashSet<>();

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		disableSecurityForSsl();
		dateFormat = android.text.format.DateFormat.getMediumDateFormat(context);
		mAbstractVideo = getArguments().getParcelable(CAMERA);
		mEventId = getArguments().getInt(EVENT_ID, UNACCEPTABLE_ID);
		boolean isPreenrolled = getArguments().getBoolean(PRE_ENROLLED, false);
		mState = new UiState(mEventId != UNACCEPTABLE_ID, isPreenrolled);
		if (mState.isEvent()) {
			mCameraId = ((AlarmPreview) mAbstractVideo).camera_id;
		} else {
			mCameraId = ((Camera) mAbstractVideo).zone;
		}
	}

	@Override
	protected int getLayoutId() {
		return R.layout.fragment_video_player;
	}

	@NonNull
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = super.onCreateView(inflater, container, savedInstanceState);
		mGestureDetector = new GestureDetectorCompat(getActivity(),
				new GestureDetector.SimpleOnGestureListener() {
					@Override
					public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
						Log.d(TAG, "onFling() called with: " + "e1 = [" + e1 + "], e2 = [" + e2 + "], velocityX = [" + velocityX + "], velocityY = [" + velocityY + "]");
						if (Math.abs(velocityX) > Math.abs(velocityY) && velocityX > 0) {
							((MainActivity.BackButtonListener) getParentFragment()).onBackButtonPressed();
							return true;
						}
						return false;
					}
				});
		// Init listeners
		mDataManager = new VideoLoadingDataManager(mLoginPrefs, getActivity());
		mDataManager.addCallbacks(this);

		mImagesPager.addOnPageChangeListener(mOnPageChangeListener);
		mParentView.setOnTouchListener(mParentOnTouchListener);

		// Request initial state
		mDataManager.checkOnDemandVideoStatus(mCameraId);
		loadData();
		if (getArguments().getBoolean(REQUEST_VOD, false)) {
			makeVideo();
		}

		// init view
		if (mState.isEvent()) {
			view.findViewById(R.id.main_view).setBackgroundResource(R.drawable.alarm_video_item_background);
			view.findViewById(R.id.rounded_corners).setBackgroundResource(R.drawable.alarm_video_rounded_corners);
		}
		locationLabel.setText(mAbstractVideo.location);
		if (mAbstractVideo.timestamp != null) {
			try {
				Date timeStamp = UiUtils.TIMESTAMP_FORMAT.parse(mAbstractVideo.timestamp);
				dateLabel.setText(dateFormat.format(timeStamp));
				timeLabel.setText(mUiUtils.getDisplayTimeFormat().format(timeStamp));
			} catch (ParseException e) {
				throw new RuntimeException("String:" + mAbstractVideo.timestamp, e);
			}
		} else {
			dateLabel.setText(R.string.not_available_abbr);
			timeLabel.setText(R.string.not_available_abbr);
		}
		int zoneId = mState.isEvent() ? ((AlarmPreview) mAbstractVideo).camera_id
				: ((Camera) mAbstractVideo).zone;
		zoneLabel.setText(getString(R.string.zone_pattern, zoneId));

		syncUi();
		return view;
	}

	private void initVideoView(StateListenerVideoView videoView) {
		videoView.setOnCompletionListener(mOnCompletionListener);
		videoView.setOnPreparedListener(mOnPreparedListener);
		videoView.setPlayPauseListener(mPlayPauseListener);
		videoView.setOnErrorListener(mDummyOnVideoErrorListener);
	}

	@Override
	public void onDestroyView() {
		mDataManager.stopPolling();
		mDataManager.removeCallbacks(this);
		invalidateCachedImages();
		super.onDestroyView();
	}

	public void invalidateCachedImages() {
		for (String imageUrl : mUrlsToInvalidate) {
			BaseDataManager.getPicassoInstance(getActivity()).invalidate(imageUrl);
		}
	}

	// Clickers
	@OnClick(R.id.video_button)
	void setVideoMode() {
		mState.setVideoMode();
	}

	@OnClick(R.id.frames_button)
	void setFramesMode() {
		mState.setFramesMode();
	}

	@OnClick(R.id.play_button)
	void play() {
		if (mVideoView != null) {
			mVideoView.start();
		}
	}

	@OnClick(R.id.play_pause_button)
	void togglePlaying() {
		if (mVideoView != null) {
			if (mState.isPlaying()) {
				mVideoView.pause();
			} else {
				mVideoView.start();
			}
		}
	}

	@OnClick(R.id.share_button)
	void share() {
		File file = mDataManager.getVideoFile();
		if (file != null) {
			Log.d(TAG, "Video size=" + file.length());
			Intent sendIntent = new Intent();
			sendIntent.setAction(Intent.ACTION_SEND);
			Uri videoUri = FileProvider.getUriForFile(getActivity(),
					VideoLoadingDataManager.FILE_PROVIDER_AUTHORITY, file);
			sendIntent.putExtra(Intent.EXTRA_STREAM, videoUri);
			sendIntent.putExtra(Intent.EXTRA_TEXT,
					getString(R.string.email_body_format, mLoginPrefs.getPanelId()));
			sendIntent.setType("text/plain");
			sendIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
			startActivity(sendIntent);
		}
	}

	@OnClick(R.id.mute_button)
	void toggleMute() {
		mState.toggleMute();
		int newVolumeLevel = mState.isMuted() ? 0 : 1;
		if (mMediaPlayer != null) {
			mMediaPlayer.setVolume(newVolumeLevel, newVolumeLevel);
		}
	}

	@OnClick(R.id.make_video)
	void makeVideo() {
		if (mState.getVideoState() == VideoProcessStatus.FAILED
				|| mState.getVideoState() == VideoProcessStatus.SUCCEEDED
				|| mState.getVideoState() == null) {
			invalidateCachedImages();
			mDataManager.invalidateData();
			syncUi();
			if (mState.isEvent()) {
				((MainFragment) getParentFragment().getParentFragment())
						.startVideoOnDemandForCamera(mCameraId);
			} else {
				mState.setRequestingVideoOnDemand(true);
				mDataManager.makeVideo(mCameraId);
			}
		} else {
			Toast.makeText(getActivity(), R.string.video_on_demand_has_already_started,
					Toast.LENGTH_SHORT).show();
		}
	}

	private void syncUi() {
		mFramesButton.setEnabled(true);
		mVideoButton.setEnabled(true);
		if (mState.isVideo()) {
			mImagesPager.setVisibility(View.GONE);
			mVideoButton.setChecked(true);
			mFramesButton.setChecked(false);
			mFrameNumberLabel.setVisibility(View.GONE);
			if (mState.isEvent()) {
				if (mDataManager.getResult() != null) {
					showVideoIfHasPath();
				} else {
					showProgress(true);
					enableVideo(false);
					setErrorTextRes(-1);
				}
			} else {
				if (mState.isRequestingVideoOnDemand()) {
					showProgress(true);
					enableVideo(false);
					setErrorTextRes(-1);
				} else if (mState.getVideoState() == null) {
					showProgress(false);
					enableVideo(false);
					if (mState.isPreenrolled()) {
						setErrorTextRes(R.string.pre_enrolled);
						mFramesButton.setEnabled(false);
						mVideoButton.setEnabled(false);
					} else if (mDataManager.getResult() != null) {
						setErrorTextRes(R.string.no_preview);
					} else {
						showProgress(true);
					}
				} else {
					switch (mState.getVideoState()) {
						case FAILED:
							showVideoIfHasPath(R.string.failed);
							break;
						case HANDLED:
						case START:
							setErrorTextRes(UNACCEPTABLE_ID);
							showProgress(true);
							if (mState.hasPath()) {
								setErrorTextRes(-1);
								showProgress(false);
								enableVideo(true);
							} else {
								setErrorTextRes(R.string.started);
								showProgress(true);
								enableVideo(false);
							}
							break;
						case SUCCEEDED:
							setErrorTextRes(UNACCEPTABLE_ID);
							showProgress(false);
							showVideoIfHasPath();
							break;
					}
				}
			}
		} else if (mState.isFrames()) {
			enableVideo(false);
			mVideoButton.setChecked(false);
			mFramesButton.setChecked(true);
			mFrameNumberLabel.setVisibility(View.VISIBLE);
			if (mState.isRequestingVideoOnDemand()) {
				showProgress(true);
				setErrorTextRes(UNACCEPTABLE_ID);
				mImagesPager.setVisibility(View.GONE);
				mFrameNumberLabel.setVisibility(View.GONE);
			} else if (mState.getVideoState() == null) {
				showProgress(false);
				setErrorTextRes(R.string.no_preview);
				showFramesIfHasPath();
			} else {
				showProgress(false);
				switch (mState.getVideoState()) {
					case FAILED:
						setErrorTextRes(R.string.failed);
						showFramesIfHasPath();
						break;
					case HANDLED:
					case START:
						if (mDataManager.getFrames() == null
								|| mDataManager.getFrames().size() == 0) {
							showProgress(true);
						}
						setErrorTextRes(UNACCEPTABLE_ID);
						showFramesIfHasPath();
						break;
					case SUCCEEDED:
						setErrorTextRes(UNACCEPTABLE_ID);
						showFramesIfHasPath();
						break;
				}
			}
		} else {
			throw new IllegalStateException("Unxepected state:" + mState);
		}
		if (mState.isPlaying()) {
			mPlayPauseButton.setImageResource(R.drawable.ic_pause_white_24dp);
		} else {
			mPlayPauseButton.setImageResource(R.drawable.ic_play_arrow_white_24dp);
		}
		Log.d(TAG, "videoFile=" + mDataManager.getVideoFile());
		Log.d(TAG, "UiState=" + mState);
		syncMuteButton();
	}

	private void showFramesIfHasPath() {
		if (mDataManager.getFrames() == null || mDataManager.getFrames().size() == 0) {
			mImagesPager.setVisibility(View.GONE);
			mFrameNumberLabel.setVisibility(View.GONE);
		} else {
			showProgress(false);
			setErrorTextRes(-1);
			mImagesPager.setVisibility(View.VISIBLE);
			ImagesPagerAdapter imagesPagerAdapter =
					new ImagesPagerAdapter(getChildFragmentManager(), mDataManager.getFrames());
			int currentItem = mImagesPager.getCurrentItem();
			currentItem = currentItem < imagesPagerAdapter.getCount() ? currentItem
					: imagesPagerAdapter.getCount() - 1;
			mImagesPager.setAdapter(imagesPagerAdapter);
			mImagesPager.setCurrentItem(currentItem);
			syncFrameCount(currentItem);
		}
	}

	private void enableVideo(boolean enable) {
		mVideoViewWrapper.setVisibility(enable ? View.VISIBLE : View.GONE);
		mMuteButton.setEnabled(enable);
		mPlayPauseButton.setEnabled(enable);
		mShareButton.setEnabled(enable);
		if (enable) {
			if (mState.isPlaying()) {
				mPlayButton.setVisibility(View.GONE);
			} else {
				mPlayButton.setVisibility(View.VISIBLE);
			}
		} else {
			mPlayButton.setVisibility(View.GONE);
		}
	}

	private void setErrorTextRes(@StringRes int textRes) {
		if (textRes != UNACCEPTABLE_ID) {
			mErrorLabel.setVisibility(View.VISIBLE);
			mErrorLabel.setText(textRes);
		} else {
			mErrorLabel.setVisibility(View.GONE);
		}
	}

	private void showProgress(boolean show) {
		mCircularProgressBar.setVisibility(show ? View.VISIBLE : View.GONE);
	}

	private void showVideoIfHasPath() {
		showVideoIfHasPath(R.string.no_preview);
	}

	private void showVideoIfHasPath(@StringRes int errorMessage) {
		if (mState.hasPath()) {
			setErrorTextRes(-1);
			showProgress(false);
			enableVideo(true);
		} else {
			setErrorTextRes(errorMessage);
			showProgress(false);
			enableVideo(false);
		}
	}

	private void syncFrameCount(int activeFrame) {
		if (mDataManager.getFrames() != null && activeFrame != -1
				&& mDataManager.getFrames().size() > 0 && mState.isFrames()) {
			mFrameNumberLabel.setVisibility(View.VISIBLE);
			mFrameNumberLabel.setText(String.format(Locale.UK,
					"%d/%d", activeFrame + 1, mDataManager.getFrames().size()));
			mErrorLabel.setVisibility(View.GONE);
		} else {
			mFrameNumberLabel.setVisibility(View.GONE);
			mErrorLabel.setVisibility(View.VISIBLE);
		}
	}

	@Override
	public void dataStartedLoading() {
		// TODO: 2/19/16 implement
	}

	@Override
	public void dataFinishedLoading() {
		if (!isVisible()) {
			return;
		}
		if (mDataManager.getResult() == BaseStatusDataManager.Result.OK) {
			if (!mState.isEvent()) {
				if (mDataManager.getProcessStatus() != null) {
					mState.setRequestingVideoOnDemand(false);
				}
				mState.setVideoState(mDataManager.getProcessStatus());
			}
			mState.setHasPath(mDataManager.getVideoFile() != null);
			if (isVideoReady() && mDataManager.isPolling() || mState.isEvent()) {
				mDataManager.stopPolling();
			}
			if (mState.isVideo()) {
				if (mDataManager.getVideoFile() != null) {
					File videoFile = mDataManager.getVideoFile();
					Uri videoUri = Uri.fromFile(videoFile);

					mState.setPaused();
					mVideoViewWrapper.removeView(mVideoView);
					mVideoView = new StateListenerVideoView(getActivity());
					mVideoViewWrapper.addView(mVideoView);
					initVideoView(mVideoView);
					mVideoView.setVideoURI(videoUri);
				}
			}
		} else {
			mState.setRequestingVideoOnDemand(false);
			File videoFile = mDataManager.getVideoFile();
			if (videoFile == null) {
				if (mVideoView != null) {
					mVideoView.setVisibility(View.GONE);
				}
			}
			mUiUtils.handleError(mDataManager.getResult());
		}

		syncUi();
	}

	private boolean isVideoReady() {
		return mDataManager.getProcessStatus() == VideoProcessStatus.FAILED ||
				mDataManager.getProcessStatus() == VideoProcessStatus.SUCCEEDED;
	}

	private void loadData() {
		if (mState.isEvent()) {
			mDataManager.loadAlarmVideoFrames(mEventId, mCameraId);
			mDataManager.loadAlarmVideo(mEventId, mCameraId);
		} else {
			mDataManager.loadVideoFrameUrls(String.valueOf(mCameraId));
			mDataManager.loadVideo(String.valueOf(mCameraId));
		}
	}

	private void syncMuteButton() {
		@DrawableRes
		int volumeIconId = mState.isMuted() ? R.drawable.ic_volume_off_white_24dp
				: R.drawable.ic_volume_up_white_24dp;
		mMuteButton.setImageResource(volumeIconId);
	}

	public static VideoPlayerFragment createCameraInstance(Camera camera,
														   boolean requestVod,
														   boolean preEnrolled) {
		Bundle args = new Bundle();
		args.putParcelable(CAMERA, camera);
		args.putBoolean(REQUEST_VOD, requestVod);
		args.putBoolean(PRE_ENROLLED, preEnrolled);
		VideoPlayerFragment fragment = new VideoPlayerFragment();
		fragment.setArguments(args);
		return fragment;
	}

	public static VideoPlayerFragment createAlertInstance(AlarmPreview camera, int eventId) {
		Bundle args = new Bundle();
		args.putParcelable(CAMERA, camera);
		args.putInt(EVENT_ID, eventId);
		VideoPlayerFragment fragment = new VideoPlayerFragment();
		fragment.setArguments(args);
		return fragment;
	}

	public static class ImagesPagerAdapter extends FragmentStatePagerAdapter {
		private final List<String> mImageUrls;

		public ImagesPagerAdapter(FragmentManager fm, List<String> imageUrls) {
			super(fm);
			this.mImageUrls = imageUrls;
		}

		@Override
		public Fragment getItem(int position) {
			return ImagePageFragment.createInstance(mImageUrls.get(position));
		}

		@Override
		public int getCount() {
			if (mImageUrls == null) {
				return 0;
			}
			return mImageUrls.size();
		}
	}

	public void addUrlsToInvalidate(String url) {
		mUrlsToInvalidate.add(url);
	}

	public static class ImagePageFragment extends BaseFragment {
		private static final String TAG = "ImagePageFragment";
		private static final String IMAGE_URL = "image_url";

		@BindView(R.id.image)
		ImageView mImageView;

		@Override
		protected int getLayoutId() {
			return R.layout.fragment_image_page;
		}

		@NonNull
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			View view = super.onCreateView(inflater, container, savedInstanceState);
			String imageUrlFragment = getArguments().getString(IMAGE_URL);
			assert imageUrlFragment != null;
			String imageUrl = String.format("https://%s%s", mLoginPrefs.getHostAddress(), imageUrlFragment);
			((VideoPlayerFragment) getParentFragment()).addUrlsToInvalidate(imageUrl);
			BaseDataManager.getPicassoInstance(getActivity()).load(imageUrl)
					.into(mImageView);
			return view;
		}

		public static ImagePageFragment createInstance(String imageUrl) {
			Bundle bundle = new Bundle();
			bundle.putString(IMAGE_URL, imageUrl);

			ImagePageFragment fragment = new ImagePageFragment();
			fragment.setArguments(bundle);
			return fragment;
		}
	}


	private void disableSecurityForSsl() {
		TrustManager[] trustAllCerts = new TrustManager[]
				{new X509TrustManager() {
					public java.security.cert.X509Certificate[] getAcceptedIssuers() {
						return null;
					}

					@SuppressLint("TrustAllX509TrustManager")
					public void checkClientTrusted(X509Certificate[] chain, String authType) {
					}

					@SuppressLint("TrustAllX509TrustManager")
					public void checkServerTrusted(X509Certificate[] chain, String authType) {
					}
				}
				};

		try {
			SSLContext sc = SSLContext.getInstance("SSL"); // "TLS" "SSL"
			sc.init(null, trustAllCerts, null);
			// sc.init( null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
			HttpsURLConnection.setDefaultHostnameVerifier(
					new HostnameVerifier() {
						@SuppressLint("BadHostnameVerifier")
						public boolean verify(String hostname, SSLSession session) {
							return true;
						}
					});
		} catch (Exception e) {
			Log.w(TAG, e);
		}
	}

	private final ViewPager.OnPageChangeListener mOnPageChangeListener = new ViewPager.OnPageChangeListener() {
		@Override
		public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
		}

		@Override
		public void onPageSelected(int position) {
			syncFrameCount(position);
		}

		@Override
		public void onPageScrollStateChanged(int state) {

		}
	};

	private final MediaPlayer.OnCompletionListener mOnCompletionListener = new MediaPlayer.OnCompletionListener() {
		@Override
		public void onCompletion(MediaPlayer mp) {
			mState.setPaused();
		}
	};

	private final StateListenerVideoView.PlayPauseListener mPlayPauseListener = new StateListenerVideoView.PlayPauseListener() {
		@Override
		public void onPlay() {
			mState.setPlaying();
		}

		@Override
		public void onPause() {
			mState.setPaused();
		}
	};

	private GestureDetectorCompat mGestureDetector;

	private final View.OnTouchListener mParentOnTouchListener = new View.OnTouchListener() {
		@Override
		public boolean onTouch(View v, MotionEvent event) {
			return mGestureDetector.onTouchEvent(event);
		}
	};

	MediaPlayer.OnPreparedListener mOnPreparedListener = new MediaPlayer.OnPreparedListener() {
		@Override
		public void onPrepared(MediaPlayer mp) {
			mState.prepared();
			mMediaPlayer = mp;
		}
	};

	private MediaPlayer.OnErrorListener mDummyOnVideoErrorListener = new MediaPlayer.OnErrorListener() {
		public boolean onError(MediaPlayer mp, int what, int extra) {
			return true;
		}
	};

	private enum VideoUiMode {
		VIDEO,
		FRAME
	}

	private class UiState {
		private final boolean mIsEvent;
		private final boolean mIsPreenrolled;
		private VideoUiMode mVideoUiMode = VideoUiMode.VIDEO;
		@Nullable
		private VideoProcessStatus mVideoState;
		private boolean mIsMuted;
		private boolean mIsPlaying;
		private boolean mIsPanelConnected;
		private boolean mIsPrepared;
		private boolean mHasPath;
		private boolean mIsRequestingVideoOnDemand;

		public UiState(boolean isEvent, boolean isPreenrolled) {
			mIsEvent = isEvent;
			mIsPreenrolled = isPreenrolled;
		}

		public boolean isPreenrolled() {
			return mIsPreenrolled;
		}

		public boolean isVideo() {
			return mVideoUiMode == VideoUiMode.VIDEO;
		}

		public boolean isFrames() {
			return mVideoUiMode == VideoUiMode.FRAME;
		}

		public boolean isEvent() {
			return mIsEvent;
		}

		public boolean isMuted() {
			return mIsMuted;
		}

		public void setVideoMode() {
			mVideoUiMode = VideoUiMode.VIDEO;
			loadData();
			syncUi();
		}

		public void setFramesMode() {
			mVideoUiMode = VideoUiMode.FRAME;
			mIsPlaying = false;
			loadData();
			syncUi();
		}

		public void toggleMute() {
			mIsMuted = !mIsMuted;
			syncUi();
		}

		public void setPlaying() {
			mIsPlaying = true;
			syncUi();
		}

		public void setPaused() {
			mIsPlaying = false;
			syncUi();
		}

		public boolean isPlaying() {
			return mIsPlaying;
		}

		public boolean hasPath() {
			return mHasPath;
		}

		public void setHasPath(boolean hasPath) {
			mHasPath = hasPath;
		}

		private void prepared() {
			mIsPrepared = true;
			syncUi();
		}

		@Nullable
		public VideoProcessStatus getVideoState() {
			return mVideoState;
		}

		public void setVideoState(@Nullable VideoProcessStatus videoState) {
			mVideoState = videoState;
		}

		public boolean isRequestingVideoOnDemand() {
			return mIsRequestingVideoOnDemand;
		}

		public void setRequestingVideoOnDemand(boolean requestingVideoOnDemand) {
			mIsRequestingVideoOnDemand = requestingVideoOnDemand;
		}

		@Override
		public String toString() {
			return "UiState{" +
					"mIsEvent=" + mIsEvent +
					", mVideoUiMode=" + mVideoUiMode +
					", mVideoState=" + mVideoState +
					", mIsMuted=" + mIsMuted +
					", mIsPlaying=" + mIsPlaying +
					", mIsPanelConnected=" + mIsPanelConnected +
					", mIsPrepared=" + mIsPrepared +
					", mHasPath=" + mHasPath +
					", mIsRequestingVideoOnDemand=" + mIsRequestingVideoOnDemand +
					'}';
		}
	}
}
