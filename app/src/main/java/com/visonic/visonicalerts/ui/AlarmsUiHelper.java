package com.visonic.visonicalerts.ui;

import com.visonic.visonicalerts.data.model.Alarm;

import java.util.List;

public final class AlarmsUiHelper {
	private AlarmsUiHelper() {
	}

	public interface AlarmsProvider {
		void notifyAlarmsListener();
	}

	public interface AlarmsListener {
		void onAlarmsUpdated(List<Alarm> alarms);
	}
}
