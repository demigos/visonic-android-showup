package com.visonic.visonicalerts.ui.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

public class AspectRatioFrameLayout extends FrameLayout {

	public AspectRatioFrameLayout(Context context) {
		super(context, null);
	}

	public AspectRatioFrameLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int fourThreeHeight = MeasureSpec.makeMeasureSpec(MeasureSpec.getSize(widthMeasureSpec) * 3 / 4,
				MeasureSpec.EXACTLY);
		super.onMeasure(widthMeasureSpec, fourThreeHeight);
	}

	@Override
	public boolean hasOverlappingRendering() {
		return false;
	}
}
