package com.visonic.visonicalerts.ui;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorInt;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.util.TypedValue;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.visonic.visonicalerts.R;
import com.visonic.visonicalerts.data.datamanager.BaseStatusDataManager;
import com.visonic.visonicalerts.data.model.Alarm;
import com.visonic.visonicalerts.data.model.DeviceType;
import com.visonic.visonicalerts.data.model.ZoneSubtype;
import com.visonic.visonicalerts.ui.fragments.settings.ApplicationSettingsFragment;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.TimeZone;

public final class UiUtils {
	public static final SimpleDateFormat TIMESTAMP_FORMAT =
			new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);

	static {
		TIMESTAMP_FORMAT.setTimeZone(TimeZone.getTimeZone("UTC"));
	}

	private final DateFormat mDisplayTimeFormat;
	private final int mTextColorPrimary;
	private final int mHighlightColor;
	private final int mTextColor;
	private final Context mContext;

	public UiUtils(Context context) {
		mContext = context;

		TypedValue typedValue = new TypedValue();
		Resources.Theme theme = context.getTheme();
		theme.resolveAttribute(android.R.attr.textColorPrimary, typedValue, true);

		mTextColorPrimary = typedValue.data;
		mHighlightColor = ContextCompat.getColor(context, R.color.blue);

		theme.resolveAttribute(android.R.attr.textColor, typedValue, true);
		mTextColor = typedValue.data;

		mDisplayTimeFormat = DateFormat.getTimeInstance(DateFormat.MEDIUM,
				ApplicationSettingsFragment.Localization.getActiveLocale(context));
	}

	public DateFormat getDisplayTimeFormat() {
		return mDisplayTimeFormat;
	}

	public static void setButtonColor(Button button, @ColorInt int color) {
		button.setTextColor(color);
		Drawable drawable = button.getCompoundDrawables()[1];
		DrawableCompat.wrap(drawable);
		drawable.mutate();
		DrawableCompat.setTint(drawable, color);
	}

	public static void setListItemColor(TextView button, ColorStateList color) {
		button.setTextColor(color);
		Drawable drawable = button.getCompoundDrawables()[0];
		DrawableCompat.wrap(drawable);
		drawable.mutate();
		DrawableCompat.setTintList(drawable, color);
	}

	public static void setListItemColor(TextView button, @ColorInt int color) {
		button.setTextColor(color);
		Drawable drawable = button.getCompoundDrawables()[0];
		DrawableCompat.wrap(drawable);
		drawable.mutate();
		DrawableCompat.setTint(drawable, color);
	}

	public Drawable getIconDrawable(@DrawableRes int drawableRes) {
		return getIconDrawable(drawableRes, mTextColor);
	}

	public Drawable getIconDrawable(@DrawableRes int drawableRes, @ColorInt int color) {
		Drawable drawable = ContextCompat.getDrawable(mContext, drawableRes);
		drawable = DrawableCompat.wrap(drawable);
		drawable.mutate();
		DrawableCompat.setTint(drawable, color);
		return drawable;
	}

	@ColorInt
	public int getTextColor() {
		return mTextColor;
	}

	@ColorInt
	public int getTextColorPrimary() {
		return mTextColorPrimary;
	}

	public int getHighlightColor() {
		return mHighlightColor;
	}

	public void handleError(BaseStatusDataManager.Result result) {
		if (result == BaseStatusDataManager.Result.NETWORK_PROBLEM) {
			Toast.makeText(mContext, R.string.cant_connect_no_internet, Toast.LENGTH_SHORT).show();
		}
	}


	@DrawableRes
	public static int getDrawableIdForAlarmType(Alarm alarm) {
		switch (alarm.alarm_type) {
			case PANIC:
				return R.drawable.panic_alarm;
			case EMERGENCY:
				return R.drawable.emergency_alarm;
			case ALARM_IN_MEMORY:
				if (alarm.zone_type == null) {
					return R.drawable.burglary_alarm;
				} else {
					switch (alarm.zone_type) {
						case PUSH_BUTTON:
						case KEYFOB:
							return R.drawable.panic_alarm;
						case SMOKE:
							return R.drawable.smoke_alarm;
						case GAS:
							return R.drawable.gas;
						case FLOOD:
							return R.drawable.flood_alarm;
						case SMOKE_OR_HEAT:
						case FIRE:
							return R.drawable.fire_alarm;
						case EMERGENCY:
							return R.drawable.emergency_alarm;
						case DELAY_1:
						case DELAY_2:
						case HOME_DELAY:
						case INTERIOR:
						case INTERIOR_FOLLOW:
						case GUARD_KEYBOX:
						case PERIMETER:
						case PERIMETER_FOLLOW:
						case TWENTY_FOUR_H_SILENT:
						case TWENTY_FOUR_H_AUDIBLE:
						case OUTDOOR:
							return R.drawable.burglary_alarm;
						case CO:
							return R.drawable.co2;
					}
				}
				return R.drawable.burglary_alarm;
			case FIRE:
				return R.drawable.fire_alarm;
			case SMOKE_MEMORY:
				return R.drawable.smoke_alarm;
			case HEAT_MEMORY:
				return R.drawable.heat_alarm;
			case TAMPER_MEMORY:
				return R.drawable.tamper_alarm;
			default:
				return -1;
		}
	}

	@StringRes
	public static int getStringIdForAlarmType(Alarm alarm) {
		switch (alarm.alarm_type) {
			case PANIC:
			case EMERGENCY:
			case FIRE:
			case SMOKE_MEMORY:
			case HEAT_MEMORY:
			case TAMPER_MEMORY:
				return alarm.alarm_type.getTextResource();
			case ALARM_IN_MEMORY:
				if (alarm.zone_type == null) {
					return R.string.burglary_alarm;
				} else {
					switch (alarm.zone_type) {
						case PUSH_BUTTON:
						case KEYFOB:
							return R.string.panic_alarm;
						case SMOKE:
							return R.string.smoke_memory;
						case GAS:
							return R.string.gas_alert;
						case FLOOD:
							return R.string.flood_alert;
						case SMOKE_OR_HEAT:
						case FIRE:
							return R.string.fire_alarm;
						case EMERGENCY:
							return R.string.emergency_alarm;
						case DELAY_1:
						case DELAY_2:
						case HOME_DELAY:
						case INTERIOR:
						case INTERIOR_FOLLOW:
						case GUARD_KEYBOX:
						case PERIMETER:
						case PERIMETER_FOLLOW:
						case TWENTY_FOUR_H_SILENT:
						case TWENTY_FOUR_H_AUDIBLE:
						case OUTDOOR:
							return R.string.burglary_alarm;
						case CO:
							return R.string.co_alert;
					}
				}
			default:
				return -1;
		}
	}

	@DrawableRes
	public static int getDeviceIcon(@Nullable ZoneSubtype zoneSubtype, @Nullable DeviceType deviceType) {
		if (zoneSubtype == null) {
			if (deviceType == null) {
				return R.drawable.ic_device_undefined;
			}

			switch (deviceType) {
				case ZONE:
					return R.drawable.ic_device_type_sensor;
				case KEYFOB:
					return R.drawable.ic_device_type_keyfob;
				case PENDANT:
				case GUARD_KEY:
					return R.drawable.ic_device_type_keyfob_one_btn;
				case WIRELESS_COMMANDER:
				case TWO_WAY_WIRELESS_KEYPAD:
					return R.drawable.ic_device_type_keyboard;
				case CAMERA:
					return R.drawable.ic_device_type_camera;
				case PROXY_TAG:
					return R.drawable.ic_device_type_nfc;
				case WL_SIREN:
					return R.drawable.ic_device_type_alarm;
				case REPEATER:
				case X10:
				case PGM:
					return R.drawable.ic_device_type_unknown;
				default:
					return R.drawable.ic_device_type_unknown;
			}
		}

		switch (zoneSubtype) {
			case CONTACT:
				return R.drawable.ic_mc_302;
			case CONTACT_AUX:
				return R.drawable.ic_mc_302e;
			case CONTACT_V:
				return R.drawable.ic_mc_302v_pg2;
			case MOTION:
				return R.drawable.ic_next_pg2;
			case CURTAIN:
				return R.drawable.ic_clip_pg2;
			case MOTION_V_ANTIMASK:
				return R.drawable.ic_tower_30_pg2;
			case MOTION_OUTDOOR:
				return R.drawable.ic_tower_20am_pg2;
			case MOTION_OUTDOOR_CAMERA:
				return R.drawable.ic_tower_cam_pg2;
			case MOTION_CAMERA:
				return R.drawable.ic_next_cam_pg2;
			case MOTION_DUAL:
				return R.drawable.ic_tower_30_pg2;
			case GLASS_BREAK:
				return R.drawable.ic_gb_501;
			case SHOCK_AUX:
			case SHOCK_CONTACT_G2:
			case SHOCK_CONTACT_G3:
			case SHOCK_CONTACT_AUX_ANTIMASK:
				return R.drawable.ic_sd_304_pg2;
			case SMOKE:
				return R.drawable.ic_smd_426_pg2;
			case SMOKE_HEAT:
			case SMOKE_OR_HEAT:
				return R.drawable.ic_smd_427_pg2;
			case CO:
				return R.drawable.ic_gsd_442_pg2;
			case GAS:
				return R.drawable.ic_gsd_441_pg2;
			case FLOOD:
				return R.drawable.ic_fld_550_pg2;
			case FLOOD_PROBE:
				return R.drawable.ic_fld_550_pg2;
			case TEMPERATURE:
				return R.drawable.ic_tmd_560_pg2;
			case KEYFOB_ARM_LED:
				return R.drawable.ic_kf_235_pg2;
			case BASIC_KEYFOB:
				return R.drawable.ic_kf_234_pg2;
			case SINGLE_BUTTON:
				return R.drawable.ic_pb_101_pg2;
			case TWO_BUTTON:
				return R.drawable.ic_pm_102_pg2;
			case KEYPAD:
				return R.drawable.ic_mcm_140;
			case PROXIMITY_KEYPAD:
				return R.drawable.ic_mcm_140;
			case LCD_KEYPAD:
				return R.drawable.ic_mkp_160;
			case LCD_PRG_KEYPAD:
				return R.drawable.ic_kp_250;
			case INDOOR:
				return R.drawable.ic_rp_600_pg2;
			case OUTDOOR:
			case AC_OUTDOOR:
				return R.drawable.ic_sr_730_pg2;
			case BASIC_REPEATER:
				return R.drawable.ic_rp_600_pg2;
			default:
				return R.drawable.ic_device_undefined;
		}
	}

	@StringRes
	public static int getDeviceName(@Nullable ZoneSubtype zoneSubtype, @Nullable DeviceType deviceType) {
		if (zoneSubtype == null) {
			if (deviceType == null) {
				return R.string.unknown;
			}

			switch (deviceType) {
				case KEYFOB:
				case WIRELESS_COMMANDER:
				case TWO_WAY_WIRELESS_KEYPAD:
				case PENDANT:
				case GUARD_KEY:
					return R.string.keyfob;
				case ZONE:
					return R.string.zone;
				case WL_SIREN:
					return R.string.wireless_siren;
				case REPEATER:
					return R.string.repeater;
				case X10:
					return R.string.x10;
				default:
					return R.string.unknown;
			}
		}

		switch (zoneSubtype) {
			case CONTACT:
				return R.string.device_name_contact;
			case CONTACT_AUX:
				return R.string.device_name_contact_aux;
			case CONTACT_V:
				return R.string.device_name_contact_v;
			case MOTION:
				return R.string.device_name_motion;
			case CURTAIN:
				return R.string.device_name_curtain;
			case MOTION_V_ANTIMASK:
				return R.string.device_name_motion_v_antimask;
			case MOTION_OUTDOOR:
				return R.string.device_name_motion_outdoor;
			case MOTION_OUTDOOR_CAMERA:
				return R.string.device_name_motion_outdoor_camera;
			case MOTION_CAMERA:
				return R.string.device_name_motion_camera;
			case MOTION_DUAL:
				return R.string.device_name_motion_dual;
			case GLASS_BREAK:
				return R.string.device_name_glass_break;
			case SHOCK:
				return R.string.device_name_shock;
			case SHOCK_AUX:
				return R.string.device_name_shock_aux;
			case SHOCK_CONTACT_G2:
				return R.string.device_name_shock_contact_G2;
			case SHOCK_CONTACT_G3:
				return R.string.device_name_shock_contact_G3;
			case SHOCK_CONTACT_AUX_ANTIMASK:
				return R.string.device_name_shock_contact_aux_antimask;
			case SMOKE:
				return R.string.device_name_smoke;
			case SMOKE_HEAT:
				return R.string.device_name_smoke_heat;
			case SMOKE_OR_HEAT:
				return R.string.device_name_smoke_or_heat;
			case CO:
				return R.string.device_name_co;
			case GAS:
				return R.string.gas_detector;
			case FLOOD:
				return R.string.device_name_flood_detector;
			case FLOOD_PROBE:
				return R.string.device_name_flood_probe;
			case TEMPERATURE:
				return R.string.device_name_temperature;
			case KEYFOB_ARM_LED:
				return R.string.device_name_keyfob_arm_led;
			case BASIC_KEYFOB:
				return R.string.device_name_basic_keyfob;
			case SINGLE_BUTTON:
				return R.string.device_name_single_button;
			case TWO_BUTTON:
				return R.string.device_name_two_button;
			case KEYPAD:
				return R.string.device_name_keypad;
			case PROXIMITY_KEYPAD:
				return R.string.device_name_proximity_keypad;
			case LCD_KEYPAD:
				return R.string.device_name_lcd_keypad;
			case LCD_PRG_KEYPAD:
				return R.string.device_name_lcd_prg_keypad;
			case INDOOR:
				return R.string.device_name_indoor;
			case OUTDOOR:
				return R.string.device_name_outdoor;
			case AC_OUTDOOR:
				return R.string.device_name_ac_outdoor;
			case BASIC_REPEATER:
				return R.string.device_name_basic_repeater;

			default:
				return R.string.unknown;
		}
	}
}
