package com.visonic.visonicalerts.ui.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import com.visonic.visonicalerts.R;

public class ProgressDialogFragment extends DialogFragment {
	public static final String TAG = "ProgressDialogFragment";
	@NonNull
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AppTheme_ProgressDialogTheme);
		builder.setView(R.layout.circular_indetermined_progress_bar);
		return builder.create();
	}
}
