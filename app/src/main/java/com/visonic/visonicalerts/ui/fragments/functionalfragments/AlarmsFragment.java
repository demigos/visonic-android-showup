package com.visonic.visonicalerts.ui.fragments.functionalfragments;


import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.visonic.visonicalerts.R;
import com.visonic.visonicalerts.data.model.Alarm;
import com.visonic.visonicalerts.data.model.AlarmType;
import com.visonic.visonicalerts.ui.AlarmsUiHelper;
import com.visonic.visonicalerts.ui.UiUtils;
import com.visonic.visonicalerts.ui.fragments.BaseFragment;
import com.visonic.visonicalerts.ui.fragments.settings.ApplicationSettingsFragment;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;


/**
 * A simple {@link Fragment} subclass.
 */
public class AlarmsFragment extends BaseFragment implements AlarmsUiHelper.AlarmsListener {
	List<Alarm> mCachedAlarms;

	@BindView(R.id.alarms_pager)
	ViewPager mAlarmsViewPager;
	@BindView(R.id.bottom_text_switcher)
	ViewFlipper mBottomTextSwitcher;
	@BindView(R.id.left_button)
	ImageButton mLeftButton;
	@BindView(R.id.right_button)
	ImageButton mRightButton;
	@BindView(R.id.circular_progress_bar)
	ProgressBar mProgressBar;

	@Override
	protected int getLayoutId() {
		return R.layout.fragment_alarms;
	}

	@NonNull
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = super.onCreateView(inflater, container, savedInstanceState);
		int arrowColor = ContextCompat.getColor(getActivity(), R.color.colorAccent);
		Drawable leftArrow = mUiUtils.getIconDrawable(
				R.drawable.ic_keyboard_arrow_left_white_48dp, arrowColor);
		mLeftButton.setImageDrawable(leftArrow);
		Drawable rightArrow = mUiUtils.getIconDrawable(
				R.drawable.ic_keyboard_arrow_right_white_48dp, arrowColor);
		mRightButton.setImageDrawable(rightArrow);
		mAlarmsViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
			@Override
			public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
			}

			@Override
			public void onPageSelected(int position) {
				syncAlarmArrowsVisibility();
			}

			@Override
			public void onPageScrollStateChanged(int state) {
			}
		});
		return view;
	}

	@Override
	public void onResume() {
		super.onResume();
		((AlarmsUiHelper.AlarmsProvider) getParentFragment()).notifyAlarmsListener();
	}

	@Override
	public void onDestroyView() {
		mAlarmsViewPager.setAdapter(null);
		super.onDestroyView();
	}

	@OnClick(R.id.mute_sirens_button)
	void muteSirens() {
		((AlarmsParentFragment) getParentFragment()).muteSirens();
	}

	@OnClick(R.id.right_button)
	void nextPage() {
		if (mAlarmsViewPager.getCurrentItem() + 1 < mAlarmsViewPager.getAdapter().getCount()) {
			mAlarmsViewPager.setCurrentItem(mAlarmsViewPager.getCurrentItem() + 1, true);
		}
	}

	@OnClick(R.id.left_button)
	void previousPage() {
		if (mAlarmsViewPager.getCurrentItem() > 0) {
			mAlarmsViewPager.setCurrentItem(mAlarmsViewPager.getCurrentItem() - 1, true);
		}
	}

	public void onEventVideoClick(Integer evtId) {
		((AlarmsParentFragment) getParentFragment()).showVideoForEvent(evtId);
	}

	@Override
	public void onAlarmsUpdated(@Nullable final List<Alarm> alarms) {
		if (mCachedAlarms == null
				|| !mCachedAlarms.equals(alarms)
				|| mCachedAlarms.size() != alarms.size()
				|| mAlarmsViewPager.getAdapter() == null) {
			mCachedAlarms = alarms;
			mBottomTextSwitcher.removeAllViews();
			boolean shouldAggregate = PreferenceManager.getDefaultSharedPreferences(getActivity())
					.getBoolean(ApplicationSettingsFragment.ALARMS_AGGREGATION, false);
			if (mCachedAlarms != null && shouldAggregate) {
				Map<AlarmType, Alarm> aggregatedAlarms = new HashMap<>();
				for (Alarm alarm : mCachedAlarms) {
					Alarm cachedAlarm = aggregatedAlarms.get(alarm.alarm_type);
					if (cachedAlarm == null
							|| alarm.has_video && !cachedAlarm.has_video
							|| alarm.has_video && alarm.compareTo(cachedAlarm) > 1
							|| !alarm.has_video && !cachedAlarm.has_video && alarm.compareTo(cachedAlarm) > 1) {
						aggregatedAlarms.put(alarm.alarm_type, alarm);
					}
				}
				Collection<Alarm> values = aggregatedAlarms.values();
				ArrayList<Alarm> aggregatedAlarmsList = new ArrayList<>(values);
				Collections.sort(aggregatedAlarmsList);
				Collections.reverse(aggregatedAlarmsList);
				mAlarmsViewPager.setAdapter(new AlarmsPagerAdapter(getChildFragmentManager(),
						aggregatedAlarmsList));
			} else {
				List<Alarm> sortedData;
				if (mCachedAlarms == null) {
					sortedData = Collections.emptyList();
				} else {
					sortedData = new ArrayList<>(mCachedAlarms);
					Collections.sort(sortedData);
					Collections.reverse(sortedData);
				}
				mAlarmsViewPager.setAdapter(new AlarmsPagerAdapter(getChildFragmentManager(), sortedData));
			}
			if (mCachedAlarms != null) {
				for (Alarm alarm : mCachedAlarms) {
					TextView alarmText = (TextView) LayoutInflater.from(getActivity())
							.inflate(R.layout.alarm_title_element_small, mBottomTextSwitcher, false);
					StringBuilder alarmDesc = new StringBuilder();
					alarmDesc.append(getString(alarm.device_type.getTextResource()));
					if (alarm.zone != null) {
						alarmDesc.append(" ").append(alarm.zone);
					}
					alarmDesc.append(" - ");
					if (alarm.location != null) {
						alarmDesc.append(alarm.location).append(" - ");
					}
					alarmDesc.append(getString(UiUtils.getStringIdForAlarmType(alarm)));
					alarmText.setText(alarmDesc.toString());
					mBottomTextSwitcher.addView(alarmText);
				}
			}
			if (mBottomTextSwitcher.getChildCount() == 1) {
				mBottomTextSwitcher.stopFlipping();
			} else {
				mBottomTextSwitcher.startFlipping();
			}
			syncAlarmArrowsVisibility();
		}
		syncProgressState();
	}

	public void syncAlarmArrowsVisibility() {
		if (mAlarmsViewPager.getCurrentItem() == 0) {
			mLeftButton.setVisibility(View.GONE);
		} else {
			mLeftButton.setVisibility(View.VISIBLE);
		}
		if (mAlarmsViewPager.getCurrentItem() == mAlarmsViewPager.getAdapter().getCount() - 1) {
			mRightButton.setVisibility(View.GONE);
		} else {
			mRightButton.setVisibility(View.VISIBLE);
		}
	}

	private void syncProgressState() {
		mProgressBar.setVisibility(mCachedAlarms == null ? View.VISIBLE : View.GONE);
	}

	public static class AlarmsPagerAdapter extends FragmentStatePagerAdapter {
		private static final int ALARMS_PER_PAGE = 6;
		@NonNull
		private final List<Alarm> data;

		public AlarmsPagerAdapter(FragmentManager fm, @Nullable List<Alarm> data) {
			super(fm);
			if (data != null) {
				this.data = data;
			} else {
				this.data = Collections.emptyList();
			}
		}

		@Override
		public Fragment getItem(int position) {
			int begin = position * 6;
			int end = (position + 1) * 6;
			if (end >= data.size()) {
				end = data.size();
			}
			return AlarmsPageFragment.createInstance(new ArrayList<>(data.subList(begin, end)));
		}

		@Override
		public int getCount() {
			if (data.size() > 0 && data.size() % ALARMS_PER_PAGE == 0) {
				return data.size() / ALARMS_PER_PAGE;
			} else {
				return data.size() / ALARMS_PER_PAGE + 1;
			}
		}

		@Override
		public Parcelable saveState() {
			return null;
		}
	}

	public static class AlarmsPageFragment extends BaseFragment {
		private static final String DATA = "data";
		private static final String TAG = "AlarmsPageFragment";

		@BindView(R.id.alarm_row_container)
		LinearLayout alarmRowContainer;

		@Override
		protected int getLayoutId() {
			return R.layout.fragment_alarm_page;
		}

		@NonNull
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			View view = super.onCreateView(inflater, container, savedInstanceState);
			ArrayList<Alarm> data = getArguments().getParcelableArrayList(DATA);
			assert data != null;
			int itemsInRow = data.size() <= 4 ? 2 : 3;
			if (data.size() == 0) {
				showNoAlarms();
			} else {
				for (int i = 0; i < data.size(); i++) {
					final LinearLayout row;
					if (i % itemsInRow == 0) {
						row = createRowLayout();
					} else {
						row = (LinearLayout) alarmRowContainer.getChildAt(i / itemsInRow);
					}
					Alarm alarm = data.get(i);
					addAlarmView(row, alarm);
				}
			}
			return view;
		}

		private void showNoAlarms() {
			TextView noAlarms = (TextView) LayoutInflater.from(getActivity())
				.inflate(R.layout.alarm_title_element, alarmRowContainer, false);
			noAlarms.setText(R.string.no_alarms);
			alarmRowContainer.addView(noAlarms);
		}

		@NonNull
		private LinearLayout createRowLayout() {
			LinearLayout row;
			row = new LinearLayout(getActivity());
			LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
					LinearLayout.LayoutParams.MATCH_PARENT, 0, 1);
			row.setLayoutParams(params);
			row.setGravity(Gravity.CENTER);
			alarmRowContainer.addView(row);
			return row;
		}

		private void addAlarmView(@NonNull ViewGroup container, @Nullable final Alarm alarm) {
			int drawableIdForAlarmType = 0;
			boolean hasVideo = false;

			if (alarm != null) {
				drawableIdForAlarmType = UiUtils.getDrawableIdForAlarmType(alarm);
				hasVideo = alarm.has_video;
			}

			View itemView = LayoutInflater.from(container.getContext())
					.inflate(R.layout.alarm_item, container, false);
			ImageView alarmIcon = (ImageView) itemView.findViewById(R.id.alarm_icon);
			alarmIcon.setImageResource(drawableIdForAlarmType);
			ImageButton playButton = (ImageButton) itemView.findViewById(R.id.play_button);
			if (hasVideo) {
				playButton.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						((AlarmsFragment) getParentFragment()).onEventVideoClick(alarm.evt_id);
					}
				});
			} else {
				playButton.setVisibility(View.GONE);
			}
			container.addView(itemView);
		}

		public static AlarmsPageFragment createInstance(ArrayList<Alarm> data) {
			Bundle bundle = new Bundle();
			bundle.putParcelableArrayList(DATA, data);

			AlarmsPageFragment fragment = new AlarmsPageFragment();
			fragment.setArguments(bundle);
			return fragment;
		}
	}
}
