package com.visonic.visonicalerts.ui.fragments.functionalfragments;

import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.visonic.visonicalerts.R;
import com.visonic.visonicalerts.data.datamanager.BaseDataManager;
import com.visonic.visonicalerts.data.DataLoadingSubject;
import com.visonic.visonicalerts.data.datamanager.ListDataManager;
import com.visonic.visonicalerts.data.model.AbstractVideo;
import com.visonic.visonicalerts.data.model.AlarmPreview;
import com.visonic.visonicalerts.data.model.Camera;
import com.visonic.visonicalerts.data.model.VideoProcessStatus;
import com.visonic.visonicalerts.ui.BaseRecyclerViewAdapter;
import com.visonic.visonicalerts.ui.CanShowVideoPlayer;
import com.visonic.visonicalerts.ui.DataBindable;
import com.visonic.visonicalerts.ui.UiUtils;
import com.visonic.visonicalerts.ui.fragments.BaseFragment;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import butterknife.BindView;

public class CamerasFragment extends BaseFragment
		implements DataLoadingSubject.DataLoadingCallbacks {
	private static final String TAG = "CamerasFragment";

	private static final String EVENT_ID = "event_id";
	private static final int UNACCEPTABLE_ID = -1;
	@BindView(R.id.list)
	RecyclerView mCameraGreed;
	@BindView(R.id.circular_progress_bar)
	ProgressBar mProgressBar;
	// @BindViewInt(R.integer.num_columns)
	int columns = 2;

	private BaseRecyclerViewAdapter<? extends AbstractVideo, ? extends AbstractVideoViewHolder>
			mRecyclerViewAdapter;
	private ListDataManager<? extends AbstractVideo> mDataManager;
	private boolean mIsAlert = false;
	private int mEventId = UNACCEPTABLE_ID;
	private int mCameraId = UNACCEPTABLE_ID;
	@Nullable
	private Set<String> mImagePathsSet;

	@NonNull
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = super.onCreateView(inflater, container, savedInstanceState);

		if (getArguments() != null) {
			mEventId = getArguments().getInt(EVENT_ID, UNACCEPTABLE_ID);
			mIsAlert = mEventId != UNACCEPTABLE_ID;
			mCameraId = getArguments().getInt(CameraParentFragment.CAMERA_ID, UNACCEPTABLE_ID);
		}
		if (mIsAlert) {
			mDataManager = new ListDataManager<AlarmPreview>(mLoginPrefs) {
				@Override
				protected void visonicServiceCall(SimpleListCallback<AlarmPreview> callback) {
					getVisonicService().alarmPreviews(mLoginPrefs.getSessionToken(),
							mEventId).enqueue(callback);
				}

				@Override
				protected Class<AlarmPreview> getDataClass() {
					return null;
				}
			};
			mRecyclerViewAdapter = new AlarmVideoAdapter();
		} else {
			mDataManager = new ListDataManager<Camera>(mLoginPrefs) {
				@Override
				protected void visonicServiceCall(SimpleListCallback<Camera> callback) {
					getVisonicService().cameras(mLoginPrefs.getSessionToken()).enqueue(callback);
				}

				@Override
				protected Class<Camera> getDataClass() {
					return Camera.class;
				}
			};
			mRecyclerViewAdapter = new CameraAdapter();
		}
		mDataManager.addCallbacks(this);
		mCameraGreed.setLayoutManager(new GridLayoutManager(getActivity(), columns));
		mCameraGreed.setAdapter(mRecyclerViewAdapter);
		return view;
	}

	@Override
	protected int getLayoutId() {
		return R.layout.fragment_cameras;
	}

	@Override
	public void onResume() {
		super.onResume();
		if (mDataManager != null) {
			mDataManager.startPolling();
		}
		syncProgressState();
	}

	@Override
	public void onPause() {
		super.onPause();
		if (mDataManager != null) {
			mDataManager.stopPolling();
		}
		if (mImagePathsSet != null) {
			for (String path : mImagePathsSet) {
				BaseDataManager.getPicassoInstance(getActivity()).invalidate(path);
			}
			mImagePathsSet = null;
		}
	}

	@Override
	public void dataStartedLoading() {
		// TODO: 1/31/16 start animation
	}

	@SuppressWarnings("unchecked")
	@Override
	public void dataFinishedLoading() {
		// TODO: 2/1/16 stop animation
		List<? extends AbstractVideo> data = mDataManager.getData();
		mUiUtils.handleError(mDataManager.getResult());
		if (mCameraId != UNACCEPTABLE_ID) {
			List<Camera> cameras = (List<Camera>) data;
			for (Camera camera : cameras) {
				if (camera.zone == mCameraId) {
					((CameraParentFragment) getParentFragment()).showVideoPlayer(camera, true, camera.preenroll);
				}
			}
			getArguments().remove(CameraParentFragment.CAMERA_ID);
		}
		if (mIsAlert) {
			((AlarmVideoAdapter) mRecyclerViewAdapter).setData((List<AlarmPreview>) data);
		} else {
			((CameraAdapter) mRecyclerViewAdapter).setData((List<Camera>) data);
		}
		syncProgressState();
	}

	public static CamerasFragment createInstance(int event_id) {
		Bundle args = new Bundle();
		args.putInt(EVENT_ID, event_id);
		CamerasFragment fragment = new CamerasFragment();
		fragment.setArguments(args);
		return fragment;
	}

	public void addCachedPath(String path) {
		if (mImagePathsSet == null) {
			mImagePathsSet = new HashSet<>();
		}
		mImagePathsSet.add(path);
	}

	private void syncProgressState() {
		if (mDataManager != null && isVisible()) {
			mProgressBar.setVisibility(mDataManager.getData() == null ? View.VISIBLE : View.GONE);
		}
	}

	private class CameraAdapter extends BaseRecyclerViewAdapter<Camera, CameraAdapter.CameraViewHolder> {
		@Override
		protected CameraViewHolder getNewViewHolder(ViewGroup parent) {
			return new CameraViewHolder(getViewFromLayout(R.layout.camera_item, parent));
		}

		public class CameraViewHolder extends AbstractVideoViewHolder<Camera> {
			public CameraViewHolder(View itemView) {
				super(itemView, CamerasFragment.this);
			}

			@Override
			public void bind(final Camera camera) {
				super.bind(camera);
				zone.setText(getString(R.string.zone_pattern, camera.zone));
				itemView.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						((CameraParentFragment) fragment.getParentFragment())
								.showVideoPlayer(camera, false, camera.preenroll);
					}
				});
				if (camera.preenroll) {
					status.setText(R.string.pre_enrolled);
				} else if (camera.status == VideoProcessStatus.FAILED) {
					status.setText(R.string.failed);
				} else if (camera.preview_path == null) {
					status.setText(R.string.no_video_yet);
				} else {
					status.setText("");
				}
			}
		}
	}

	private class AlarmVideoAdapter extends BaseRecyclerViewAdapter<AlarmPreview,
			AlarmVideoAdapter.EventVideoViewHolder> {
		@Override
		protected EventVideoViewHolder getNewViewHolder(ViewGroup parent) {
			View view = getViewFromLayout(R.layout.camera_item, parent);
			view.findViewById(R.id.main_view).setBackgroundResource(R.drawable.alarm_video_item_background);
			view.findViewById(R.id.rounded_corners).setBackgroundResource(R.drawable.alarm_video_rounded_corners);
			return new EventVideoViewHolder(view);
		}

		public class EventVideoViewHolder extends AbstractVideoViewHolder<AlarmPreview> {
			public EventVideoViewHolder(View itemView) {
				super(itemView, CamerasFragment.this);
			}

			@Override
			public void bind(final AlarmPreview alarmPreview) {
				super.bind(alarmPreview);
				zone.setText(getString(R.string.zone_pattern, alarmPreview.camera_id));
				itemView.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						((CanShowVideoPlayer) fragment.getParentFragment())
								.showVideoPlayer(alarmPreview, mEventId);
					}
				});
			}
		}
	}

	public static abstract class AbstractVideoViewHolder<T extends AbstractVideo> extends RecyclerView.ViewHolder
			implements DataBindable<T> {
		private final DateFormat dateFormat;
		protected final TextView date;
		protected final TextView time;
		protected final View itemView;
		protected final ImageView cameraPreview;
		protected final TextView zone;
		protected final TextView location;
		protected final CamerasFragment fragment;
		protected final AppCompatTextView status;

		public AbstractVideoViewHolder(View itemView, CamerasFragment fragment) {
			super(itemView);
			this.itemView = itemView;
			this.fragment = fragment;
			date = (TextView) itemView.findViewById(R.id.date);
			time = (TextView) itemView.findViewById(R.id.time);
			cameraPreview = (ImageView) itemView.findViewById(R.id.camera_preview);
			location = (TextView) itemView.findViewById(R.id.location);
			zone = (TextView) itemView.findViewById(R.id.zone_number);
			status = (AppCompatTextView) itemView.findViewById(R.id.video_status_text);
			dateFormat = android.text.format.DateFormat.getMediumDateFormat(fragment.getActivity());
		}

		@CallSuper
		public void bind(final T abstractVideo) {
			location.setText(abstractVideo.location);

			if (abstractVideo.timestamp != null) {
				try {
					Date timeStamp = UiUtils.TIMESTAMP_FORMAT.parse(abstractVideo.timestamp);
					date.setText(dateFormat.format(timeStamp));
					time.setText(fragment.mUiUtils.getDisplayTimeFormat().format(timeStamp));
				} catch (ParseException e) {
					throw new RuntimeException("String:" + abstractVideo.timestamp, e);
				}
			} else {
				date.setText(R.string.not_available_abbr);
				time.setText(R.string.not_available_abbr);
			}

			if (abstractVideo.preview_path != null) {
				String imageUrl = String.format("https://%s%s", fragment.mLoginPrefs.getHostAddress(),
						abstractVideo.preview_path);
				BaseDataManager.getPicassoInstance(fragment.getActivity()).load(imageUrl)
						.into(cameraPreview);
				fragment.addCachedPath(imageUrl);
			}
		}
	}
}
