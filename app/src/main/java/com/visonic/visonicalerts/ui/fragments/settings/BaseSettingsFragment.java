package com.visonic.visonicalerts.ui.fragments.settings;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.visonic.visonicalerts.data.prefs.LoginPrefs;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public abstract class BaseSettingsFragment extends PreferenceFragmentCompat {
	private static final String TAG = "BaseSettingsFragment";
	protected LoginPrefs mLoginPrefs;
	private Unbinder mUnbinder;

	@Override
	@CallSuper
	public void onAttach(Context context) {
		super.onAttach(context);
		mLoginPrefs = LoginPrefs.getInstance(getActivity());
	}

	public void initWithFileName() {
		String filename = getPreferenceName(getActivity(), mLoginPrefs);
		getPreferenceManager().setSharedPreferencesName(filename);
	}

	public static String getPreferenceName(Context context, LoginPrefs loginPrefs) {
		//noinspection ConstantConditions
		return getDefaultSharedPreferencesName(context)
				+ loginPrefs.getHostAddress().hashCode() + loginPrefs.getPanelId().hashCode();
	}

	private static String getDefaultSharedPreferencesName(Context context) {
		return context.getPackageName() + "_preferences";
	}

	@Override
	@CallSuper
	@NonNull
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View view = super.onCreateView(inflater, container, savedInstanceState);
		assert view != null;
		mUnbinder = ButterKnife.bind(this, view);
		return view;
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		mUnbinder.unbind();
	}
}
