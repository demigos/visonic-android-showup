package com.visonic.visonicalerts.ui;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.preference.PreferenceManager;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.visonic.visonicalerts.R;
import com.visonic.visonicalerts.ui.fragments.settings.ApplicationSettingsFragment;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class FilterAdapter<T> extends RecyclerView.Adapter<FilterAdapter.FilterViewHolder> {
	private final List<Filter<? extends Set<T>>> mFilters;
	private final Set<Filter<? extends Set<T>>> mActiveItems = new HashSet<>();
	private final Context mContext;
	private final UiUtils mUiUtils;
	private final HashSet<T> mFilterSet;
	private final SharedPreferences mSharedPreferences;
	@Nullable
	private final Filter<? extends Set<T>> mAllFilter;

	@Nullable
	private List<FiltersChangedListener<T>> mListeners;

	public FilterAdapter(@NonNull Context context,
						 @NonNull List<Filter<? extends Set<T>>> filters,
						 @NonNull HashSet<T> filterSet,
						 @NonNull Filter<? extends Set<T>> initialActive,
						 @Nullable Filter<? extends Set<T>> allFilter) {
		this.mContext = context.getApplicationContext();
		this.mFilterSet = filterSet;
		mFilters = filters;
		mUiUtils = new UiUtils(context);
		setHasStableIds(true);
		mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
		mActiveItems.add(initialActive);
		mAllFilter = allFilter;
	}

	@Override
	public FilterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		final FilterViewHolder holder = new FilterViewHolder(LayoutInflater.from(mContext)
				.inflate(R.layout.filter_item, parent, false), mContext, mUiUtils);
		holder.itemView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				final int position = holder.getAdapterPosition();
				if (position == RecyclerView.NO_POSITION) return;
				final Filter<? extends Set<T>> filter = mFilters.get(position);
				boolean advacedFiltering = mSharedPreferences.getBoolean(
						ApplicationSettingsFragment.ADVANCED_FILTER, false);
				if (advacedFiltering) {
					if (mActiveItems.contains(filter)) {
						mActiveItems.remove(filter);
					} else {
						if (filter == mAllFilter) {
							mActiveItems.clear();
						} else {
							mActiveItems.remove(mAllFilter);
						}
						mActiveItems.add(filter);
					}
				} else {
					mActiveItems.clear();
					mActiveItems.add(filter);
				}
				notifyDataSetChanged();
				dispatchFiltersChanged();
			}
		});
		return holder;
	}

	@Override
	public void onBindViewHolder(FilterViewHolder holder, int position) {
		holder.bindFilter(mFilters.get(position), mActiveItems.contains(mFilters.get(position)));
	}

	@Override
	public int getItemCount() {
		return mFilters.size();
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	static class FilterViewHolder extends RecyclerView.ViewHolder {
		final TextView itemView;
		private final Context mContext;
		private final UiUtils mUiUtils;

		public FilterViewHolder(View itemView, Context context, UiUtils uiUtils) {
			super(itemView);
			mContext = context;
			mUiUtils = uiUtils;
			this.itemView = (TextView) itemView;
		}

		public void bindFilter(Filter filter, boolean isActive) {
			// TODO: 1/30/16 Implement is active
			itemView.setText(filter.getTitle());
			Drawable drawable = mUiUtils.getIconDrawable(filter.getIconId());
			itemView.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
			UiUtils.setListItemColor(itemView, isActive ? mUiUtils.getHighlightColor()
					: mUiUtils.getTextColorPrimary());
		}
	}

	public void addFilterChangedListener(FiltersChangedListener<T> listener) {
		if (mListeners == null) {
			mListeners = new ArrayList<>();
		}
		mListeners.add(listener);
	}

	public void removeFilterChangedListener(FiltersChangedListener<T> listener) {
		if (mListeners != null) {
			mListeners.remove(listener);
		}
	}

	private void dispatchFiltersChanged() {
		if (mListeners != null) {
			try {
				mFilterSet.clear();
				boolean hasVideo = false;
				for (Filter<? extends Set<T>> activeItem : mActiveItems) {
					mFilterSet.addAll(activeItem.getContent());
					hasVideo = hasVideo || activeItem.hasVideo();
				}
				for (FiltersChangedListener<T> listener : mListeners) {
					listener.onFiltersChanged(mFilterSet, hasVideo);
				}
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
	}

	public static class Filter<S> {
		@DrawableRes
		private final int mIconId;
		@NonNull
		private final String mTitle;
		@NonNull
		private final S mContent;
		private final boolean mHasVideo;

		public Filter(int iconId, @NonNull String title, @NonNull S content) {
			this.mIconId = iconId;
			this.mTitle = title;
			mContent = content;
			mHasVideo = false;
		}

		public Filter(int iconId, @NonNull String title, @NonNull S content, boolean hasVideo) {
			this.mIconId = iconId;
			this.mTitle = title;
			mContent = content;
			mHasVideo = hasVideo;
		}

		@DrawableRes
		public int getIconId() {
			return mIconId;
		}

		@NonNull
		public String getTitle() {
			return mTitle;
		}

		@NonNull
		public S getContent() {
			return mContent;
		}

		public boolean hasVideo() {
			return mHasVideo;
		}
	}

	public interface FiltersChangedListener<P> {
		void onFiltersChanged(Set<P> includedTypes, boolean hasVideo);
	}
}
