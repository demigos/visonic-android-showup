package com.visonic.visonicalerts.ui;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

public class MyBroadcastReceiver extends WakefulBroadcastReceiver {
	public void onReceive(Context context, Intent intent) {
		ComponentName comp = new ComponentName(context.getPackageName(), PushNotificationService.class.getName());
		startWakefulService(context, intent.setComponent(comp));
	}
}