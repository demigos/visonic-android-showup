package com.visonic.visonicalerts.ui;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.preference.PreferenceManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import com.visonic.visonicalerts.BuildConfig;
import com.visonic.visonicalerts.R;
import com.visonic.visonicalerts.ui.fragments.settings.NotificationSettingsFragment;

import java.io.IOException;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class AlarmActivity extends AppCompatActivity {
    public static final String TAG = "AlarmActivity";
    public static final String ARG_MESSAGE = "com.visonic.visonicalerts.ui.AlarmActivity.ARG_MESSAGE";
    private TextView alarmMessage;
    private MediaPlayer mediaPlayer;
    private final Handler alarmHandler = new Handler();
    private boolean alarmNotificationSoundEnabled;
    /**
     * Touch listener to use for in-layout UI controls to delay hiding the
     * system UI. This is to prevent the jarring behavior of controls going away
     * while interacting with activity UI.
     */
    private final View.OnTouchListener mDelayHideTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (alarmNotificationSoundEnabled) {
                try {
                    if (mediaPlayer.isPlaying()) {
                        mediaPlayer.stop();
                        mediaPlayer.release();
                    }
                } catch (IllegalStateException e) {
                    Log.d(TAG, "Player already stopped.", e);
                }
            }
            finish();
            return true;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        alarmNotificationSoundEnabled = PreferenceManager.getDefaultSharedPreferences(this)
            .getBoolean(NotificationSettingsFragment.ALARMS_SOUND, false);
        if (alarmNotificationSoundEnabled) {
            try {
                mediaPlayer = new MediaPlayer();
                mediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
                mediaPlayer.setDataSource(this, Uri.parse("android.resource://" + BuildConfig.APPLICATION_ID + "/" + R.raw.alarm_sound));
                mediaPlayer.prepare();
            } catch (IOException e) {
                mediaPlayer = MediaPlayer.create(this, R.raw.alarm_sound);
                Log.e(TAG, "Can't set datasource for media player, using  default", e);
            }
            mediaPlayer.setLooping(true);
            mediaPlayer.setScreenOnWhilePlaying(true);
            mediaPlayer.start();
            alarmHandler.postDelayed(new StopAlarmTask(mediaPlayer, alarmNotificationSoundEnabled),
                BuildConfig.ALARM_SOUND_DURATION_MILLIS);
        }

        setContentView(R.layout.activity_alarm);

        if (getIntent() != null) {
            alarmMessage = (TextView) findViewById(R.id.activity_alarm_message);
            final String message = getIntent().getStringExtra(ARG_MESSAGE);
            alarmMessage.setText(message);
        }

        findViewById(R.id.dismiss_button).setOnTouchListener(mDelayHideTouchListener);
    }


    public static Intent createStartIntent(Context context, String message) {
        Intent startIntent = new Intent(context, AlarmActivity.class);
        startIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startIntent.addFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        startIntent.putExtra(AlarmActivity.ARG_MESSAGE, message);
        return startIntent;
    }

    private static final class StopAlarmTask implements Runnable {

        private final MediaPlayer mediaPlayer;
        private boolean alarmNotificationSoundEnabled;

        public StopAlarmTask(MediaPlayer mediaPlayer, boolean alarmNotificationSoundEnabled) {
            this.mediaPlayer = mediaPlayer;
            this.alarmNotificationSoundEnabled = alarmNotificationSoundEnabled;
        }

        @Override
        public void run() {
            if (alarmNotificationSoundEnabled) {
                try {
                    if (mediaPlayer.isPlaying()) {
                        mediaPlayer.stop();
                        mediaPlayer.release();
                    }
                } catch (IllegalStateException e) {
                    Log.d(TAG, "Player already stopped.", e);
                }
            }
        }
    }
}
