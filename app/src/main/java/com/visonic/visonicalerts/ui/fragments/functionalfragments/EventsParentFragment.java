package com.visonic.visonicalerts.ui.fragments.functionalfragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.visonic.visonicalerts.R;
import com.visonic.visonicalerts.data.model.AlarmPreview;
import com.visonic.visonicalerts.ui.CanShowVideoPlayer;

/**
 * A simple {@link Fragment} subclass.
 */
public class EventsParentFragment extends BaseFunctionalFragment
		implements CanShowVideoPlayer {
	private EventsFragment mEventsFragment = new EventsFragment();

	@NonNull
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = super.onCreateView(inflater, container, savedInstanceState);
		if (getArguments() != null) {
			mEventsFragment.setArguments(getArguments());
		}

		getChildFragmentManager().beginTransaction()
				.replace(R.id.fragmentContainer, mEventsFragment)
				.commit();
		return view;
	}

	public void showVideoForEvent(int eventId) {
		getChildFragmentManager().beginTransaction()
				.replace(R.id.fragmentContainer, CamerasFragment.createInstance(eventId))
				.addToBackStack(null)
				.commit();
	}

	@Override
	public void showVideoPlayer(AlarmPreview camera, int eventId) {
		getChildFragmentManager().beginTransaction()
				.replace(R.id.fragmentContainer, VideoPlayerFragment.createAlertInstance(camera, eventId))
				.addToBackStack(null)
				.commit();
	}

	@Override
	protected int getLayoutId() {
		return R.layout.fragment_with_subfragments;
	}

	@Override
	public boolean onBackButtonPressed() {
		if (getChildFragmentManager().getBackStackEntryCount() >= 1) {
			getChildFragmentManager().popBackStack();
			return true;
		}
		return false;
	}

	public static Fragment newInstance(Bundle bundle) {
		Fragment result = new EventsParentFragment();
		if (bundle != null) {
			result.setArguments(bundle);
		}
		return result;
	}
}
