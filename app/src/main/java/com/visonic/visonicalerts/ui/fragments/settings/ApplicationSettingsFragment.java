package com.visonic.visonicalerts.ui.fragments.settings;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v7.preference.ListPreference;
import android.support.v7.preference.Preference;

import com.visonic.visonicalerts.BuildConfig;
import com.visonic.visonicalerts.R;

import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class ApplicationSettingsFragment extends BaseSettingsFragment
		implements SharedPreferences.OnSharedPreferenceChangeListener {
	private static final String TAG = "ApplicationSettingsFrag";
	private static final String VERSION = "version";
	public static final String LOCALIZATION_LANGUAGE = "localization_language";
	public static final String REMEMBER_LOGIN_INFO = "remember_login_info";
	public static final String ALARMS_AGGREGATION = "alarms_aggregation";
	public static final String ARM_BY_SIMPLE_TOUCH = "arm_by_simple_touch";
	public static final String ADVANCED_FILTER = "advanced_filter";
	public static final String SHOW_HELP_TOOLTIPS = "show_help_tooltips";

	// Locale staff
	public static final String ENGLISH_KEY = "english";
	public static final String DEFAULT_KEY = "default";
	public static final String FRENCH_KEY = "french";
	public static final String GERMAN_KEY = "german";
	public static final String SPAIN_KEY = "spain";
	public static final String ITALIAN_KEY = "italian";
	public static final String SWEDISH_KEY = "swedish";
	public static final String HEBREW_KEY = "hebrew";
	public static final String RUSSIAN_KEY = "russian";
	public static final String CHINESE_KEY = "chinese";
	public static final String DANISH_KEY = "danish";
	public static final String PORTUGAL_KEY = "portugal";
	public static final String NORWEGIAN_KEY = "norwegian";
	public static final String FINNISH_KEY = "finnish";
	public static final String POLISH_KEY = "polish";

	public static final Map<String, Localization> KEY_LOCALE_MAP = new HashMap<>();

	@Override
	public void onCreatePreferences(Bundle bundle, String s) {
		addPreferencesFromResource(R.xml.preferences);
		findPreference(VERSION).setSummary(BuildConfig.VERSION_NAME);

		Localization[] locales = Localization.values();
		String[] localeKeys = new String[locales.length];
		String[] localeTitles = new String[locales.length];
		for (int i = 0; i < locales.length; i++) {
			localeKeys[i] = locales[i].getKey();
			localeTitles[i] = getString(locales[i].getStringId());
		}
		final ListPreference localePreference = (ListPreference) findPreference(LOCALIZATION_LANGUAGE);
		localePreference.setEntries(localeTitles);
		localePreference.setEntryValues(localeKeys);
		localePreference.setSummary(Localization.getByKey(localePreference.getValue()).getStringId());
		localePreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
			@Override
			public boolean onPreferenceChange(Preference preference, Object o) {
				Locale locale = Localization.getByKey(localePreference.getValue()).getLocale();
				Configuration config = new Configuration();
				config.locale = locale;
				Context baseContext = getActivity().getBaseContext();
				Resources resources = baseContext.getResources();
				resources.updateConfiguration(config, resources.getDisplayMetrics());
				Intent i = baseContext.getPackageManager()
						.getLaunchIntentForPackage(baseContext.getPackageName());
				i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(i);
				return true;
			}
		});

		RememberLoginInfo[] loginInfos = RememberLoginInfo.values();
		String[] loginInfoKeys = new String[loginInfos.length];
		String[] loginInfoTitles = new String[loginInfos.length];
		for (int i = 0; i < loginInfos.length; i++) {
			loginInfoKeys[i] = loginInfos[i].getKey();
			loginInfoTitles[i] = getString(loginInfos[i].getStringId());
		}
		ListPreference rememberLoginInfoPreference =
				(ListPreference) findPreference(REMEMBER_LOGIN_INFO);
		rememberLoginInfoPreference.setEntries(loginInfoTitles);
		rememberLoginInfoPreference.setEntryValues(loginInfoKeys);
		rememberLoginInfoPreference.setSummary(
				RememberLoginInfo.getByKey(rememberLoginInfoPreference.getValue()).getStringId());
	}

	@Override
	public void onResume() {
		super.onResume();
		getPreferenceScreen().getSharedPreferences()
				.registerOnSharedPreferenceChangeListener(this);
	}

	@Override
	public void onPause() {
		super.onPause();
		getPreferenceScreen().getSharedPreferences()
				.unregisterOnSharedPreferenceChangeListener(this);
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
		if (LOCALIZATION_LANGUAGE.equals(key)) {
			ListPreference preference = (ListPreference) findPreference(key);
			preference.setSummary(Localization.getByKey(
					sharedPreferences.getString(key, preference.getValue())).getStringId());

			Intent intent = getActivity().getIntent();
			getActivity().finish();
			startActivity(intent);
		} else if (REMEMBER_LOGIN_INFO.equals(key)) {
			ListPreference preference = (ListPreference) findPreference(key);
			preference.setSummary(RememberLoginInfo.getByKey(
					sharedPreferences.getString(key, preference.getValue())).getStringId());
		}
	}

	@NonNull
	private static Locale getDeviceLocaleIfAvailable() {
		Locale defaultLocale = Locale.getDefault();
		switch (defaultLocale.getLanguage()) {
			case "en":
			case "fr":
			case "de":
			case "zh":
			case "it":
			case "es":
			case "sv":
			case "iw":
			case "ru":
			case "da":
			case "pt":
			case "no":
			case "fi":
			case "pl": return defaultLocale;
			default: return Locale.ENGLISH;
		}
	}

	public enum Localization {
		DEFAULT(R.string.default_language, DEFAULT_KEY, getDeviceLocaleIfAvailable()),
		ENGLISH(R.string.english, ENGLISH_KEY, Locale.ENGLISH),
		FRENCH(R.string.french, FRENCH_KEY, Locale.FRENCH),
		GERMAN(R.string.german, GERMAN_KEY, Locale.GERMAN),
		SPAIN(R.string.spain, SPAIN_KEY, new Locale("es")),
		ITALIAN(R.string.italian, ITALIAN_KEY, Locale.ITALIAN),
		SWEDISH(R.string.swedish, SWEDISH_KEY, new Locale("sv")),
		HEBREW(R.string.hebrew, HEBREW_KEY, new Locale("iw")),
		RUSSIAN(R.string.russian, RUSSIAN_KEY, new Locale("ru")),
		CHINESE(R.string.chinese, CHINESE_KEY, Locale.CHINESE),
		DANISH(R.string.danish, DANISH_KEY, new Locale("da")),
		PORTUGAL(R.string.portugal, PORTUGAL_KEY, new Locale("pt")),
		NORWEGIAN(R.string.norwegian, NORWEGIAN_KEY, new Locale("no")),
		FINNISH(R.string.finnish, FINNISH_KEY, new Locale("fi")),
		POLISH(R.string.polish, POLISH_KEY, new Locale("pl"));
		@StringRes
		private final int mStringId;
		private final String mKey;
		private final Locale mLocale;

		Localization(@StringRes int stringId, String key, Locale locale) {
			mStringId = stringId;
			this.mKey = key;
			mLocale = locale;
			KEY_LOCALE_MAP.put(key, this);
		}

		public String getKey() {
			return mKey;
		}

		public int getStringId() {
			return mStringId;
		}

		public static Localization getByKey(String key) {
			Map<String, Localization> keyLocaleMap = KEY_LOCALE_MAP;
			return keyLocaleMap.get(key);
		}

		public Locale getLocale() {
			return mLocale;
		}

		public static Locale getActiveLocale(Context context) {
			SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
			String key = sharedPref.getString(
					ApplicationSettingsFragment.LOCALIZATION_LANGUAGE, DEFAULT_KEY);
			return ApplicationSettingsFragment.Localization.getByKey(key).getLocale();
		}
	}

	public enum RememberLoginInfo {
		NONE(R.string.none, Constants.NONE_KEY),
		LAST(R.string.last, Constants.LAST_KEY),
		ALL(R.string.all, Constants.ALL_KEY);
		@StringRes
		private int mStringId;
		private String key;

		RememberLoginInfo(@StringRes int stringId, String key) {
			mStringId = stringId;
			this.key = key;
			Constants.keyMap.put(key, this);
		}

		public String getKey() {
			return key;
		}

		public int getStringId() {
			return mStringId;
		}

		public static RememberLoginInfo getByKey(String key) {
			return Constants.keyMap.get(key);
		}

		private static class Constants {
			public static final String NONE_KEY = "none";
			public static final String LAST_KEY = "last";
			public static final String ALL_KEY = "all";
			public static final Map<String, RememberLoginInfo> keyMap = new HashMap<>();
		}
	}
}
