package com.visonic.visonicalerts.ui.fragments;

import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.DrawableRes;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.visonic.visonicalerts.R;
import com.visonic.visonicalerts.ui.BaseRecyclerViewAdapter;
import com.visonic.visonicalerts.ui.DataBindable;
import com.visonic.visonicalerts.ui.fragments.functionalfragments.BaseFunctionalFragment;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import butterknife.BindView;

public class BottomMenuItemsFragment extends BaseFunctionalFragment {
	private static final String MENU_ORDER = "menu_order";
	private static final String SKIPPED_ITEMS = "skipped_items";
	private static final String SEPARATOR = ";";
	private static final String ALARMS_BUTTON_KEY = "alarms_button_key";
	private static final String DEVICES_BUTTON_KEY = "devices_button_key";
	private static final String CAMERAS_BUTTON_KEY = "cameras_button_key";
	private static final String EVENTS_BUTTON_KEY = "events_button_key";
	private static final String ALERTS_BUTTON_KEY = "alerts_button_key";
	private static final String TROUBLES_BUTTON_KEY = "troubles_button_key";
	private static final String KEYFOBS_BUTTON_KEY = "keyfobs_button_key";
	private static final String STATS_BUTTON_KEY = "stats_button_key";
	private static final String DEFAULT_ORDER = STATS_BUTTON_KEY + SEPARATOR
			+ ALARMS_BUTTON_KEY + SEPARATOR
			+ DEVICES_BUTTON_KEY + SEPARATOR
			+ CAMERAS_BUTTON_KEY + SEPARATOR
			+ EVENTS_BUTTON_KEY + SEPARATOR
			+ ALERTS_BUTTON_KEY + SEPARATOR
			+ TROUBLES_BUTTON_KEY;
	private final static Map<String, BottomMenuItem> BOTTOM_MENU_ITEMS = Collections.unmodifiableMap(
			new HashMap<String, BottomMenuItem>() {{
				this.put(STATS_BUTTON_KEY, new BottomMenuItem(STATS_BUTTON_KEY, R.id.statsButton, R.string.main_screen, R.drawable.ic_main_screen));
				this.put(ALARMS_BUTTON_KEY, new BottomMenuItem(ALARMS_BUTTON_KEY, R.id.alarmsButton, R.string.alarms, R.drawable.ic_alarms));
				this.put(DEVICES_BUTTON_KEY, new BottomMenuItem(DEVICES_BUTTON_KEY, R.id.devicesButton, R.string.devices, R.drawable.ic_devices));
				this.put(CAMERAS_BUTTON_KEY, new BottomMenuItem(CAMERAS_BUTTON_KEY, R.id.camerasButton, R.string.cameras, R.drawable.ic_video_events));
				this.put(EVENTS_BUTTON_KEY, new BottomMenuItem(EVENTS_BUTTON_KEY, R.id.eventsButton, R.string.events, R.drawable.ic_events));
				this.put(ALERTS_BUTTON_KEY, new BottomMenuItem(ALERTS_BUTTON_KEY, R.id.alertsButton, R.string.alerts, R.drawable.ic_alerts));
				this.put(TROUBLES_BUTTON_KEY, new BottomMenuItem(TROUBLES_BUTTON_KEY, R.id.troublesButton, R.string.troubles, R.drawable.ic_troubles));
			}});

	@BindView(R.id.list)
	RecyclerView mRecyclerView;
	@BindView(R.id.toolbar)
	Toolbar mToolbar;

	private BottomMenuItemAdapter mAdapter;
	private SharedPreferences mSharedPreferences;

	@NonNull
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		View view = super.onCreateView(inflater, container, savedInstanceState);
		mAdapter = new BottomMenuItemAdapter();
		mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
		mAdapter.setData(getFullBottomMenuOrder(mSharedPreferences));
		mRecyclerView.setAdapter(mAdapter);
		ItemTouchHelper ith = new ItemTouchHelper(new ItemTouchHelper.Callback() {
			@Override
			public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
				return makeFlag(ItemTouchHelper.ACTION_STATE_DRAG,
						ItemTouchHelper.DOWN | ItemTouchHelper.UP);
			}

			@Override
			public boolean onMove(RecyclerView recyclerView,
								  RecyclerView.ViewHolder viewHolder,
								  RecyclerView.ViewHolder target) {
				Collections.swap(mAdapter.getEditableData(), viewHolder.getAdapterPosition(),
						target.getAdapterPosition());
				mAdapter.notifyItemMoved(viewHolder.getAdapterPosition(),
						target.getAdapterPosition());

				StringBuilder newOrder = new StringBuilder();
				for (BottomMenuItem bottomMenuItem : mAdapter.getEditableData()) {
					newOrder.append(bottomMenuItem.getKey()).append(SEPARATOR);
				}
				newOrder.delete(newOrder.length() - 1, newOrder.length());
				mSharedPreferences.edit().putString(MENU_ORDER, newOrder.toString()).apply();
				((MainFragment) getParentFragment()).initBottomPanel();
				return true;
			}

			@Override
			public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {

			}
		});
		ith.attachToRecyclerView(mRecyclerView);
		return view;
	}

	@Override
	protected int getLayoutId() {
		return R.layout.fragment_bottom_menu_items_list;
	}

	public static List<BottomMenuItem> getBottomMenuOrder(SharedPreferences preferences) {
		String order = preferences.getString(MENU_ORDER, DEFAULT_ORDER);
		if (order.length() != DEFAULT_ORDER.length()) {
			preferences.edit().remove(MENU_ORDER).apply();
			order = DEFAULT_ORDER;
		}
		String[] keys = order.split(SEPARATOR);
		List<BottomMenuItem> data = new ArrayList<>(keys.length);
		Set<String> skippedItems = preferences.getStringSet(SKIPPED_ITEMS, new HashSet<String>());
		for (String key : keys) {
			if (!skippedItems.contains(key)) {
				data.add(BOTTOM_MENU_ITEMS.get(key));
			}
		}
		return data;
	}

	public static List<BottomMenuItem> getFullBottomMenuOrder(SharedPreferences preferences) {
		String order = preferences.getString(MENU_ORDER, DEFAULT_ORDER);
		String[] keys = order.split(SEPARATOR);
		List<BottomMenuItem> data = new ArrayList<>(keys.length);
		for (String key : keys) {
			data.add(BOTTOM_MENU_ITEMS.get(key));
		}
		return data;
	}

	private class BottomMenuItemAdapter extends BaseRecyclerViewAdapter<BottomMenuItem, BottomMenuItemAdapter.MenuItemViewHolder> {
		@Override
		protected MenuItemViewHolder getNewViewHolder(ViewGroup parent) {
			return new MenuItemViewHolder(getViewFromLayout(R.layout.bottom_menu_order_item, parent));
		}

		public class MenuItemViewHolder extends RecyclerView.ViewHolder
				implements DataBindable<BottomMenuItem> {
			private TextView name;
			private ToggleButton enableDisableToggleButton;

			public MenuItemViewHolder(View itemView) {
				super(itemView);
				name = (TextView) itemView.findViewById(R.id.filter_name);
				enableDisableToggleButton = (ToggleButton) itemView.findViewById(R.id.enable_disable_toggle);
			}

			@Override
			public void bind(final BottomMenuItem data) {
				name.setText(data.getTitle());
				Drawable drawable = mUiUtils.getIconDrawable(data.getIcon());
				name.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
				Set<String> skippedItems = mSharedPreferences.getStringSet(SKIPPED_ITEMS, new HashSet<String>());
				enableDisableToggleButton.setChecked(!skippedItems.contains(data.getKey()));
				enableDisableToggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
					@Override
					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
						Set<String> skippedItems = mSharedPreferences.getStringSet(SKIPPED_ITEMS, new HashSet<String>());
						if (!isChecked) {
							skippedItems.add(data.getKey());
						} else {
							skippedItems.remove(data.getKey());
						}
						mSharedPreferences.edit().putStringSet(SKIPPED_ITEMS, skippedItems).apply();
						((MainFragment) getParentFragment()).initBottomPanel();
					}
				});
			}
		}
	}

	public static class BottomMenuItem {
		@IdRes
		private final int mId;
		@StringRes
		private final int mTitle;
		@DrawableRes
		private final int mIcon;
		private final String mKey;

		public BottomMenuItem(String key, int id, int title, int icon) {
			mKey = key;
			mId = id;
			mTitle = title;
			mIcon = icon;
		}

		public int getId() {
			return mId;
		}

		public int getTitle() {
			return mTitle;
		}

		public int getIcon() {
			return mIcon;
		}

		public String getKey() {
			return mKey;
		}
	}
}
