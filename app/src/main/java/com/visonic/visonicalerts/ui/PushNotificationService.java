package com.visonic.visonicalerts.ui;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.support.v7.preference.PreferenceManager;

import com.visonic.visonicalerts.R;
import com.visonic.visonicalerts.data.model.PushMessage;

import java.text.MessageFormat;

public class PushNotificationService extends IntentService {
	private static final String NOTIFICATION_ID = "notification_id";

	public PushNotificationService() {
		super("PushNotificationService");
	}

	protected void onHandleIntent(Intent intent) {
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		int notificationId = sharedPreferences.getInt(NOTIFICATION_ID, 0);
		Bundle bundle = intent.getExtras();
		String message = bundle != null ? bundle.getString("message") : null;
		if (message != null) {
			final PushMessage pushMessage = PushMessage.parse(message);

			Intent sendIntent = new Intent(this, MainActivity.class);
			sendIntent.setAction(MainActivity.ACTION_VIEW_PUSH_NOTIFICATION);
			sendIntent.putExtra(MainActivity.ARG_PUSH_NOTIFICATION_MESSAGE, message);
			sendIntent.putExtra(MainActivity.ARG_PUSH_NOTIFICATION_TITLE, pushMessage.getPanel());

			NotificationManager notificationManager =
					(NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
			PendingIntent contentIntent = PendingIntent.getActivity(this, 0, sendIntent,
					PendingIntent.FLAG_UPDATE_CURRENT);

			NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
				.setContentIntent(contentIntent)
				.setSmallIcon(R.drawable.ic_all)
				.setContentTitle(MessageFormat.format(getString(R.string.format_panel), pushMessage.getPanel()))
				.setContentText(pushMessage.getDescription())
				.setStyle(new NotificationCompat.BigTextStyle().bigText(pushMessage.buildMessageBody()))
				.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);

			mBuilder.setColor(0xFF4F4FBF);

			PowerManager pm = (PowerManager) this.getSystemService(Context.POWER_SERVICE);
			PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "TAG");
			wl.acquire(15000);
			notificationId++;
			notificationManager.notify(notificationId, mBuilder.build());
			sharedPreferences.edit().putInt(NOTIFICATION_ID, notificationId).apply();

			if (pushMessage.isAlarm()) {
				Intent alarmActivity = AlarmActivity.createStartIntent(this, message);
				startActivity(alarmActivity);
			}
		}
		MyBroadcastReceiver.completeWakefulIntent(intent);
	}
}