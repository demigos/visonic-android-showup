package com.visonic.visonicalerts.ui.fragments;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.widget.TextView;

import com.visonic.visonicalerts.R;
import com.visonic.visonicalerts.data.datamanager.BaseStatusDataManager;
import com.visonic.visonicalerts.data.DataLoadingSubject;
import com.visonic.visonicalerts.data.Installation;
import com.visonic.visonicalerts.data.datamanager.LoginDataManager;
import com.visonic.visonicalerts.ui.LoginListener;
import com.visonic.visonicalerts.ui.MainActivity;

import java.util.Stack;

import butterknife.BindView;
import butterknife.OnClick;

public class LoginUserCodeFragment extends BaseFragment implements DataLoadingSubject.DataLoadingCallbacks {
	private static final String TAG = "LoginUserCodeFragment";
	public static final int INTERACTIVE_IS_DISABLED = 448;
	public static final int LOGIN_ATTEMPTS_LIMIT_REACHED = 442;
	public static final int MAX_LOGGED_IN_DEVICES_LIMIT_REACHED = 443;
	public static final int WRONG_USER_CODE = 444;
	public static final int UNKNOWN_PAWEL_WEB_NAME = 445;
	public static final int DISCOVERY_IN_PROGRESS = 446;
	public static final int MAX_SESSIONS_LIMIT_REACHED = 447;
	
	private LoginListener mListener;

	private LoginDataManager mDataManager;
	Stack<Integer> userCodeStack = new Stack<>();

	@BindView(R.id.title)
	TextView tittleTextView;
	@BindView(R.id.user_code_text_view)
	TextView userCodeTextView;

	@Override
	protected int getLayoutId() {
		return R.layout.fragment_loging_user_code;
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		if (context instanceof LoginListener) {
			mListener = (LoginListener) context;
		} else {
			throw new RuntimeException(context.toString()
					+ " must implement LoginListener");
		}
		mDataManager = new LoginDataManager(mLoginPrefs);
		mDataManager.addCallbacks(this);
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mListener = null;
	}

	@OnClick(R.id.login_button)
	public void doLogin() {
		StringBuilder userCode = new StringBuilder(userCodeStack.size());
		@SuppressWarnings("unchecked")
		Stack<Integer> userCodeStackCopy = (Stack<Integer>) userCodeStack.clone();
		for (int i = 0; i < userCodeStack.size(); i++) {
			userCode.insert(0, userCodeStackCopy.pop());
		}
		if (!TextUtils.isEmpty(userCode)) {
			mDataManager.doLogin(userCode.toString(), Installation.id(getActivity()));
		} else {
			notifyWrongCode();
		}
	}

	@OnClick(R.id.digit0_button)
	public void push0() {
		push(0);
	}

	@OnClick(R.id.digit1_button)
	public void push1() {
		push(1);
	}

	@OnClick(R.id.digit2_button)
	public void push2() {
		push(2);
	}

	@OnClick(R.id.digit3_button)
	public void push3() {
		push(3);
	}

	@OnClick(R.id.digit4_button)
	public void push4() {
		push(4);
	}

	@OnClick(R.id.digit5_button)
	public void push5() {
		push(5);
	}

	@OnClick(R.id.digit6_button)
	public void push6() {
		push(6);
	}

	@OnClick(R.id.digit7_button)
	public void push7() {
		push(7);
	}

	@OnClick(R.id.digit8_button)
	public void push8() {
		push(8);
	}

	@OnClick(R.id.digit9_button)
	public void push9() {
		push(9);
	}

	@OnClick(R.id.backspace_button)
	public void pop() {
		if (!userCodeStack.isEmpty()) {
			userCodeStack.pop();
			syncTextView();
		}
	}

	private void push(int number) {
		userCodeStack.push(number);
		syncTextView();
	}

	private void syncTextView() {
		StringBuilder stringBuilder = new StringBuilder();
		for (int i = 0; i < userCodeStack.size(); i++) {
			stringBuilder.append("● ");
		}
		userCodeTextView.setText(stringBuilder);
	}

	private void notifyWrongCode() {
		// TODO implement
	}

	@Override
	public void dataStartedLoading() {
		new ProgressDialogFragment().show(getChildFragmentManager(), ProgressDialogFragment.TAG);
	}

	@Override
	public void dataFinishedLoading() {
		Fragment progressDialog = getChildFragmentManager().findFragmentByTag(ProgressDialogFragment.TAG);
		if (progressDialog != null) {
			((ProgressDialogFragment) progressDialog).dismiss();
		}
		BaseStatusDataManager.Result result = mDataManager.getResult();
		switch (result) {
			case OK:
				mListener.onPinCorrect();
				break;
			case HTTP_PROBLEM:
				switch (mDataManager.getCode()) {
					case INTERACTIVE_IS_DISABLED:
						mListener.goBackToLoginWithError(R.string.panel_is_not_authorized);
						break;
					case LOGIN_ATTEMPTS_LIMIT_REACHED:
						String title = getString(R.string.login_attempts_limit_reached_message);
						@SuppressLint("StringFormatMatches")
						String message = getString(R.string.please_try_again_after_seconds_pattern,
								mDataManager.getErrorMessage());
						ErrorMessageDialogFragment.createInstance(title, message)
								.show(getChildFragmentManager(), null);
						break;
					case MAX_LOGGED_IN_DEVICES_LIMIT_REACHED:
						tittleTextView.setText(R.string.session_limit_message);
						break;
					case WRONG_USER_CODE:
						userCodeStack.clear();
						syncTextView();
						tittleTextView.setText(R.string.wrong_password_message);
						break;
					case UNKNOWN_PAWEL_WEB_NAME:
						break;
					case DISCOVERY_IN_PROGRESS:
						break;
					case MAX_SESSIONS_LIMIT_REACHED:
						break;
				}
				break;
			case HOST_NOT_ACCESSIBLE:
				userCodeStack.clear();
				syncTextView();
				tittleTextView.setText(R.string.host_is_unreachable);
				break;
			case NETWORK_PROBLEM:
				userCodeStack.clear();
				syncTextView();
				tittleTextView.setText(R.string.no_internet_access);
				break;
			default:
				userCodeStack.clear();
				syncTextView();
				// TODO: 2/14/16 handle errors separately
		}
	}

	public static class ErrorMessageDialogFragment extends DialogFragment {

		public static final String MESSAGE = "message";
		public static final String TITLE = "title";

		@NonNull
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			String message = getArguments().getString(MESSAGE);
			String title = getArguments().getString(TITLE);

			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setTitle(title);
			builder.setMessage(message);
			builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					getActivity().getSupportFragmentManager()
							.popBackStack(MainActivity.USER_CODE_STATE,
									FragmentManager.POP_BACK_STACK_INCLUSIVE);
				}
			});
			return builder.create();
		}

		public static ErrorMessageDialogFragment createInstance(String title, String message) {
			Bundle args = new Bundle();
			args.putString(MESSAGE, message);
			args.putString(TITLE, title);
			ErrorMessageDialogFragment fragment = new ErrorMessageDialogFragment();
			fragment.setArguments(args);
			return fragment;
		}
	}
}
