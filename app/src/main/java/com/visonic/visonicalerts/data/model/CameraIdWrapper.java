package com.visonic.visonicalerts.data.model;

public class CameraIdWrapper {
	public final String camera_id;

	public CameraIdWrapper(String camera_id) {
		this.camera_id = camera_id;
	}
}
