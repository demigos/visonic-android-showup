package com.visonic.visonicalerts.data.model;

public enum VideoProcessStatus {
	FAILED,
	HANDLED,
	START,
	SUCCEEDED,
}
