package com.visonic.visonicalerts.data.datamanager;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.visonic.visonicalerts.data.model.ContentResponse;
import com.visonic.visonicalerts.data.prefs.LoginPrefs;

import java.io.IOException;
import java.net.SocketTimeoutException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BaseStatusDataManager extends BaseDataManager {
	private static final String TAG = "BaseStatusDataManager";
	protected Result mResult;
	protected int mCode;
	protected String mErrorMessage = null;

	private Gson mGson = new Gson();

	public BaseStatusDataManager(LoginPrefs loginPrefs) {
		super(loginPrefs);
	}

	public Result getResult() {
		return mResult;
	}

	public int getCode() {
		return mCode;
	}

	public String getErrorMessage() {
		return mErrorMessage;
	}

	public enum Result {
		OK,
		UNEXPECTED_PROBLEM,
		NETWORK_PROBLEM,
		HTTP_PROBLEM,
		HOST_NOT_ACCESSIBLE,

		WRONG_PANEL_ID,
	}

	public class BaseCallback<T> implements Callback<T> {
		@Override
		public void onResponse(Call<T> call, Response<T> response) {
			if (response.isSuccessful()) {
				mResult = Result.OK;
				success(response.body());
			} else {
				mResult = Result.HTTP_PROBLEM;
				mCode = response.code();
				if (response.errorBody() != null) {
					try {
						String json = response.errorBody().string();
						ContentResponse contentResponse = mGson.fromJson(json, ContentResponse.class);
						if (contentResponse != null && contentResponse.content != null) {
							mErrorMessage = contentResponse.content;
						} else {
							mErrorMessage = null;
						}
					} catch (IOException | JsonSyntaxException e) {
						Log.e(TAG, "", e);
						mErrorMessage = null;
					}
				} else {
					mErrorMessage = null;
				}
				failure();
			}
		}

		protected void success(T data) {
			loadFinished();
		}

		protected void failure() {
			loadFinished();
		}

		@Override
		public void onFailure(Call<T> call, Throwable t) {
			if (t instanceof SocketTimeoutException) {
				mResult = Result.HOST_NOT_ACCESSIBLE;
			} else if (t instanceof IOException) {
				mResult = Result.NETWORK_PROBLEM;
			}
			failure();
			Log.e(TAG, "", t);
		}
	}
}
