package com.visonic.visonicalerts.data.model;

import com.visonic.visonicalerts.R;

public enum AlertType {
	BBA_WENT_OFFLINE(R.string.bba_went_offline),
	COLD_ALERT(R.string.cold_alert),
	FREEZER_ALERT(R.string.freezer_alert),
	FREEZING_ALERT(R.string.freezing_alert),
	GPRS_WENT_OFFLINE(R.string.gprs_went_offline),
	HOT_ALERT(R.string.hot_alert),
	PLINK_WENT_OFFLINE(R.string.plink_went_offline),
	WENT_OFFLINE(R.string.went_offline);

	private final int description;

	AlertType(int description) {
		this.description = description;
	}

	public int getTextResource() {
		return description;
	}
}
