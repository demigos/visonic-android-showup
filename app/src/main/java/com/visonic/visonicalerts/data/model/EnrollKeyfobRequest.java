package com.visonic.visonicalerts.data.model;

public class EnrollKeyfobRequest {
	String device_id;
	String zone;

	public EnrollKeyfobRequest(String device_id, int zone) {
		this.device_id = device_id;
		this.zone = String.valueOf(zone);
	}
}
