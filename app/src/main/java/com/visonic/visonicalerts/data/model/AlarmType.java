package com.visonic.visonicalerts.data.model;

import android.support.annotation.StringRes;

import com.visonic.visonicalerts.R;

public enum AlarmType {
	// ZoneAlarmType
	ALARM_IN_MEMORY(R.string.burglary_alarm),
	TAMPER_MEMORY(R.string.tamper_memory),
	HEAT_MEMORY(R.string.heat_memory),
	SMOKE_MEMORY(R.string.smoke_memory),

	// PanelAlarmType
	FIRE(R.string.fire_alarm),
	//TAMPER_MEMORY,
	PANIC(R.string.panic_alarm),
	EMERGENCY(R.string.emergency_alarm),

	// Should not happen
	UNKNOWN(-1);
	@StringRes
	private final int textResource;

	AlarmType(int textResource) {
		this.textResource = textResource;
	}

	@StringRes
	public int getTextResource() {
		return textResource;
	}
}
