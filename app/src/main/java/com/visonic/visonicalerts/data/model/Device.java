package com.visonic.visonicalerts.data.model;

import android.support.annotation.Nullable;

import java.util.Arrays;

public class Device {
	public int zone;
	@Nullable public String location;
	@Nullable public DeviceType device_type;
	@Nullable public ZoneType type;
	@Nullable public ZoneSubtype subtype;
	public boolean preenroll;
	public boolean soak;
	public boolean bypass;
	@Nullable public AlarmType[] alarms;
	@Nullable public AlertType[] alerts;
	@Nullable public TroubleType[] troubles;
	public boolean bypass_availability;

	public Device(int zone, @Nullable DeviceType device_type) {
		this.zone = zone;
		this.device_type = device_type;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Device device = (Device) o;

		if (zone != device.zone) return false;
		if (preenroll != device.preenroll) return false;
		if (soak != device.soak) return false;
		if (bypass != device.bypass) return false;
		if (bypass_availability != device.bypass_availability) return false;
		if (location != null ? !location.equals(device.location) : device.location != null)
			return false;
		if (device_type != device.device_type) return false;
		if (type != device.type) return false;
		if (subtype != device.subtype) return false;
		if (!Arrays.equals(alarms, device.alarms)) return false;
		//noinspection SimplifiableIfStatement
		if (!Arrays.equals(alerts, device.alerts)) return false;
		return Arrays.equals(troubles, device.troubles);

	}

	@Override
	public int hashCode() {
		int result = zone;
		result = 31 * result + (location != null ? location.hashCode() : 0);
		result = 31 * result + (device_type != null ? device_type.hashCode() : 0);
		result = 31 * result + (type != null ? type.hashCode() : 0);
		result = 31 * result + (subtype != null ? subtype.hashCode() : 0);
		result = 31 * result + (preenroll ? 1 : 0);
		result = 31 * result + (soak ? 1 : 0);
		result = 31 * result + (bypass ? 1 : 0);
		result = 31 * result + Arrays.hashCode(alarms);
		result = 31 * result + Arrays.hashCode(alerts);
		result = 31 * result + Arrays.hashCode(troubles);
		result = 31 * result + (bypass_availability ? 1 : 0);
		return result;
	}

	@Override
	public String toString() {
		return "Device{" +
				"zone=" + zone +
				", location='" + location + '\'' +
				", device_type=" + device_type +
				", type=" + type +
				", subtype=" + subtype +
				", preenroll=" + preenroll +
				", soak=" + soak +
				", bypass=" + bypass +
				", alarms=" + Arrays.toString(alarms) +
				", alerts=" + Arrays.toString(alerts) +
				", troubles=" + Arrays.toString(troubles) +
				", bypass_availability=" + bypass_availability +
				'}';
	}

}

