package com.visonic.visonicalerts.data.model;

import com.google.gson.annotations.SerializedName;
import com.visonic.visonicalerts.R;

public enum TroubleType {
	// ZoneTroubleType
	@SerializedName("1_WAY")ONE_WAY(R.string.working_oneway),
	@SerializedName("1_WAY_REPORTED")ONE_WAY_REPORTED(R.string.reported_oneway),
	AC_FAILURE(R.string.ac_failure),
	BYPASS(R.string.bypass),
	CLEAN_ME(R.string.clean_me),
	COMM_FAILURE(R.string.commander_failure),
	FUSE(R.string.fuse),
	HEAT_OPEN(R.string.heat_open),
	HEAT_TROUBLE(R.string.heat_trouble),
	INACTIVE(R.string.inactive),
	JAMMED(R.string.jammed_alert),
	LINE_FAILURE(R.string.line_failure),
	LOW_BATTERY(R.string.low_battery),
	MARK_FOR_SERVICE(R.string.mark_for_service),
	MASKING_TROUBLE(R.string.masking_trouble),
	NET_TROUBLE(R.string.metwork_trouble),
	NO_ACTIVE(R.string.no_active),
	OPENED(R.string.opened),
	POOR_OR_LESS_24H(R.string.rssi_is_poor_or_less_in_24h),
	POOR_OR_LESS_NOW(R.string.rssi_is_poor_or_less_now),
	PREENROLL_NO_CODE(R.string.pre_enrolled),
	RSSI_LOW(R.string.rsii_is_low),
	SIM_NOT_VERIFIED(R.string.sim_is_not_verfied),
	SMOKE_OPEN(R.string.smoke_open),
	SMOKE_TROUBLE(R.string.smoke_trouble),
	SOAK_FAILED(R.string.soak_failed),
	TAMPER(R.string.tamper),
	TROUBLE(R.string.trouble),
	UNBYPASS(R.string.unbypass),
	UNKNOWN(-1);

	private final int description;

	TroubleType(int description) {
		this.description = description;
	}

	public int getTextResource() {
		return description;
	}
}
