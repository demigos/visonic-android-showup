package com.visonic.visonicalerts.data.datamanager;

import android.os.Handler;

import com.visonic.visonicalerts.BuildConfig;
import com.visonic.visonicalerts.data.VisonicService;
import com.visonic.visonicalerts.data.datamanager.BaseStatusDataManager;
import com.visonic.visonicalerts.data.model.ContentResponse;
import com.visonic.visonicalerts.data.model.GenericResponse;
import com.visonic.visonicalerts.data.model.Status;
import com.visonic.visonicalerts.data.model.WakeUpSms;
import com.visonic.visonicalerts.data.prefs.LoginPrefs;

public class PollStateDataManager extends BaseStatusDataManager {
	private static final int POLL_DELAY = BuildConfig.PANEL_STATUS_POLLING_DELAY;
	private static final int ARMED_POLL_DELAY = BuildConfig.ARMED_PANEL_STATUS_POLLING_DELAY;
	private static final String TAG = "PollStateDataManager";

	private String mWakeUpPhone;
	private String mWakeUpSms;
	private int mExitDelay;

	private Status.ConnectedStatus mConnectedStatus;
	private Status.ReadyStatus mReadyStatus;
	private Status.State mState;

	private boolean mIsTimerTicking;

	private Handler mHandler = new Handler();
	PollTask pollTask = new PollTask();
	private boolean mIsWakeUpDataLoaded;

	public PollStateDataManager(LoginPrefs loginPrefs) {
		super(loginPrefs);
	}

	public void startPoling() {
		mHandler.removeCallbacks(pollTask);
		performPoling();
	}

	public void stopPolling() {
		mHandler.removeCallbacks(pollTask);
	}

	private void performPoling() {
		mHandler.postDelayed(pollTask, mIsTimerTicking ? ARMED_POLL_DELAY : POLL_DELAY);
	}

	public void checkConnectionStatus() {
		VisonicService visonicService = getVisonicService();
		loadStarted();
		visonicService.status(mLoginPrefs.getSessionToken()).enqueue(
				new BaseCallback<GenericResponse<Status>>() {
					@Override
					public void success(GenericResponse<Status> contentResponse) {
						mConnectedStatus = contentResponse.content.is_connected;
						mReadyStatus = contentResponse.content.ready_status;
						mState = contentResponse.content.state;
						mExitDelay = contentResponse.content.exit_delay;

						switch (contentResponse.content.is_connected) {
							case DISCONNECTED:
								if (!mIsWakeUpDataLoaded) {
									loadWakeUpSmsData();
								}
								break;
							case CONNECTED:
								mIsWakeUpDataLoaded = false;
								mResult = Result.OK;
								break;
							default:
								mIsWakeUpDataLoaded = false;
								mResult = Result.UNEXPECTED_PROBLEM;
								break;
						}
						loadFinished();
					}
				});
	}

	public void loadWakeUpSmsData() {
		VisonicService visonicService = getVisonicService();
		loadStarted();
		visonicService.wakeUpSms(mLoginPrefs.getSessionToken()).enqueue(
				new BaseCallback<GenericResponse<WakeUpSms>>() {
					@Override
					public void success(GenericResponse<WakeUpSms> contentResponse) {
						boolean isEmpty = contentResponse.content == null;
						mIsWakeUpDataLoaded = true;
						mWakeUpPhone = isEmpty ? null : contentResponse.content.phone;
						mWakeUpSms = isEmpty ? null : contentResponse.content.sms;
						super.success(contentResponse);
					}
				}
		);
	}

	public void armAway() {
		loadStarted();
		getVisonicService().armAway(mLoginPrefs.getSessionToken())
				.enqueue(new ArmResponseListener());
	}

	public void armAwayInstant() {
		loadStarted();
		getVisonicService().armAwayInstant(mLoginPrefs.getSessionToken())
				.enqueue(new ArmResponseListener());
	}

	public void armHome() {
		loadStarted();
		getVisonicService().armHome(mLoginPrefs.getSessionToken())
				.enqueue(new ArmResponseListener());
	}

	public void armHomeInstant() {
		loadStarted();
		getVisonicService().armHomeInstant(mLoginPrefs.getSessionToken())
				.enqueue(new ArmResponseListener());
	}

	public void disarm() {
		loadStarted();
		getVisonicService().disarm(mLoginPrefs.getSessionToken())
				.enqueue(new BaseCallback<ContentResponse>(){
					@Override
					protected void success(ContentResponse data) {
						super.success(data);
					}
				});
	}

	public void setTimerTicking(boolean timerTicking) {
		mIsTimerTicking = timerTicking;
	}

	public String getWakeUpPhone() {
		return mWakeUpPhone;
	}

	public String getWakeUpSms() {
		return mWakeUpSms;
	}

	public Status.ConnectedStatus getConnectedStatus() {
		return mConnectedStatus;
	}

	public Status.ReadyStatus getReadyStatus() {
		return mReadyStatus;
	}

	public Status.State getState() {
		return mState;
	}

	public int getExitDelay() {
		return mExitDelay;
	}

	private class PollTask implements Runnable {
		@Override
		public void run() {
			checkConnectionStatus();
			performPoling();
		}
	}

	private class ArmResponseListener extends BaseCallback<ContentResponse> {
		@Override
		protected void success(ContentResponse data) {
			super.success(data);
		}
	}
}
