package com.visonic.visonicalerts.data.datamanager;

import android.content.Context;

import com.visonic.visonicalerts.data.datamanager.BaseStatusDataManager;
import com.visonic.visonicalerts.data.model.ContentResponse;
import com.visonic.visonicalerts.data.model.GenericResponse;
import com.visonic.visonicalerts.data.model.ModeWrapper;
import com.visonic.visonicalerts.data.model.RecipientType;
import com.visonic.visonicalerts.data.model.RegisterPushRecipientRequest;
import com.visonic.visonicalerts.data.prefs.LoginPrefs;

public class NotificationDataManager extends BaseStatusDataManager {
	public static int ALERT_MASK = 0B0000_0000_0001;
	public static int ALARMS_MASK = 0b0000_0000_0010;
	public static int RESTORE_MASK = 0b0000_0000_0100;
	public static int SECURITY_OPEN_CLOSE_MASK = 0b0000_0000_1000;

	public static int CAMERA_BEING_VIEW_MASK = 0b0000_0001_0000;
	public static int CAMERA_TROUBLE_MASK = 0b0000_0010_0000;
	public static int HOME_DEVICE_ON_OFF_MASK = 0b0000_0100_0000;
	public static int HOME_DEVICE_TROUBLE_MASK = 0b0000_1000_0000;

	public static int ONLINE_MASK = 0b0001_0000_0000;
	public static int OFFLINE_MASK = 0b0010_0000_0000;
	public static int NOTICE_MASK = 0b0100_0000_0000;

	private int mode;

	public NotificationDataManager(LoginPrefs loginPrefs, Context context) {
		super(loginPrefs);
	}

	public void requestAvailableModes() {
		loadStarted();
		getVisonicService().pushOptions(mLoginPrefs.getSessionToken())
				.enqueue(new BaseCallback<GenericResponse<ModeWrapper>>() {
					@Override
					public void success(GenericResponse<ModeWrapper> modeWrapperGenericResponse) {
						mode = modeWrapperGenericResponse.content.mode;
						super.success(modeWrapperGenericResponse);
					}
				});
	}

	public void changeSettings(int mode) {
		getVisonicService().registerPushRecipient(mLoginPrefs.getSessionToken(),
				new RegisterPushRecipientRequest(mLoginPrefs.getGcmToken(),
						RecipientType.ANDROID,
						mode)).enqueue(new BaseCallback<ContentResponse>() {
		});
	}

	public boolean checkForAvailability(int mask) {
		return (mode & mask) == mask;
	}
}
