package com.visonic.visonicalerts.data.model;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

@SuppressWarnings("NullableProblems")
public class OnDemandVideoStatus {
	public int process_id;
	public int image_frames_count;
	public int audio_frames_count;
	public int total_expected_frames_count;
	@NonNull
	public VideoProcessStatus process_status;
	@NonNull
	public String preview_path;
	// Date
	@NonNull
	public String timestamp;
	@Nullable
	public String error_message;

}
