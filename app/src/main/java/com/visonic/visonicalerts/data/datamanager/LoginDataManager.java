package com.visonic.visonicalerts.data.datamanager;

import com.visonic.visonicalerts.data.VisonicService;
import com.visonic.visonicalerts.data.datamanager.BaseStatusDataManager;
import com.visonic.visonicalerts.data.model.ContentResponse;
import com.visonic.visonicalerts.data.prefs.LoginPrefs;

import java.util.HashMap;
import java.util.Map;

public class LoginDataManager extends BaseStatusDataManager {
	public LoginDataManager(LoginPrefs loginPrefs) {
		super(loginPrefs);
	}

	public void checkPanelAvailability(final String panelId) {
		VisonicService visonicService = getVisonicService();
		loadStarted();
		visonicService.isPanelExists(panelId).enqueue(
				new BaseCallback<ContentResponse>() {
					@Override
					public void success(ContentResponse contentResponse) {
						switch (contentResponse.content) {
							case "true":
								mResult = Result.OK;
								mLoginPrefs.setPanelId(panelId);
								break;
							case "false":
								mResult = Result.WRONG_PANEL_ID;
								break;
							default:
								mResult = Result.UNEXPECTED_PROBLEM;
								break;
						}
						loadFinished();
					}
				});
	}

	public void doLogin(final String userCode, final String userId) {
		VisonicService visonicService = getVisonicService();
		final Map<String, String> loginParams = new HashMap<>();
		loginParams.put("panel_web_name", mLoginPrefs.getPanelId());
		loginParams.put("user_code", userCode);
		loginParams.put("user_id", userId);
		loadStarted();
		visonicService.login(loginParams).enqueue(
				new BaseCallback<ContentResponse>() {
					@Override
					public void success(ContentResponse contentResponse) {
						String token = contentResponse.content;
						mLoginPrefs.login(token);
						mResult = Result.OK;
						super.success(contentResponse);
					}
				}
		);
	}
}
