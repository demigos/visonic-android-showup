package com.visonic.visonicalerts.data.datamanager;

import com.visonic.visonicalerts.data.model.ContentResponse;
import com.visonic.visonicalerts.data.model.Device;
import com.visonic.visonicalerts.data.model.DeviceType;
import com.visonic.visonicalerts.data.model.EnrollKeyfobRequest;
import com.visonic.visonicalerts.data.model.SetBypassRequest;
import com.visonic.visonicalerts.data.model.ZoneSubtype;
import com.visonic.visonicalerts.data.model.ZoneWrapper;
import com.visonic.visonicalerts.data.prefs.LoginPrefs;

import java.util.HashSet;
import java.util.Set;

public class DevicesDataManager extends ListDataManager<Device> {
	private static final String TAG = "DevicesDataManager";

	private Set<Integer> zonesDeleted = new HashSet<>();
	private Set<Integer> zonesNotDeleted = new HashSet<>();
	private Set<Integer> zonesAdded = new HashSet<>();
	private Set<Integer> zonesNotAdded = new HashSet<>();
	Set<DeviceType> mFilters;
	Set<ZoneSubtype> mSubtypesFilters;

	public DevicesDataManager(LoginPrefs loginPrefs) {
		super(loginPrefs);
	}

	@Override
	protected boolean filter(Device item) {
		return item.device_type != DeviceType.GSM
				&& (mFilters == null || mFilters.contains(item.device_type))
				&& (mSubtypesFilters == null || mSubtypesFilters.contains(item.subtype));
	}

	@Override
	protected void visonicServiceCall(SimpleListCallback<Device> callback) {
		getVisonicService().allDevices(mLoginPrefs.getSessionToken()).enqueue(callback);
	}

	@Override
	protected Class<Device> getDataClass() {
		return Device.class;
	}

	public void setFilters(Set<DeviceType> filters) {
		this.mFilters = filters;
	}

	public void setSubtypesFilters(Set<ZoneSubtype> subtypesFilters) {
		this.mSubtypesFilters = subtypesFilters;
	}

	public void setBypassZone(int zone, boolean bypass) {
		loadStarted();
		getVisonicService().setBypassZone(mLoginPrefs.getSessionToken(),
				new SetBypassRequest(zone, bypass)).enqueue(new BaseCallback<ContentResponse>());
	}

	public void deleteKeyfob(final int zone) {
		loadStarted();
		getVisonicService().removeKeyfob(mLoginPrefs.getSessionToken(), new ZoneWrapper(zone))
				.enqueue(new BaseCallback<ContentResponse>() {
					@Override
					public void success(ContentResponse response) {
						zonesDeleted.add(zone);
						super.success(response);
						zonesDeleted.remove(zone);
					}

					@Override
					public void failure() {
						zonesNotDeleted.add(zone);
						super.failure();
						zonesNotDeleted.remove(zone);
					}
				});
	}

	public void enrollKeyfob(String keyfobId, final int zone) {
		loadStarted();
		getVisonicService().enrollKeyfob(mLoginPrefs.getSessionToken(),
				new EnrollKeyfobRequest(keyfobId, zone))
				.enqueue(new BaseCallback<ContentResponse>() {
					@Override
					public void success(ContentResponse response) {
						zonesAdded.add(zone);
						super.success(response);
						zonesAdded.remove(zone);
					}

					@Override
					public void failure() {
						zonesNotAdded.add(zone);
						super.failure();
						zonesNotAdded.remove(zone);
					}
				});
	}

	public Set<Integer> getZonesDeleted() {
		return zonesDeleted;
	}

	public Set<Integer> getZonesNotDeleted() {
		return zonesNotDeleted;
	}

	public Set<Integer> getZonesAdded() {
		return zonesAdded;
	}

	public Set<Integer> getZonesNotAdded() {
		return zonesNotAdded;
	}
}
