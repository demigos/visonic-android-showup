package com.visonic.visonicalerts.data;

import com.visonic.visonicalerts.data.model.Alarm;
import com.visonic.visonicalerts.data.model.AlarmPreview;
import com.visonic.visonicalerts.data.model.Alert;
import com.visonic.visonicalerts.data.model.Camera;
import com.visonic.visonicalerts.data.model.CameraIdWrapper;
import com.visonic.visonicalerts.data.model.ContentResponse;
import com.visonic.visonicalerts.data.model.Device;
import com.visonic.visonicalerts.data.model.DisableSirensModeWrapper;
import com.visonic.visonicalerts.data.model.EnrollKeyfobRequest;
import com.visonic.visonicalerts.data.model.Event;
import com.visonic.visonicalerts.data.model.GenericResponse;
import com.visonic.visonicalerts.data.model.ListResponse;
import com.visonic.visonicalerts.data.model.ModeWrapper;
import com.visonic.visonicalerts.data.model.OnDemandVideoStatus;
import com.visonic.visonicalerts.data.model.RegisterPushRecipientRequest;
import com.visonic.visonicalerts.data.model.SetBypassRequest;
import com.visonic.visonicalerts.data.model.Status;
import com.visonic.visonicalerts.data.model.Trouble;
import com.visonic.visonicalerts.data.model.VideoFile;
import com.visonic.visonicalerts.data.model.VideoFormat;
import com.visonic.visonicalerts.data.model.VideoFrames;
import com.visonic.visonicalerts.data.model.WakeUpSms;
import com.visonic.visonicalerts.data.model.ZoneWrapper;

import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Streaming;

/**
 * Models the API
 */
public interface VisonicService {
	@FormUrlEncoded
	@POST("login")
	Call<ContentResponse> login(@FieldMap Map<String, String> loginParams);

	// TODO rewrite as dynamic URL
	@GET("is_panel_exists")
	Call<ContentResponse> isPanelExists(@Query("panel_web_name") String panelWebName);

	@GET("all_devices")
	Call<ListResponse<Device>> allDevices(@Header("Session-Token") String sessionToken);

	@GET("troubles")
	Call<ListResponse<Trouble>> troubles(@Header("Session-Token") String sessionToken);

	@GET("cameras")
	Call<ListResponse<Camera>> cameras(@Header("Session-Token") String sessionToken);

	@GET("alerts")
	Call<ListResponse<Alert>> alerts(@Header("Session-Token") String sessionToken);

	@GET("events")
	Call<ListResponse<Event>> events(@Header("Session-Token") String sessionToken);

	@GET("alarms")
	Call<ListResponse<Alarm>> alarms(@Header("Session-Token") String sessionToken);

	@GET("is_connected")
	Call<ContentResponse> isConnected(@Header("Session-Token") String sessionToken);

	@GET("wakeup_sms")
	Call<GenericResponse<WakeUpSms>> wakeUpSms(@Header("Session-Token") String sessionToken);

	@GET("status")
	Call<GenericResponse<Status>> status(@Header("Session-Token") String sessionToken);

	@GET("on_demand_video_status")
	Call<GenericResponse<OnDemandVideoStatus>> onDemandVideoStatus(@Header("Session-Token") String sessionToken,
							 @Query("camera_id") String cameraId);

	@GET("on_demand_video_frames")
	Call<GenericResponse<VideoFrames>> onDemandVideoFrames(@Header("Session-Token") String sessionToken,
							 @Query("camera_id") String cameraId);

	@POST("make_video")
	Call<ContentResponse> makeVideo(@Header("Session-Token") String sessionToken,
				   @Body CameraIdWrapper cameraId);

	@GET("on_demand_video")
	Call<GenericResponse<VideoFile>> onDemandVideo(@Header("Session-Token") String sessionToken,
					   @Query("camera_id") String cameraId,
					   @Query("video_format") VideoFormat videoFormat);

	@GET("alarm_video_frames")
	Call<GenericResponse<VideoFrames>> alarmVideoFrames(@Header("Session-Token") String sessionToken,
						  @Query("event_id") int eventId,
						  @Query("camera_id") int cameraId);

	@GET("alarm_video")
	Call<GenericResponse<VideoFile>> alarmVideo(@Header("Session-Token") String sessionToken,
					@Query("event_id") int eventId,
					@Query("camera_id") int cameraId,
					@Query("video_format") VideoFormat videoFormat);

	@POST("disarm")
	Call<ContentResponse> disarm(@Header("Session-Token") String sessionToken);

	@POST("arm_away")
	Call<ContentResponse> armAway(@Header("Session-Token") String sessionToken);

	@POST("arm_away_instant")
	Call<ContentResponse> armAwayInstant(@Header("Session-Token") String sessionToken);

	@POST("arm_home")
	Call<ContentResponse> armHome(@Header("Session-Token") String sessionToken);

	@POST("arm_home_instant")
	Call<ContentResponse> armHomeInstant(@Header("Session-Token") String sessionToken);

	@POST("disable_siren")
	Call<ContentResponse> disableSiren(@Header("Session-Token") String sessionToken,
					  @Body DisableSirensModeWrapper mode);

	@GET("../../{path}")
	@Streaming
	Call<ResponseBody> getVideoStream(@Path(value = "path", encoded = true) String path);

	@GET("push_options")
	Call<GenericResponse<ModeWrapper>> pushOptions(@Header("Session-Token") String sessionToken);

	@POST("register_push_recipient")
	Call<ContentResponse> registerPushRecipient(@Header("Session-Token") String sessionToken,
							   @Body RegisterPushRecipientRequest request);

	@GET("alarm_previews")
	Call<ListResponse<AlarmPreview>> alarmPreviews(@Header("Session-Token") String sessionToken,
					   @Query("event_id") int eventId);

	@POST("remove_keyfob")
	Call<ContentResponse> removeKeyfob(@Header("Session-Token") String sessionToken,
					  @Body ZoneWrapper zoneWrapper);

	@POST("enroll_keyfob")
	Call<ContentResponse> enrollKeyfob(@Header("Session-Token") String sessionToken,
					  @Body EnrollKeyfobRequest enrollKeyfobRequest);

	@POST("set_bypass_zone")
	Call<ContentResponse> setBypassZone(@Header("Session-Token") String sessionToken,
					  @Body SetBypassRequest enrollKeyfobRequest);
}
