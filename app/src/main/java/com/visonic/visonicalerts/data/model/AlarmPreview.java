package com.visonic.visonicalerts.data.model;

import android.os.Parcel;

public class AlarmPreview extends AbstractVideo {
	public int camera_id;

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		super.writeToParcel(dest, flags);
		dest.writeInt(this.camera_id);
	}

	public AlarmPreview() {
	}

	protected AlarmPreview(Parcel in) {
		super(in);
		this.camera_id = in.readInt();
	}

	public static final Creator<AlarmPreview> CREATOR = new Creator<AlarmPreview>() {
		@Override
		public AlarmPreview createFromParcel(Parcel source) {
			return new AlarmPreview(source);
		}

		@Override
		public AlarmPreview[] newArray(int size) {
			return new AlarmPreview[size];
		}
	};
}
