package com.visonic.visonicalerts.data.databasemodel;

import io.realm.RealmObject;
import io.realm.annotations.Required;

public class PanelData extends RealmObject {
	@Required
	private String hostAddress;
	@Required
	private String panelName;

	public String getHostAddress() {
		return hostAddress;
	}

	public void setHostAddress(String hostAddress) {
		this.hostAddress = hostAddress;
	}

	public String getPanelName() {
		return panelName;
	}

	public void setPanelName(String panelName) {
		this.panelName = panelName;
	}
}
