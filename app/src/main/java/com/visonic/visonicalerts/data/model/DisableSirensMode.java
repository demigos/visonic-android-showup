package com.visonic.visonicalerts.data.model;

import com.google.gson.annotations.SerializedName;

public enum DisableSirensMode {
	@SerializedName("all")
	ALL,
	@SerializedName("only_sirens")
	ONLY_SIRENS;

	@Override
	public String toString() {
		return super.toString().toLowerCase();
	}
}
