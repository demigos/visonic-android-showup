package com.visonic.visonicalerts.data.datamanager;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;
import com.visonic.visonicalerts.data.DataLoadingSubject;
import com.visonic.visonicalerts.data.VisonicService;
import com.visonic.visonicalerts.data.prefs.LoginPrefs;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BaseDataManager implements DataLoadingSubject {
	private static final String TAG = "BaseDataManager";
	private static Picasso sPicasso;
	protected final LoginPrefs mLoginPrefs;
	private VisonicService visonicService;
	private List<DataLoadingSubject.DataLoadingCallbacks> loadingCallbacks;
	private AtomicInteger loadingCount;
	private static final HashMap<Class, List> mDataCache = new HashMap<>();

	public BaseDataManager(LoginPrefs loginPrefs) {
		mLoginPrefs = loginPrefs;
		initVisonicService(loginPrefs.getHostAddress());
		loadingCount = new AtomicInteger(0);
	}

	private void initVisonicService(String serverAddress) {
		visonicService = new Retrofit.Builder()
				.baseUrl("https://" + serverAddress + "/rest_api/2.0/")
				.client(getOkHttpClient())
				.addConverterFactory(GsonConverterFactory.create())
				.build()
				.create(VisonicService.class);
	}

	protected VisonicService getVisonicService() {
		if (visonicService == null) {
			throw new IllegalStateException("You must call initVisonicService() " +
					"before making this call");
		}
		return visonicService;
	}

	public static OkHttpClient getOkHttpClient() {
		// XXX unsafe implementation
		try {
			// Create a trust manager that does not validate certificate chains
			final TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
				@Override
				public void checkClientTrusted(
						X509Certificate[] chain,
						String authType) throws CertificateException {
				}

				@Override
				public void checkServerTrusted(
						X509Certificate[] chain,
						String authType) throws CertificateException {
				}

				@Override
				public X509Certificate[] getAcceptedIssuers() {
					return new X509Certificate[0];
				}
			}};

			// Install the all-trusting trust manager
			final SSLContext sslContext = SSLContext.getInstance("SSL");
			sslContext.init(null, trustAllCerts,
					new java.security.SecureRandom());
			// Create an ssl socket factory with our all-trusting manager
			final SSLSocketFactory sslSocketFactory = sslContext
					.getSocketFactory();

			OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient.Builder();
			okHttpClientBuilder.sslSocketFactory(sslSocketFactory);
			okHttpClientBuilder.hostnameVerifier(new HostnameVerifier() {

				@Override
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
			});

			HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
			interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
			okHttpClientBuilder.addInterceptor(interceptor);

			return okHttpClientBuilder.build();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}

	@Override
	public boolean isDataLoading() {
		return loadingCount.get() > 0;
	}

	@Override
	public void addCallbacks(@NonNull DataLoadingSubject.DataLoadingCallbacks callbacks) {
		//noinspection ConstantConditions
		if (callbacks == null) {
			throw new NullPointerException("Callbacks can't be null");
		}
		if (loadingCallbacks == null) {
			loadingCallbacks = new ArrayList<>(1);
		}
		loadingCallbacks.add(callbacks);
	}

	@Override
	public void removeCallbacks(DataLoadingSubject.DataLoadingCallbacks callbacks) {
		if (loadingCallbacks.contains(callbacks)) {
			loadingCallbacks.remove(callbacks);
		}
	}

	protected void loadStarted() {
		if (0 == loadingCount.getAndIncrement()) {
			notifyCallbacksLoadingStarted();
		}
	}

	protected void loadFinished() {
		if (0 == loadingCount.decrementAndGet()) {
			notifyCallbacksLoadingFinished();
		}
	}

	protected void resetLoadingCount() {
		loadingCount.set(0);
	}

	protected void notifyCallbacksLoadingStarted() {
		if (loadingCallbacks == null) return;
		for (DataLoadingCallbacks loadingCallback : loadingCallbacks) {
			loadingCallback.dataStartedLoading();
		}
	}

	protected void notifyCallbacksLoadingFinished() {
		if (loadingCallbacks == null) return;
		for (DataLoadingCallbacks loadingCallback : loadingCallbacks) {
			loadingCallback.dataFinishedLoading();
		}
	}

	public static Picasso getPicassoInstance(Context context) {
		if (sPicasso == null) {
			Picasso.Builder builder = new Picasso.Builder(context);
			builder.downloader(new OkHttp3Downloader(getOkHttpClient()));
			sPicasso = builder.build();
		}

		return sPicasso;
	}

	protected void cacheData(Class classItem, List data) {
		mDataCache.put(classItem, data);
	}

	protected List getCachedData(Class classItem) {
		List list = mDataCache.get(classItem);
		return list;
	}
}
