package com.visonic.visonicalerts.data.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;

public class AbstractVideo implements Parcelable {
	@Nullable
	public String location;
	@Nullable
	public String preview_path;
	/**
	 * UTC date when preview was created
	 */
	@SuppressWarnings("NullableProblems")
	@Nullable
	public String timestamp;


	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.location);
		dest.writeString(this.preview_path);
		dest.writeString(this.timestamp);
	}

	public AbstractVideo() {
	}

	protected AbstractVideo(Parcel in) {
		this.location = in.readString();
		this.preview_path = in.readString();
		this.timestamp = in.readString();
	}

	public static final Creator<AbstractVideo> CREATOR = new Creator<AbstractVideo>() {
		@Override
		public AbstractVideo createFromParcel(Parcel source) {
			return new AbstractVideo(source);
		}

		@Override
		public AbstractVideo[] newArray(int size) {
			return new AbstractVideo[size];
		}
	};
}
