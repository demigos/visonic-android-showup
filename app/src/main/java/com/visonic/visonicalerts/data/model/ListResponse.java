package com.visonic.visonicalerts.data.model;

import java.util.List;

/**
 * Created by GaidamakUA on 1/31/16.
 */
public class ListResponse<T> {
	public List<T> content;
}
