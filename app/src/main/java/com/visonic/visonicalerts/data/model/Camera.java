package com.visonic.visonicalerts.data.model;

import android.os.Parcel;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class Camera extends AbstractVideo {
	public int zone;
	public boolean preenroll;
	@Nullable
	public VideoProcessStatus status;

	public Camera(int zone, boolean preenroll, @Nullable String location,
				  @Nullable VideoProcessStatus status, @Nullable String preview_path,
				  @NonNull String timestamp) {
		this.zone = zone;
		this.preenroll = preenroll;
		this.location = location;
		this.status = status;
		this.preview_path = preview_path;
		this.timestamp = timestamp;
	}

	@Override
	public String toString() {
		return "Camera{" +
				"zone=" + zone +
				", preenroll=" + preenroll +
				", location='" + location + '\'' +
				", status=" + status +
				", preview_path='" + preview_path + '\'' +
				", timestamp='" + timestamp + '\'' +
				'}';
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		super.writeToParcel(dest, flags);
		dest.writeInt(this.zone);
		dest.writeByte(preenroll ? (byte) 1 : (byte) 0);
		dest.writeInt(this.status == null ? -1 : this.status.ordinal());
	}

	protected Camera(Parcel in) {
		super(in);
		this.zone = in.readInt();
		this.preenroll = in.readByte() != 0;
		int tmpStatus = in.readInt();
		this.status = tmpStatus == -1 ? null : VideoProcessStatus.values()[tmpStatus];
	}

	public static final Creator<Camera> CREATOR = new Creator<Camera>() {
		@Override
		public Camera createFromParcel(Parcel source) {
			return new Camera(source);
		}

		@Override
		public Camera[] newArray(int size) {
			return new Camera[size];
		}
	};
}
