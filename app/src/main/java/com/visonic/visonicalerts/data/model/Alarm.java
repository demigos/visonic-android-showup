package com.visonic.visonicalerts.data.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.visonic.visonicalerts.ui.UiUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class Alarm implements Parcelable, Comparable<Alarm> {
	private static final String TAG = "Alarm";
	@SuppressWarnings("NullableProblems")
	@NonNull
	public DeviceType device_type;
	@SuppressWarnings("NullableProblems")
	@NonNull
	public AlarmType alarm_type;
	@SuppressWarnings("NullableProblems")
	@NonNull
	public String datetime;
	public boolean has_video;
	@Nullable
	public Integer evt_id;
	@Nullable
	public Integer zone;
	@Nullable
	public ZoneType zone_type;
	@Nullable
	public String location;

	@Override
	public String toString() {
		return "Alarm{" +
				"device_type=" + device_type +
				", alarm_type=" + alarm_type +
				", datetime='" + datetime + '\'' +
				", has_video=" + has_video +
				", evt_id=" + evt_id +
				", zone=" + zone +
				", zone_type=" + zone_type +
				", location='" + location + '\'' +
				'}';
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		//noinspection ConstantConditions
		dest.writeInt(this.device_type == null ? -1 : this.device_type.ordinal());
		//noinspection ConstantConditions
		dest.writeInt(this.alarm_type == null ? -1 : this.alarm_type.ordinal());
		dest.writeString(this.datetime);
		dest.writeByte(has_video ? (byte) 1 : (byte) 0);
		dest.writeValue(this.evt_id);
		dest.writeValue(this.zone);
		dest.writeInt(this.zone_type == null ? -1 : this.zone_type.ordinal());
		dest.writeString(this.location);
	}

	public Alarm(@NonNull DeviceType device_type,
				 @NonNull AlarmType alarm_type,
				 @NonNull String datetime,
				 boolean has_video,
				 @Nullable Integer evt_id,
				 @Nullable Integer zone,
				 @Nullable ZoneType zone_type,
				 @Nullable String location) {
		this.device_type = device_type;
		this.alarm_type = alarm_type;
		this.datetime = datetime;
		this.has_video = has_video;
		this.evt_id = evt_id;
		this.zone = zone;
		this.zone_type = zone_type;
		this.location = location;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Alarm alarm = (Alarm) o;

		if (has_video != alarm.has_video) return false;
		if (device_type != alarm.device_type) return false;
		if (alarm_type != alarm.alarm_type) return false;
		if (!datetime.equals(alarm.datetime)) return false;
		if (evt_id != null ? !evt_id.equals(alarm.evt_id) : alarm.evt_id != null) return false;
		if (zone != null ? !zone.equals(alarm.zone) : alarm.zone != null) return false;
		//noinspection SimplifiableIfStatement
		if (zone_type != alarm.zone_type) return false;
		return location != null ? location.equals(alarm.location) : alarm.location == null;

	}

	@Override
	public int hashCode() {
		int result = device_type.hashCode();
		result = 31 * result + alarm_type.hashCode();
		result = 31 * result + datetime.hashCode();
		result = 31 * result + (has_video ? 1 : 0);
		result = 31 * result + (evt_id != null ? evt_id.hashCode() : 0);
		result = 31 * result + (zone != null ? zone.hashCode() : 0);
		result = 31 * result + (zone_type != null ? zone_type.hashCode() : 0);
		result = 31 * result + (location != null ? location.hashCode() : 0);
		return result;
	}

	public Alarm() {
	}

	protected Alarm(Parcel in) {
		int tmpDevice_type = in.readInt();
		//noinspection ConstantConditions
		this.device_type = tmpDevice_type == -1 ? null : DeviceType.values()[tmpDevice_type];
		int tmpAlarm_type = in.readInt();
		//noinspection ConstantConditions
		this.alarm_type = tmpAlarm_type == -1 ? null : AlarmType.values()[tmpAlarm_type];
		this.datetime = in.readString();
		this.has_video = in.readByte() != 0;
		this.evt_id = (Integer) in.readValue(Integer.class.getClassLoader());
		this.zone = (Integer) in.readValue(Integer.class.getClassLoader());
		int tmpZone_type = in.readInt();
		this.zone_type = tmpZone_type == -1 ? null : ZoneType.values()[tmpZone_type];
		this.location = in.readString();
	}

	public static final Parcelable.Creator<Alarm> CREATOR = new Parcelable.Creator<Alarm>() {
		public Alarm createFromParcel(Parcel source) {
			return new Alarm(source);
		}

		public Alarm[] newArray(int size) {
			return new Alarm[size];
		}
	};

	@Override
	public int compareTo(@NonNull Alarm another) {
		SimpleDateFormat format = UiUtils.TIMESTAMP_FORMAT;
		try {
			return format.parse(datetime).compareTo(format.parse(another.datetime));
		} catch (ParseException e) {
			Log.e(TAG, "", e);
			return -1;
		}
	}
}
