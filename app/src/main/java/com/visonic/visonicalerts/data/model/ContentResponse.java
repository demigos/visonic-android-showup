package com.visonic.visonicalerts.data.model;

/**
 * Models a response from the Visionic API that returns a JSON with field "content"
 */
public class ContentResponse {
	public final String content;

	public ContentResponse(String content) {
		this.content = content;
	}

	@Override
	public String toString() {
		return "ContentResponse{" +
				"content='" + content + '\'' +
				'}';
	}
}
