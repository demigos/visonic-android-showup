package com.visonic.visonicalerts.data.model;

import android.support.annotation.Nullable;

import java.util.List;

public class VideoFrames {
	@Nullable
	public String location;
	// Date
	public String timestamp;
	public List<String> frames;
}
