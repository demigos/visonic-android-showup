package com.visonic.visonicalerts.data.model;

import android.support.annotation.NonNull;

import com.visonic.visonicalerts.ui.UiUtils;

import java.text.ParseException;
import java.util.Date;

@SuppressWarnings("NullableProblems")
public class Event {
	public int event;
	@NonNull public EventLabel label;
	@NonNull public String description;
	@NonNull public String appointment;
	@NonNull public String datetime;
	public boolean video;
	private Date mDate;

	public Date getDate() {
		if (mDate == null) {
			try {
				mDate = UiUtils.TIMESTAMP_FORMAT.parse(datetime);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		return mDate;
	}

	public enum EventLabel {
		NONE,
		ARM,
		DISARM,
		BURGLER,
		PANEL_ALARM,
		ONLINE,
		OFFLINE,
		BATTERY,
		AC_FAIL
	}
}
