package com.visonic.visonicalerts.data.model;

import com.google.gson.annotations.SerializedName;

public enum VideoFormat {
	@SerializedName("avi")
	AVI,
	@SerializedName("flv")
	FLV,
	@SerializedName("mp4")
	MP4,
	@SerializedName("webm")
	WEBM;

	@Override
	public String toString() {
		return super.toString().toLowerCase();
	}
}
