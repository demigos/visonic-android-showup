package com.visonic.visonicalerts.data.model;

import android.support.annotation.StringRes;

import com.google.gson.annotations.SerializedName;
import com.visonic.visonicalerts.R;

public class Status {
	public ConnectedStatus is_connected;
	public State state;
	public ReadyStatus ready_status;
	public int exit_delay;

	public enum ConnectedStatus {
		@SerializedName("1")
		CONNECTED,
		@SerializedName("0")
		DISCONNECTED
	}

	public enum State {
		@SerializedName("Disarm")
		DISARM,
		@SerializedName("Away")
		AWAY,
		@SerializedName("Home")
		HOME,
		@SerializedName("ExitDelayAway")
		EXIT_DELAY_AWAY,
		@SerializedName("ExitDelayHome")
		EXIT_DELAY_HOME,
		@SerializedName("EntryDelay")
		ENTRY_DELAY,
		@SerializedName("Unknown")
		UNKNOWN;

		@StringRes
		public int getStringId() {
			switch (this) {
				case DISARM:
					return R.string.disarm;
				case AWAY:
					return R.string.away;
				case HOME:
					return R.string.home;
				case EXIT_DELAY_AWAY:
					return R.string.arming_away;
				case EXIT_DELAY_HOME:
					return R.string.arming_home;
				case ENTRY_DELAY:
					return R.string.entry_delay;
				case UNKNOWN:
					return R.string.unknown;
			}
			return -1;
		}
	}

	public enum ReadyStatus {
		@SerializedName("Ready")
		READY,
		@SerializedName("NotReady")
		NOT_READY,
		@SerializedName("Unknown")
		UNKNOWN,
	}
}
