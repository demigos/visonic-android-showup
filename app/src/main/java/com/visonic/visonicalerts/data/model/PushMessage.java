package com.visonic.visonicalerts.data.model;

import android.util.TypedValue;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by atom on 5/5/16.
 */
public final class PushMessage {
    private static final String KEY_PANEL = "Panel";
    private static final String KEY_EVENT_TYPE = "Event type";
    private static final String KEY_ZONE = "Zone";
    private static final String KEY_LOCATION = "Location";
    private static final String KEY_DESCRIPTION = "Description";
    private static final String EVENT_TYPE_ALARM = "Alarm";

    private final String panel;
    private final String eventType;
    private final int zone;
    private final String location;
    private final String description;

    public PushMessage(String panel, String eventType, int zone, String location, String description) {
        this.panel = panel;
        this.eventType = eventType;
        this.zone = zone;
        this.location = location;
        this.description = description;
    }

    public String getPanel() {
        return panel;
    }

    public String getEventType() {
        return eventType;
    }

    public int getZone() {
        return zone;
    }

    public String getLocation() {
        return location;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return "Panel: " + panel + ";\n" +
            "Event type: " + eventType + ";\n" +
            "Zone: " + zone + ";\n" +
            "Location: " + location + ";\n" +
            "Description: " + description + ".";
    }

    public String buildMessageBody() {
        return "Event type: " + eventType + ";\n" +
            "Zone: " + zone + ";\n" +
            "Location: " + location + ";\n" +
            "Description: " + description + ".";
    }

    public boolean isAlarm() {
        return EVENT_TYPE_ALARM.equalsIgnoreCase(eventType);
    }

    public static PushMessage parse(String message) {
        if (message == null || message.isEmpty()) {
            throw new IllegalArgumentException("message is null or empty");
        }

        final Map<String, String> result = new HashMap<>();
        final String[] lines = message.split("\\n");
        for (String line : lines) {
            final String cleanedLine = line.replaceAll("(\\;|\\.)", "");
            final String[] keyValuePair = cleanedLine.split("\\:");
            result.put(keyValuePair[0].trim(), keyValuePair[1].trim());
        }

        return new PushMessage(result.get(KEY_PANEL),
            result.get(KEY_EVENT_TYPE),
            Integer.parseInt(result.get(KEY_ZONE)),
            result.get(KEY_LOCATION),
            result.get(KEY_DESCRIPTION));
    }
}
