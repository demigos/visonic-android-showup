package com.visonic.visonicalerts.data.model;

/**
 * Created by GaidamakUA on 3/3/16.
 */
public enum RecipientType {
	IPHONE,
	ANDROID
}
