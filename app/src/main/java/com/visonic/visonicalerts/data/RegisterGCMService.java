package com.visonic.visonicalerts.data;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.visonic.visonicalerts.R;
import com.visonic.visonicalerts.data.prefs.LoginPrefs;

import java.io.IOException;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class RegisterGCMService extends IntentService {
	private static final String TAG = "RegisterGCMService";

	public RegisterGCMService(){
		super("GcmIntentService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		InstanceID instanceID = InstanceID.getInstance(this);
		try {
			String token = instanceID.getToken(getString(R.string.gcm_defaultSenderId),
					GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
			LoginPrefs.getInstance(this).setGcmToken(token);
		} catch (IOException e) {
			Toast.makeText(RegisterGCMService.this, "Google play services unavailable.", Toast.LENGTH_SHORT).show();
			Toast.makeText(RegisterGCMService.this, "Notifications disabled.", Toast.LENGTH_SHORT).show();
			Log.e(TAG, "", e);
		}
	}
}
