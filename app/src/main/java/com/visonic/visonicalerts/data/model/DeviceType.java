package com.visonic.visonicalerts.data.model;

import com.google.gson.annotations.SerializedName;
import com.visonic.visonicalerts.R;

public enum DeviceType {
	@SerializedName("2_WAY_WIRELESS_KEYPAD") TWO_WAY_WIRELESS_KEYPAD(R.string.two_way_wireless_keypad),
	ZONE(R.string.zone),
	KEYFOB(R.string.keyfobs),
	WIRELESS_COMMANDER(R.string.wireless_commander),
	CAMERA(R.string.camera),
	CONTROL_PANEL(R.string.control_panel),
	GSM(R.string.gsm),
	GUARD_KEY(R.string.guard_key),
	IPMP_SERVER(R.string.ipmp_server),
	PENDANT(R.string.pedant),
	PGM(R.string.pgm),
	POWER_LINK(R.string.power_link),
	PROXY_TAG(R.string.proximity_tag),
	USER(R.string.user),
	UNKNOWN(R.string.unknown),
	WL_SIREN(R.string.wireless_siren),
	REPEATER(R.string.repeater),
	X10(R.string.x10);

	private final int description;

	DeviceType(int description) {
		this.description = description;
	}

	public int getTextResource() {
		return description;
	}
}
