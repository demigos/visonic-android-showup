package com.visonic.visonicalerts.data.model;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class Alert {
	@NonNull public DeviceType device_type;
	@NonNull public AlertType alert_type;
	@Nullable public Integer zone;
	@NonNull public ZoneType zone_type;
	@Nullable public String location;
}
