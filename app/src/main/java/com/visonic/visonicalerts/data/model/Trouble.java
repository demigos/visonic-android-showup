package com.visonic.visonicalerts.data.model;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class Trouble {
	@NonNull
	public DeviceType device_type;
	@NonNull
	public TroubleType trouble_type;
	@Nullable
	public Integer zone;
	@Nullable
	public ZoneType zone_type;
	@Nullable
	public String location;
}
