package com.visonic.visonicalerts.data.prefs;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.text.TextUtils;

public class LoginPrefs {
	private static final String LOGIN_PREF = "LOGIN_PREF";
	private static final String HOST_ADDRESS_PREF = "HOST_ADDRESS_PREF";
	private static final String PANEL_ID_PREF = "PANEL_ID_PREF";
	private static final String SESSION_TOKEN_PREF = "SESSION_TOKEN_PREF";

	private String sessionToken;
	private boolean isLoggedIn = false;
	private String hostAddress;
	private String panelId;
	private String gcmToken;

	private static volatile LoginPrefs singleton;
	private final SharedPreferences prefs;

	public static LoginPrefs getInstance(Context context) {
		if (singleton == null) {
			synchronized (LoginPrefs.class) {
				singleton = new LoginPrefs(context);
			}
		}
		return singleton;
	}

	private LoginPrefs(Context context) {
		prefs = context.getApplicationContext().getSharedPreferences(LOGIN_PREF, Context
				.MODE_PRIVATE);
		hostAddress = prefs.getString(HOST_ADDRESS_PREF, null);
		isLoggedIn = !TextUtils.isEmpty(hostAddress);
		panelId = prefs.getString(PANEL_ID_PREF, null);
		sessionToken = prefs.getString(SESSION_TOKEN_PREF, null);
	}

	public boolean isLoggedIn() {
		return isLoggedIn;
	}

	@Nullable
	public String getHostAddress() {
		return hostAddress == null ? "" : hostAddress;
	}

	@Nullable
	public String getPanelId() {
		return panelId == null ? "" : panelId;
	}

	@Nullable
	public String getSessionToken() {
		return sessionToken;
	}

	public String getGcmToken() {
		return gcmToken;
	}

	public void setGcmToken(String gcmToken) {
		this.gcmToken = gcmToken;
	}

	public void login(String sessionToken) {
		setSessionToken(sessionToken);
		isLoggedIn = true;
	}

	public void setPanelId(String panelId) {
		this.panelId = panelId;
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(PANEL_ID_PREF, panelId).apply();
	}

	public void setHostAddress(String hostAddress) {
		this.hostAddress = hostAddress;
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(HOST_ADDRESS_PREF, hostAddress).apply();
	}

	private void setSessionToken(String sessionToken) {
		this.sessionToken = sessionToken;
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(SESSION_TOKEN_PREF, sessionToken).apply();
	}

	public void logout() {
		setSessionToken(null);
		isLoggedIn = false;
	}

	@Override
	public String toString() {
		return "LoginPrefs{" +
				"sessionToken='" + sessionToken + '\'' +
				", isLoggedIn=" + isLoggedIn +
				", hostAddress='" + hostAddress + '\'' +
				", panelId='" + panelId + '\'' +
				", prefs=" + prefs +
				'}';
	}
}
