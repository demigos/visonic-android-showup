package com.visonic.visonicalerts.data.model;

public class SetBypassRequest {
	String zone;
	String set;

	public SetBypassRequest(int zone, boolean bypass) {
		this.zone = String.valueOf(zone);
		this.set = String.valueOf(bypass);
	}
}
