package com.visonic.visonicalerts.data.model;

import android.support.annotation.StringRes;

import com.google.gson.annotations.SerializedName;
import com.visonic.visonicalerts.R;

public enum ZoneType {
	ARMING_KEY(R.string.arming_key),
	CO(R.string.co_detector),
	DELAY_1(R.string.delay_1),
	DELAY_2(R.string.delay_2),
	EMERGENCY(R.string.emergency_alarm),
	FIRE(R.string.fire_alarm),
	FLOOD(R.string.device_name_flood_detector),
	GAS(R.string.gas_detector),
	GUARD_KEYBOX(R.string.guard_key),
	HOME_DELAY(R.string.home_delay),
	INTERIOR(R.string.interior),
	INTERIOR_FOLLOW(R.string.interior_follow),
	KEYFOB(R.string.device_name_basic_keyfob),
	MAGNET(R.string.magnet),
	NON_ALARM(R.string.non_alarm),
	NOT_SECURITY(R.string.non_security),
	OUTDOOR(R.string.outdoor),
	PERIMETER(R.string.perimeter),
	PERIMETER_FOLLOW(R.string.perimeter_follow),
	PIR(R.string.pir),
	PUSH_BUTTON(R.string.push_button),
	SHOCK(R.string.device_name_shock),
	SHOCK_AUX(R.string.device_name_shock_aux),
	SHOCK_CONTACT_AUX_AMASK(R.string.device_name_shock_contact_aux_antimask),
	SHOCK_CONTACT_G2(R.string.device_name_shock_contact_G2),
	SHOCK_CONTACT_G3(R.string.device_name_shock_contact_G3),
	SMOKE(R.string.device_name_smoke),
	SMOKE_OR_HEAT(R.string.device_name_smoke_or_heat),
	TEMPERATURE(R.string.device_name_temperature),
	TOWER_20(R.string.device_name_motion_v_antimask),
	TOWER_32(R.string.device_name_motion_dual),
	@SerializedName("24H_AUDIBLE") TWENTY_FOUR_H_AUDIBLE(R.string._24h_audible),
	@SerializedName("24H_SILENT") TWENTY_FOUR_H_SILENT(R.string._24h_silent),
	UNKNOWN(R.string.unknown),
	WL_COMMANDER(R.string.wireless_commander);

	@StringRes
	private final int textResource;
	ZoneType(@StringRes int textResource) {
		this.textResource = textResource;
	}

	@StringRes
	public int getTextResource() {
		return textResource;
	}
}
