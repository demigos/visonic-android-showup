package com.visonic.visonicalerts.data.datamanager;

import android.os.Handler;
import android.support.annotation.Nullable;

import com.visonic.visonicalerts.BuildConfig;
import com.visonic.visonicalerts.data.model.ListResponse;
import com.visonic.visonicalerts.data.prefs.LoginPrefs;

import java.util.ArrayList;
import java.util.List;

public abstract class ListDataManager<T> extends BaseStatusDataManager {
	private static final String TAG = "ListDataManager";
	private static final int pollDelay = BuildConfig.DATA_POLLING_DELAY;

	private List<T> mData;
	protected List<T> mFilteredData;
	private SimpleListCallback<T> mCallback = new SimpleListCallback<>();

	private Handler mHandler = new Handler();
	PollTask pollTask = new PollTask();

	public ListDataManager(LoginPrefs loginPrefs) {
		super(loginPrefs);
		mCallback.setDataManager(this);
		if (getDataClass() != null) {
			//noinspection unchecked
			mData = getCachedData(getDataClass());
			if (mData != null) {
				onFilterChanged();
			}
		}
	}

	public void startPolling() {
		loadData();
		performPoling();
	}

	public void stopPolling() {
		mHandler.removeCallbacks(pollTask);
	}

	private void performPoling() {
		mHandler.postDelayed(pollTask, pollDelay);
	}

	private void loadData() {
		loadStarted();
		visonicServiceCall(mCallback);
	}

	private void onFilterChanged() {
		if (mData != null) {
			mFilteredData = new ArrayList<>();
			for (T device : mData) {
				if (filter(device)) {
					mFilteredData.add(device);
				}
			}
		}
	}

	protected boolean filter(T item) {
		return true;
	}

	protected abstract void visonicServiceCall(SimpleListCallback<T> callback);

	@Nullable
	protected abstract Class<T> getDataClass();

	public List<T> getData() {
		return mFilteredData;
	}

	public void performFiltering() {
		loadStarted();
		onFilterChanged();
		loadFinished();
	}

	public class SimpleListCallback<S> extends BaseCallback<ListResponse<S>> {
		private ListDataManager<S> mDataManager;

		public void setDataManager(ListDataManager<S> dataManager) {
			mDataManager = dataManager;
		}

		@Override
		public void success(ListResponse<S> devicesResponse) {
			mDataManager.mData = devicesResponse.content;
			mDataManager.onFilterChanged();
			if (getDataClass() != null) {
				cacheData(getDataClass(), mDataManager.mData);
			}
			super.success(devicesResponse);
		}
	}

	private class PollTask implements Runnable {
		@Override
		public void run() {
			loadData();
			performPoling();
		}
	}
}
