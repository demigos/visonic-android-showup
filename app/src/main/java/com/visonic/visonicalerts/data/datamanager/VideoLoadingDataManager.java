package com.visonic.visonicalerts.data.datamanager;

import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import android.widget.Toast;

import com.visonic.visonicalerts.BuildConfig;
import com.visonic.visonicalerts.data.datamanager.BaseStatusDataManager;
import com.visonic.visonicalerts.data.model.CameraIdWrapper;
import com.visonic.visonicalerts.data.model.ContentResponse;
import com.visonic.visonicalerts.data.model.GenericResponse;
import com.visonic.visonicalerts.data.model.OnDemandVideoStatus;
import com.visonic.visonicalerts.data.model.VideoFile;
import com.visonic.visonicalerts.data.model.VideoFormat;
import com.visonic.visonicalerts.data.model.VideoFrames;
import com.visonic.visonicalerts.data.model.VideoProcessStatus;
import com.visonic.visonicalerts.data.prefs.LoginPrefs;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import okhttp3.ResponseBody;

public class VideoLoadingDataManager extends BaseStatusDataManager {
	private static final String TAG = "VideoLoadingDataManager";
	private static final String VIDEOS_DIRECTORY_NAME = "videos";
	public static final String FILE_PROVIDER_AUTHORITY = "com.visonic.visonicalerts.fileprovider";

	private final Context mContext;

	private VideoProcessStatus mProcessStatus;
	private List<String> mFrames;
	private File mVideoFile;

	private Handler mHandler = new Handler();
	private boolean mIsPolling = false;

	public VideoLoadingDataManager(LoginPrefs loginPrefs, Context context) {
		super(loginPrefs);
		mContext = context;
	}

	public void stopPolling() {
		setPolling(false);
	}

	private synchronized void setPolling(boolean isPolling) {
		mIsPolling = isPolling;
	}

	public synchronized boolean isPolling() {
		return mIsPolling;
	}

	private void startPoling(final String cameraId) {
		mHandler.postDelayed(new Runnable() {
			@Override
			public void run() {
				checkOnDemandVideoStatus(cameraId);
				loadVideoFrameUrls(cameraId);
				loadVideo(cameraId);
				if (isPolling()) {
					startPoling(cameraId);
				}
			}
		}, BuildConfig.VIDEO_POLLING_DELAY);
		setPolling(true);
	}

	public void checkOnDemandVideoStatus(int cameraId) {
		checkOnDemandVideoStatus(String.valueOf(cameraId));
	}

	public void checkOnDemandVideoStatus(String cameraId) {
		loadStarted();
		// TODO: 2/19/16 this request can return nothing
		getVisonicService().onDemandVideoStatus(mLoginPrefs.getSessionToken(),
				cameraId).enqueue(
				new BaseCallback<GenericResponse<OnDemandVideoStatus>>() {
					@Override
					public void success(GenericResponse<OnDemandVideoStatus> statusResponse) {
						if (statusResponse.content != null) {
							mProcessStatus = statusResponse.content.process_status;
						}
						loadFinished();
					}
				});
	}

	public void loadVideoFrameUrls(String cameraId) {
		loadStarted();
		getVisonicService().onDemandVideoFrames(mLoginPrefs.getSessionToken(), cameraId)
				.enqueue(new BaseCallback<GenericResponse<VideoFrames>>() {
					@Override
					public void success(GenericResponse<VideoFrames> onDemandVideoFramesGenericResponse) {
						if (onDemandVideoFramesGenericResponse.content != null) {
							mFrames = onDemandVideoFramesGenericResponse.content.frames;
						}
						loadFinished();
					}
				});
	}

	public void loadVideo(String cameraId) {
		loadStarted();
		getVisonicService().onDemandVideo(mLoginPrefs.getSessionToken(), cameraId, VideoFormat.MP4)
				.enqueue(new BaseCallback<GenericResponse<VideoFile>>() {
					@Override
					public void success(GenericResponse<VideoFile> onDemandVideoGenericResponse) {
						if (onDemandVideoGenericResponse.content != null
								&& onDemandVideoGenericResponse.content.path != null
								&& !onDemandVideoGenericResponse.content.path.equals("null")) {
							mResult = Result.OK;
							String videoUrl = onDemandVideoGenericResponse.content.path;
							downloadVideo(videoUrl);
						}
						loadFinished();
					}
				});
	}

	public void loadAlarmVideoFrames(int eventId, int cameraId) {
		loadStarted();
		getVisonicService().alarmVideoFrames(mLoginPrefs.getSessionToken(), eventId, cameraId)
				.enqueue(new BaseCallback<GenericResponse<VideoFrames>>() {
					@Override
					public void success(GenericResponse<VideoFrames> onDemandVideoFramesGenericResponse) {
						mFrames = onDemandVideoFramesGenericResponse.content.frames;
						super.success(onDemandVideoFramesGenericResponse);
					}
				});
	}

	public void loadAlarmVideo(int eventId, int cameraId) {
		loadStarted();
		getVisonicService().alarmVideo(mLoginPrefs.getSessionToken(),
				eventId, cameraId, VideoFormat.MP4)
				.enqueue(new BaseCallback<GenericResponse<VideoFile>>() {
					@Override
					public void success(GenericResponse<VideoFile> onDemandVideoGenericResponse) {
						String videoUrl = onDemandVideoGenericResponse.content.path;
						downloadVideo(videoUrl);
						super.success(onDemandVideoGenericResponse);
					}
				});
	}

	public void makeVideo(int cameraId) {
		makeVideo(String.valueOf(cameraId));
	}

	public void makeVideo(final String cameraId) {
		loadStarted();
		getVisonicService().makeVideo(mLoginPrefs.getSessionToken(),
				new CameraIdWrapper(cameraId))
				.enqueue(new BaseCallback<ContentResponse>() {
					@Override
					public void success(ContentResponse contentResponse) {
						mProcessStatus = null;
						startPoling(cameraId);
						super.success(contentResponse);
					}

					@Override
					protected void failure() {
						if (getErrorMessage() != null) {
							Toast.makeText(mContext, getErrorMessage(), Toast.LENGTH_SHORT).show();
						}
						super.failure();
					}
				});
	}

	private void downloadVideo(final String videoUrl) {
		loadStarted();
		if (videoUrl.length() > 0) {
			String processedUrl = videoUrl.substring(1);
			getVisonicService().getVideoStream(processedUrl).enqueue(new BaseCallback<ResponseBody>() {
				@Override
				public void success(ResponseBody body) {
					OutputStream out = null;
					try {
						InputStream in = body.byteStream();

						mVideoFile = getFile(videoUrl);
						out = new FileOutputStream(mVideoFile);

						int read;
						byte[] bytes = new byte[1024];

						while ((read = in.read(bytes)) != -1) {
							out.write(bytes, 0, read);
						}
					} catch (IOException e) {
						e.printStackTrace();
						mResult = Result.UNEXPECTED_PROBLEM;
					} finally {
						if (out != null) {
							try {
								out.close();
							} catch (IOException e) {
								e.printStackTrace();
							}
						}
					}
					loadFinished();
				}
			});
		}
	}

	public VideoProcessStatus getProcessStatus() {
		return mProcessStatus;
	}

	public List<String> getFrames() {
		return mFrames;
	}

	public File getVideoFile() {
		return mVideoFile;
	}

	// TODO: 2/28/16 Remove file
	private File getFile(String url) throws IOException {
		File videoDirectory = new File(mContext.getCacheDir(), VIDEOS_DIRECTORY_NAME);
		if (!videoDirectory.exists()) {
			if (!videoDirectory.mkdir()) {
				throw new RuntimeException("Making dir failed");
			}
		}
		String fileName = Uri.parse(url).getLastPathSegment();
		return new File(videoDirectory, fileName);
	}

	public void invalidateData() {
		mFrames = null;
		mVideoFile = null;
	}
}
