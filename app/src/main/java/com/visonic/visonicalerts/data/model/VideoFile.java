package com.visonic.visonicalerts.data.model;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class VideoFile {
	@Nullable
	public String location;
	@Nullable
	public String path;
	/**
	 * Null for AlarmVideo
	 */
	public Boolean is_closed;
	// Date string
	@SuppressWarnings("NullableProblems")
	@NonNull
	public String timestamp;
}
