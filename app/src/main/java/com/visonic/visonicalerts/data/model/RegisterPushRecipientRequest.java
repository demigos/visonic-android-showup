package com.visonic.visonicalerts.data.model;

public class RegisterPushRecipientRequest {
	final String token;
	final RecipientType type;
	final String mode;

	public RegisterPushRecipientRequest(String token, RecipientType type, int mode) {
		this.token = token;
		this.type = type;
		this.mode = String.valueOf(mode);
	}
}
